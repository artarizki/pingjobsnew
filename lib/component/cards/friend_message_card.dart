import 'package:pingjobs/model/message_model.dart';
import 'package:flutter/material.dart';

class FriendMessageCard extends StatelessWidget {
  final MessageModal message;
  final String imageUrl;

  const FriendMessageCard({
    Key key,
    this.message,
    this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          CircleAvatar(
            backgroundImage: NetworkImage(imageUrl != null
                ? imageUrl
                : 'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png'),
          ),
          SizedBox(
            width: 12,
          ),
          Expanded(
            child: Container(
              width: 310,
              padding: EdgeInsets.all(21),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                  bottomRight: Radius.circular(16),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      '${message.body}',
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
