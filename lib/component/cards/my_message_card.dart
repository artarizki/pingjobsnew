import 'package:pingjobs/model/message_model.dart';
import 'package:flutter/material.dart';

class MyMessageCard extends StatelessWidget {
  final MessageModal message;

  const MyMessageCard({
    Key key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 310,
      padding: EdgeInsets.all(21),
      margin: EdgeInsets.only(bottom: 12),
      decoration: BoxDecoration(
        color: Color.fromRGBO(204, 255, 255, 1),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
          bottomLeft: Radius.circular(16),
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              '${message.body}',
            ),
          ),
        ],
      ),
    );
  }
}
