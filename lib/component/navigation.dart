/// lib/presentation/tabs/models/tab_navigation_item.dart
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pingjobs/pages/activity/activity_screen.dart';
import 'package:pingjobs/pages/profile/profile_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/post_job_form_screen.dart';
import 'package:pingjobs/pages/chat/conversations_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/job_home_screen.dart';

class TabNavigationItem extends StatelessWidget {
  final Widget pages;
  final String title;
  final Icon icon;

  TabNavigationItem({
    @required this.pages,
    @required this.title,
    @required this.icon,
  });

  static List<TabNavigationItem> get items => [
        TabNavigationItem(
          pages: SideJobHomeScreen(),
          icon: Icon(Icons.home),
          title: "Beranda",
        ),
        TabNavigationItem(
          pages: AktivitasPage(backStatus: false),
          icon: Icon(Icons.event_note),
          title: "Aktivitas",
        ),
        TabNavigationItem(
          pages: PostJobForm(),
          icon: Icon(Icons.add_business),
          title: "PostJob",
        ),
        TabNavigationItem(
          // pages: ChatPage(title: 'Chat'),
          pages: ConversationsScreen(),
          icon: Icon(Icons.message),
          title: "Percakapan",
        ),
        TabNavigationItem(
          pages: Profile(),
          icon: Icon(Icons.account_box),
          title: "Akun",
        ),
      ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
