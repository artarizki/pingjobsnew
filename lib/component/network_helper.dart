import 'package:http/http.dart' as http;
import 'dart:convert';

class NetworkHelper {
  NetworkHelper({this.sourceLng, this.sourceLat, this.destLng, this.destLat});

  final String url = 'https://api.openrouteservice.org/v2/directions/';
  final String apiKey =
      '5b3ce3597851110001cf6248d5988198424d4dfda2ea6d1bffbbf7a4';
  final String pathParam = 'driving-car'; // Change it if you want
  final double sourceLng;
  final double sourceLat;
  final double destLng;
  final double destLat;

  // mendapatkan data dari http request
  Future getData() async {
    http.Response response = await http.get(Uri.parse(
        '$url$pathParam?api_key=$apiKey&start=$sourceLng,$sourceLat&end=$destLng,$destLat'));

    if (response.statusCode == 200) {
      String data = response.body;
      return jsonDecode(data);
    } else {
      print(response.statusCode);
    }
  }
}
