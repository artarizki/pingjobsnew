import 'package:flutter/material.dart';
import 'package:pingjobs/pages/auth/forgot_password_screen.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/auth/register_screen.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/pages/splash_screen.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => NavigationPage());
      case '/splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/login':
        return MaterialPageRoute(builder: (_) => Login());
      case '/forgot_password':
        return MaterialPageRoute(builder: (_) => ForgotPassword());
      case '/register':
        return MaterialPageRoute(builder: (_) => Register());

//    Course For Role Student
//       case '/take_course':
//         return MaterialPageRoute(builder: (_) => TakeCourseStudentScreen());
//       case '/course':
//         return MaterialPageRoute(builder: (_) => PraktikumScreen());
//       case '/course_video':
//         return MaterialPageRoute(builder: (_) => CourseScreen());
//       case '/help':
//         return MaterialPageRoute(builder: (_) => HelpScreen());
//       case '/result':
//         return MaterialPageRoute(builder: (_) => ResultScreen());

//    Course For Role Teacher
//       case '/take_course_teacher':
//         return MaterialPageRoute(builder: (_)=> TakeCourseTeacherScreen());
//       case '/option':
//         return MaterialPageRoute(builder: (_) => OptionScreen());
//       case '/add_timer':
//         return MaterialPageRoute(builder: (_) => AddTimerScreen());
//       case '/add_room':
//         return MaterialPageRoute(builder: (_) => AddRoomScreen());
//       case '/add_question':
//         return MaterialPageRoute(builder: (_) => AddQuestionScreen());
//       case '/show_result':
//         return MaterialPageRoute(builder: (_) => ShowResultScreen());
//       case '/add_discussion':
//         return MaterialPageRoute(builder: (_) => DiscussionScreen());
//       case '/add_assignment':
//         return MaterialPageRoute(builder: (_)=> AssignmentScreen());

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
