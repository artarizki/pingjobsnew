import 'package:flutter/material.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/auth/register_screen.dart';

const String initialRoute = "welcome";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'main':
        return MaterialPageRoute(builder: (_) => NavigationPage());
      // case 'welcome':
      //   return MaterialPageRoute(builder: (_) => WelcomeScreen());
      case 'login':
        return MaterialPageRoute(builder: (_) => Login());
      case 'sign-up':
        return MaterialPageRoute(builder: (_) => Register());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
