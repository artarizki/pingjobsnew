class SideJobListData {
  SideJobListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.subTxt = "",
    this.descTxt = "",
    this.dist = 1.8,
    this.reviews = 80,
    this.rating = 4.5,
    this.price = 180,
  });

  String imagePath;
  String titleTxt;
  String subTxt;
  String descTxt;
  double dist;
  double rating;
  int reviews;
  int price;

  static List<SideJobListData> jobList = <SideJobListData>[
    SideJobListData(
      imagePath: 'assets/images/kucing.jpg',
      titleTxt: 'Jalan-jalan kucing',
      subTxt: 'Krian, Sidoarjo',
      descTxt:
          'Kucing saya butuh jalan jalan 3 kali seminggu. Dibutuhkan orang untuk bisa menemaninya karena saya tidak dapat menemaninya selama sebulan',
      dist: 2.0,
      reviews: 80,
      rating: 4.4,
      price: 15000,
    ),
    SideJobListData(
      imagePath: 'assets/images/kursus.jpg',
      titleTxt: 'Bantuan Kursus 1 Hari',
      subTxt: 'Kebomas, Gresik',
      descTxt:
          'Saya lagi keluar kota. dia tidak bias belajar tanpa pendampingan seseorang. Dibutuhkan tenaga bantuan kursus 1 hari untuk pelajaran Bahasa inggris',
      dist: 4.0,
      reviews: 74,
      rating: 4.5,
      price: 24000,
    ),
    SideJobListData(
      imagePath: 'assets/images/bersihjendela.jpg',
      titleTxt: 'Bersih Jendela',
      subTxt: 'Gebang, Surabaya',
      descTxt:
          'Jendela saya Kotor sekali, apakah seseorang dapat membersihkannya .Banyak jendela di rumah saya jadinya butuh tenaga ekstra',
      dist: 3.0,
      reviews: 62,
      rating: 4.0,
      price: 100000,
    ),
    SideJobListData(
      imagePath: 'assets/images/cucimobil.jpg',
      titleTxt: 'Cuci mobil',
      subTxt: 'CitraLand, Surabaya',
      descTxt:
          'Mobil saya Kotor habis offroad saya tidak punya waktu untuk membersihkannya karena harus lembur dan berangkat pagi',
      dist: 7.0,
      reviews: 90,
      rating: 4.4,
      price: 220000,
    ),
    SideJobListData(
      imagePath: 'assets/images/servis.jpg',
      titleTxt: 'Servis sepeda motor',
      subTxt: 'Perak, Surabaya',
      descTxt:
          'Sepeda butuh ganti oli, kondisi mesin rusak adakah yang bisa menolong ?',
      dist: 2.0,
      reviews: 240,
      rating: 4.5,
      price: 150000,
    ),
  ];
}
