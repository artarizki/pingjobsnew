import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Style {
  /// Colors
  static final Color primaryColor = Color.fromRGBO(0, 59, 115, 1.0); //#4382d8
  static final Color secondaryColor =
      Color.fromRGBO(216, 36, 41, 1.0); //#4382d8
  static final Color bgColor = Colors.white; //#2facd6
  static final Color blackTextColor = Color.fromRGBO(47, 48, 55, 1);
  static final Color greyTextColor = Color.fromRGBO(106, 108, 123, 1);
  static final Color shadowColor = Color.fromARGB(51, 154, 170, 207);
  static final Color shadowColor2 = Colors.grey.withOpacity(0.6);
  static final Color darkbgColor = Color.fromRGBO(18, 18, 18, 1); // #151515
  static final List<Color> gradientColors = [primaryColor, secondaryColor];
  static final BoxShadow shadowElement = BoxShadow(
    color: Colors.grey.withOpacity(0.4),
    offset: const Offset(4, 4),
    blurRadius: 16,
  );

  static final TextStyle appStyleAppBar = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontWeight: FontWeight.bold,
    ),
  );

  static final TextStyle appStyleJobTitle = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 20,
    ),
  );

  static final TextStyle appStyleJobTitle22900 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontWeight: FontWeight.w900,
      fontSize: 22,
    ),
  );

  static final TextStyle appStyleDefault = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 14,
    ),
  );
  static final TextStyle appStyleDefaultP = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 14,
      color: Style.primaryColor
    ),
  );

  static final TextStyle appStyleDefaultBold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
  );

  static final TextStyle appStyleDefaultBoldGreen = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 14, fontWeight: FontWeight.bold, color: Colors.green),
  );

  static final TextStyle appStyleDefaultBoldRed = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 14, fontWeight: FontWeight.bold, color: Colors.redAccent),
  );

  static final TextStyle appStyleDefaultVeryLight = GoogleFonts.nunito(
    textStyle: TextStyle(
      color: Colors.black45,
      fontWeight: FontWeight.w100,
    ),
  );

  static final TextStyle appStylewhiteDefault = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 14, color: Colors.white),
  );

  static final TextStyle appStylewhiteDefaultBold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.w800),
  );

  static final TextStyle appStyleSmallestText = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 12,
    ),
  );

  static final TextStyle appStyleDetailsText = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 14,
    ),
  );

  static final TextStyle appStyleBodyText16 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 16,
    ),
  );

  static final TextStyle appStyleBodyText18 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 18,
    ),
  );

  static final TextStyle appStyleSubtitleText18 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 18,
    ),
  );

  static final TextStyle appStyleTitleText24 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 24,
    ),
  );

  static final TextStyle appStyleTitleText28 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 28,
    ),
  );

  static final TextStyle appStyleMainTitleText32 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 32,
    ),
  );

  static final TextStyle appStyleMainTitleText38 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 38,
    ),
  );

  static final TextStyle appStyleMainTitleText42 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 42,
    ),
  );

  static final TextStyle appStyle14 = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 14, fontWeight: FontWeight.w600, color: Colors.grey),
  );

  static final TextStyle appStyle14white900 = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 14, fontWeight: FontWeight.w900, color: Colors.white),
  );

  static final TextStyle appStyle14white = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 14, color: Colors.white),
  );

  static final TextStyle appStyle14sColor = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 14, color: Style.secondaryColor),
  );

  static final TextStyle appStyle14g09 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 14,
      color: Colors.grey.withOpacity(0.9),
    ),
  );

  static final TextStyle appStyle16 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16),
  );

  static final TextStyle appStyle16YellowBold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, color: Colors.yellow, fontWeight: FontWeight.w900),
  );

  static final TextStyle appStyle16600 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
  );

  static final TextStyle appStyle16400 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
  );

  static final TextStyle appStyle16Light = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w200),
  );

  static final TextStyle appStyle16300 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
  );

  static final TextStyle appStyle16Bold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
  );

  static final TextStyle appStyle16White = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, color: Colors.white),
  );

  static final TextStyle appStyle16WhiteBold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
  );

  static final TextStyle appStyle18 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 18,
    ),
  );

  static final TextStyle appStyle18b = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold
    ),
  );

  static final TextStyle appStyle18yelloww900 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w900,
      color: Colors.yellow
    ),
  );

  static final TextStyle appStyle18w900 = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.w900
    ),
  );

  static final TextStyle appStyle18Light = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w200),
  );

  static final TextStyle appStyle18white600 = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
  );

  static final TextStyle appStyle18black45 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 18, color: Colors.black45),
  );

  static final TextStyle appStyle20 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 20,
    ),
  );

  static final TextStyle appStyle20BoldpColor = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Style.primaryColor,
    ),
  );

  static final TextStyle appStyle20Bold = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
  );

  static final TextStyle appStyle20900 = GoogleFonts.nunito(
    textStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w900,
    ),
  );

  static final TextStyle appStyle20600blue = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 20, fontWeight: FontWeight.w600, color: Colors.blue),
  );

  static final TextStyle appStyle20blueBold = GoogleFonts.nunito(
    textStyle: TextStyle(
        fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blue),
  );

  static final TextStyle appStyle20600green = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
  );

  static final TextStyle appStyle22pColorLight = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 22, color: Style.primaryColor),
  );

  static final TextStyle appStyle22 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 22),
  );

  static final TextStyle appStyle22greyColorLight = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 22, color: Colors.grey),
  );

  static final TextStyle appStyle22Bold = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
  );

  static final TextStyle appStyle22800 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 22, fontWeight: FontWeight.w800),
  );

  static final TextStyle appStyle32 = GoogleFonts.nunito(
    textStyle: TextStyle(fontSize: 32, fontWeight: FontWeight.w500),
  );

  /// Decorations
  static InputDecoration inputDecoration(String label) {
    return InputDecoration(
      labelText: '$label',
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: Colors.white),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
        borderSide: BorderSide(color: Colors.white),
      ),
    );
  }
}
