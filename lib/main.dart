import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pingjobs/pages/splash_screen.dart';
import 'package:pingjobs/provider/activity_provider.dart';
import 'package:pingjobs/provider/auth_provider2.dart';
import 'package:pingjobs/provider/history_provider.dart';
import 'package:pingjobs/provider/postjob_provider.dart';
import 'package:pingjobs/provider/profile_provider.dart';
import 'package:pingjobs/provider/ratingreview_profile_provider.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:pingjobs/provider/report_provider.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:pingjobs/provider/totalrr_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:pingjobs/provider/auth_provider.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/locator.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  HttpOverrides.global = new DevHttpOverrides();
  runApp(MyApp());
}

class DevHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => SideJobVM()),
        ChangeNotifierProvider(create: (context) => AktivitasVM()),
        ChangeNotifierProvider(create: (context) => ProfilVM()),
        ChangeNotifierProvider(create: (context) => PostJobVM()),
        ChangeNotifierProvider(create: (context) => AuthVM()),
        ChangeNotifierProvider(create: (context) => RatingReviewVM()),
        ChangeNotifierProvider(create: (context) => TotalJobsVM()),
        ChangeNotifierProvider(create: (context) => ReportVM()),
        ChangeNotifierProvider(create: (context) => HistoryVM()),
        ChangeNotifierProvider(create: (context) => RatingReviewProfileVM()),
        ChangeNotifierProvider(create: (context) => ConversationProvider()),
        ChangeNotifierProvider(create: (context) => TotalRRVM()),
        ChangeNotifierProvider(create: (context) => locator<ConversationProvider>()),
        ChangeNotifierProvider(create: (context) => locator<AuthProvider>()),
        ChangeNotifierProvider(create: (context) => locator<UserProvider>()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily: 'Nunito'),
        home: SplashScreen(),
      ),
    );
  }
}
