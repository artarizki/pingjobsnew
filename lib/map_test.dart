import 'dart:async';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pingjobs/component/network_helper.dart';

import 'package:pingjobs/component/line_string.dart';

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 0;
const LatLng SOURCE_LOCATION = LatLng(-7.11439382, 112.4200467);
const double sourceLat = -7.11439382;
const double sourceLng = 112.4200467;
const LatLng DEST_LOCATION = LatLng(-7.1150422, 112.4158926);
// const double destLat = -7.1150422;
// const double destLng = 112.4158926;

class MapOpenRoute extends StatefulWidget {
  @override
  _MapOpenRouteState createState() => _MapOpenRouteState();
}

class _MapOpenRouteState extends State<MapOpenRoute> {
  // url API
  final String url = 'https://api.openrouteservice.org/v2/directions/';

  // parameter route to destination (driving car, bicycle, etc)
  final String pathParam = 'driving-car'; // Change it if you want

  // Key API
  String openrouteAPIKey =
      "5b3ce3597851110001cf6248d5988198424d4dfda2ea6d1bffbbf7a4";
  String googleAPIKey = "AIzaSyDeh5BUkFoByEsCMZgJHg02D6XpIn_i5n8";

// deklarasi pin custom marker
  BitmapDescriptor sourceIcon;
  BitmapDescriptor trackingIcon;
  BitmapDescriptor destinationIcon;

// lokasi awal pengguna dan lokasi saat ini saat berpindah tempat
  LocationData currentLocation;
  LocationData startLocation;

// referensi ke lokasi tujuan
  LocationData destinationLocation;

// wrapper API Location
  Location location;
  Completer<GoogleMapController> _controller = Completer();

  // deklarasi marker, polyline untuk rute, dan koordinat
  Set<Marker> _markers = Set<Marker>();
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];

  // PolylinePoints polylinePoints;
  // Map<PolylineId, Polyline> polylines = {};
  Polyline polyline;

  // posisi kamera
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(-7.1143938, 112.4200467),
    zoom: 15,
  );

  // data rute dari request http
  var data;

  @override
  void initState() {
    super.initState();

    // inisialisasi lokasi
    location = new Location();
    // setFirstInitialLocation();
    // membuat pin
    setSourceAndDestinationIcons();

    // request rute dari API
    getJsonData();

    // getJsonData();
    // membuat instance location
    // polylinePoints = PolylinePoints();

    // listen perubahan lokasi dari event onLocationChanged
    location.onLocationChanged.listen((LocationData cLocation) {
      // currentLocation terdapat Lat dan Longtude dari posisi user scr realtime
      currentLocation = cLocation;
      updatePinOnMap();
    });

    // setPolyLines();
  }

  // request rute dari API
  void getJsonData() async {
    // Buat instance Class NetworkHelper yang menggunakan paket http
    // untuk meminta data ke server dan menerima respons sebagai format JSON
    await setInitialLocation();
    NetworkHelper network;
    network = NetworkHelper(
      sourceLng: startLocation.longitude,
      sourceLat: startLocation.latitude,
      destLng: destinationLocation.longitude,
      destLat: destinationLocation.latitude,
    );
    print("CurrentLng : " +
        currentLocation.longitude.toString() +
        "CurrentLat : " +
        currentLocation.latitude.toString());
    print("StartLng : " +
        startLocation.longitude.toString() +
        "StartLat : " +
        startLocation.latitude.toString());
    print("DestLng : " +
        destinationLocation.longitude.toString() +
        "DestLat : " +
        destinationLocation.latitude.toString());
    try {
      // getData () mengembalikan data decoded json
      data = await network.getData();

      // dapat mencapai data JSON yang diinginkan secara manual
      LineString ls =
          LineString(data['features'][0]['geometry']['coordinates']);

      for (int i = 0; i < ls.lineString.length; i++) {
        polylineCoordinates
            .add(LatLng(ls.lineString[i][1], ls.lineString[i][0]));
      }
      //removePolyLines();
      setPolyLines();
    } catch (e) {
      print(e);
    }
  }

  // membuat pin
  void setSourceAndDestinationIcons() {
    sourceIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange);
    trackingIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure);
    destinationIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen);
  }

  // void setFirstInitialLocation() async {
  //   startLocation = await location.getLocation();
  // }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();
    print("CurrentLng : " +
        currentLocation.longitude.toString() +
        "CurrentLat : " +
        currentLocation.latitude.toString());
    startLocation = LocationData.fromMap({
      "longitude": currentLocation.longitude,
      "latitude": currentLocation.latitude,
    });

    print("StartLng : " +
        startLocation.longitude.toString() +
        "StartLat : " +
        startLocation.latitude.toString());

    destinationLocation = LocationData.fromMap({
      "latitude": DEST_LOCATION.latitude,
      "longitude": DEST_LOCATION.longitude
    });

    print("DestLng : " +
        destinationLocation.longitude.toString() +
        "DestLat : " +
        destinationLocation.latitude.toString());
  }

  // memusatkan tampilan ke lokasi terkini
  void centerOnMap() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 15.0,
      ),
    ));
  }

  // memperbarui pin
  void updatePinOnMap() async {
    // membuat posisi baru di map dan kamera mengikutinya
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));

    // agar flutter diberi tahu untuk mengubah posisi kamera
    setState(() {
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      // hapus penanda dan tambahkan lagi untuk pembaruan
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      _markers.add(Marker(
        markerId: MarkerId('trackingPin'),
        position: pinPosition,
        icon: trackingIcon,
      ));
    });
  }

  void showPinsonMap() {
    // get LatLng untuk lokasi awal dari LocationData->Objek CurrentLocation
    var pinPosition =
        LatLng(currentLocation.latitude, currentLocation.longitude);
    var startPosition = LatLng(startLocation.latitude, startLocation.longitude);
    // get LatLng untuk lokasi tujuan
    var destPosition =
        LatLng(destinationLocation.latitude, destinationLocation.longitude);

    // add pin lokasi terkini/awal
    _markers.add(Marker(
      markerId: MarkerId('sourcePin'),
      position: startPosition,
      icon: sourceIcon,
      infoWindow: InfoWindow(
        title: "AWAL",
        snippet: "Posisi awal Kamu Disini !",
      ),
    ));

    _markers.add(Marker(
      markerId: MarkerId('trackingPin'),
      position: pinPosition,
      icon: trackingIcon,
      infoWindow: InfoWindow(
        title: "JALAN",
        snippet: "Kamu Sekarang Lagi Disini !",
      ),
    ));

    // add pin lokasi tujuan
    _markers.add(Marker(
      markerId: MarkerId('destPin'),
      position: destPosition,
      icon: destinationIcon,
      infoWindow: InfoWindow(
        title: "TUJUANN",
        snippet: "Tujuan Kamu Disini !",
      ),
    ));

    // set lokasi rute
    setPolyLines();
  }

  // _addPolyLine() {
  //   PolylineId id = PolylineId("poly");
  //   Polyline polyline = Polyline(
  //       polylineId: id, color: Color.fromARGB(255, 40, 122, 198), points: polylineCoordinates);
  //   polylines[id] = polyline;
  //   setState(() {});
  // }

  // membuat rute dari 2 titik
  setPolyLines() {
    polyline = Polyline(
      polylineId: PolylineId("polyline"),
      color: Colors.blueAccent,
      points: polylineCoordinates,
      width: 4,
    );
    _polylines.add(polyline);
    setState(() {});
  }

  // removePolyLines() {
  //   _polylines.remove(polyline);
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                myLocationEnabled: true,
                compassEnabled: true,
                tiltGesturesEnabled: false,
                mapType: MapType.normal,
                initialCameraPosition: _kGooglePlex,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  showPinsonMap();
                },
                markers: _markers,
                polylines: _polylines,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // tombol untuk memusatkan tampilan ke lokasi user terkini
          centerOnMap();
        },
        child: Icon(Icons.accessibility_new),
      ),
    );
  }
}
