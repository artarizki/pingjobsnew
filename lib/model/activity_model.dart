// To parse this JSON data, do
//
//     final modelAktivitas = modelAktivitasFromJson(jsonString);

import 'dart:convert';

List<ModelAktivitas> modelAktivitasFromJson(String str) =>
    List<ModelAktivitas>.from(
        json.decode(str).map((x) => ModelAktivitas.fromJson(x)));

String modelAktivitasToJson(List<ModelAktivitas> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelAktivitas {
  ModelAktivitas({
    this.idActivityHistory,
    this.idJob,
    this.idJobseeker,
    this.idJobprovider,
    this.role,
    this.status,
    this.note,
    this.xACoordinateJobseeker,
    this.yACoordinateJobseeker,
    this.startDate,
    this.endDate,
    this.createAt,
    this.updateAt,
    this.jobName,
    this.jobCategory,
    this.jobDescription,
    this.fee,
    this.jobDate,
    this.jadwal,
    this.hoursStart,
    this.hoursEnd,
    this.jobPhotovideo,
    this.street,
    this.xJPin,
    this.yJPin,
  });

  String idActivityHistory;
  String idJob;
  String idJobseeker;
  String idJobprovider;
  String role;
  String status;
  String note;
  String xACoordinateJobseeker;
  String yACoordinateJobseeker;
  DateTime startDate;
  DateTime endDate;
  DateTime createAt;
  DateTime updateAt;
  String jobName;
  String jobCategory;
  String jobDescription;
  String fee;
  DateTime jobDate;
  String jadwal;
  String hoursStart;
  String hoursEnd;
  String jobPhotovideo;
  String street;
  String xJPin;
  String yJPin;

  factory ModelAktivitas.fromJson(Map<String, dynamic> json) => ModelAktivitas(
        idActivityHistory: json["id_activity_history"],
        idJob: json["id_job"],
        idJobseeker: json["id_jobseeker"],
        idJobprovider: json["id_jobprovider"],
        role: json["role"],
        status: json["status"],
        note: json["note"],
        xACoordinateJobseeker: json["X(a.coordinate_jobseeker)"],
        yACoordinateJobseeker: json["Y(a.coordinate_jobseeker)"],
        startDate: DateTime.parse(json["start_date"]),
        endDate: DateTime.parse(json["end_date"]),
        createAt: DateTime.parse(json["create_at"]),
        updateAt: DateTime.parse(json["update_at"]),
        jobName: json["job_name"],
        jobCategory: json["job_category"],
        jobDescription: json["job_description"],
        fee: json["fee"],
        jobDate: DateTime.parse(json["job_date"]),
        jadwal: json["jadwal"],
        hoursStart: json["hours_start"],
        hoursEnd: json["hours_end"],
        jobPhotovideo: json["job_photovideo"],
        street: json["street"],
        xJPin: json["X(j.pin)"],
        yJPin: json["Y(j.pin)"],
      );

  Map<String, dynamic> toJson() => {
        "id_activity_history": idActivityHistory,
        "id_job": idJob,
        "id_jobseeker": idJobseeker,
        "id_jobprovider": idJobprovider,
        "role": role,
        "status": status,
        "note": note,
        "start_date": startDate.toIso8601String(),
        "end_date": endDate.toIso8601String(),
        "create_at": createAt.toIso8601String(),
        "update_at": updateAt.toIso8601String(),
        "job_name": jobName,
        "job_category": jobCategory,
        "job_description": jobDescription,
        "fee": fee,
        "job_date": jobDate.toIso8601String(),
        "jadwal": jadwal,
        "hours_start": hoursStart,
        "hours_end": hoursEnd,
        "job_photovideo": jobPhotovideo,
        "street": street,
        "X(j.pin)": xJPin,
        "Y(j.pin)": yJPin,
      };
}
