// To parse this JSON data, do
//
//     final modelAuth = modelAuthFromJson(jsonString);

import 'dart:convert';

List<ModelAuth> modelAuthFromJson(String str) =>
    List<ModelAuth>.from(json.decode(str).map((x) => ModelAuth.fromJson(x)));

String modelAuthToJson(List<ModelAuth> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelAuth {
  ModelAuth({
    this.idJobseeker,
    this.idJobprovider,
    this.idJob,
    this.name,
    this.password,
    this.ratings,
    this.reviews,
    this.jobApplied,
    this.email,
    this.specialization,
    this.mobilenumber,
    this.fotoProfil,
    this.bio,
    this.createAt,
    this.updateAt,
  });

  String idJobseeker;
  String idJobprovider;
  String idJob;
  String name;
  String password;
  String ratings;
  String reviews;
  String jobApplied;
  String email;
  String specialization;
  String mobilenumber;
  String fotoProfil;
  String bio;
  DateTime createAt;
  DateTime updateAt;

  factory ModelAuth.fromJson(Map<String, dynamic> json) => ModelAuth(
        idJobseeker: json["id_jobseeker"],
        idJobprovider: json["id_jobprovider"],
        idJob: json["id_job"],
        name: json["name"],
        password: json["password"],
        ratings: json["ratings"],
        reviews: json["reviews"],
        jobApplied: json["job_applied"],
        email: json["email"],
        specialization: json["specialization"],
        mobilenumber: json["mobilenumber"],
        fotoProfil: json["foto_profil"],
        bio: json["bio"],
        createAt: DateTime.parse(json["create_at"]),
        updateAt: DateTime.parse(json["update_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id_jobseeker": idJobseeker,
        "id_jobprovider": idJobprovider,
        "id_job": idJob,
        "name": name,
        "password": password,
        "ratings": ratings,
        "reviews": reviews,
        "job_applied": jobApplied,
        "email": email,
        "specialization": specialization,
        "mobilenumber": mobilenumber,
        "foto_profil": fotoProfil,
        "bio": bio,
        "create_at": createAt.toIso8601String(),
        "update_at": updateAt.toIso8601String(),
      };
}
