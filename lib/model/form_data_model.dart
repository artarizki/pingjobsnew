class ModelFormData {
  ModelFormData(
    // this.idJob,
    this.jobName,
    this.jobCategory,
    this.jobDate,
    this.jobDescription,
    this.fee,
    // this.distance,
    this.hours,
    // this.jobPhotovideo,
    this.street,
    this.city,
    // this.pin,
  );

  String idJob;
  String jobName;
  String jobCategory;
  String jobDate;
  String jobDescription;
  String fee;

  // String distance;
  String hours;

  // String jobPhotovideo;
  String street;
  String city;
// String pin;
}
