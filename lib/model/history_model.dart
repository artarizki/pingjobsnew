// To parse this JSON data, do
//
//     final modelHistory = modelHistoryFromJson(jsonString);

import 'dart:convert';

List<ModelHistory> modelHistoryFromJson(String str) => List<ModelHistory>.from(
    json.decode(str).map((x) => ModelHistory.fromJson(x)));

String modelHistoryToJson(List<ModelHistory> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelHistory {
  ModelHistory({
    this.idJob,
    this.startDate,
    this.role,
    this.jobName,
    this.jobDescription,
    this.fee,
    this.jobDate,
    this.jadwal,
    this.hoursStart,
    this.hoursEnd,
    this.jobPhotovideo,
    this.idUser,
    this.idUser2,
    this.keterangan,
    this.bintang,
    this.ketepatanWaktu,
    this.sikapPerilaku,
    this.idJobseeker,
    this.idJobprovider,
  });

  String startDate;
  String idJob;
  String role;
  String jobName;
  String jobDescription;
  String fee;
  DateTime jobDate;
  String jadwal;
  String hoursStart;
  String hoursEnd;
  String jobPhotovideo;
  String idUser;
  String idUser2;
  String keterangan;
  String bintang;
  String ketepatanWaktu;
  String sikapPerilaku;
  String idJobseeker;
  String idJobprovider;

  factory ModelHistory.fromJson(Map<String, dynamic> json) => ModelHistory(
        idJob: json["id_job"],
        startDate: json["start_date"],
        role: json["role"],
        jobName: json["job_name"],
        jobDescription: json["job_description"],
        fee: json["fee"],
        jobDate: DateTime.parse(json["job_date"]),
        jadwal: json["jadwal"],
        hoursStart: json["hours_start"],
        hoursEnd: json["hours_end"],
        jobPhotovideo: json["job_photovideo"],
        idUser: json["id_user"],
        idUser2: json["id_user2"],
        keterangan: json["keterangan"],
        bintang: json["bintang"],
        ketepatanWaktu: json["ketepatan_waktu"],
        sikapPerilaku: json["sikap_perilaku"],
        idJobseeker: json["id_jobseeker"],
        idJobprovider: json["id_jobprovider"],
      );

  Map<String, dynamic> toJson() => {
        "start_date": startDate,
        "id_job": idJob,
        "role": role,
        "job_name": jobName,
        "job_description": jobDescription,
        "fee": fee,
        "job_date": jobDate.toIso8601String(),
        "jadwal": jadwal,
        "hours_start": hoursStart,
        "hours_end": hoursEnd,
        "job_photovideo": jobPhotovideo,
        "id_user": idUser,
        "id_user2": idUser2,
        "keterangan": keterangan,
        "bintang": bintang,
        "ketepatan_waktu": ketepatanWaktu,
        "sikap_perilaku": sikapPerilaku,
        "id_jobseeker": idJobseeker,
        "id_jobprovider": idJobprovider,
      };
}
