// To parse this JSON data, do
//
//     final modelPostJob = modelPostJobFromJson(jsonString);

import 'dart:convert';

List<ModelPostJob> modelPostJobFromJson(String str) => List<ModelPostJob>.from(
    json.decode(str).map((x) => ModelPostJob.fromJson(x)));

String modelPostJobToJson(List<ModelPostJob> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelPostJob {
  ModelPostJob({
    // this.idJob,
    this.jobName,
    this.jobCategory,
    this.jobDate,
    this.jobDescription,
    this.fee,
    // this.distance,
    this.hours,
    // this.jobPhotovideo,
    this.street,
    this.city,
    // this.pin,
  });

  String idJob;
  String jobName;
  String jobCategory;
  DateTime jobDate;
  String jobDescription;
  String fee;

  // String distance;
  String hours;

  // String jobPhotovideo;
  String street;
  String city;

  // String pin;

  factory ModelPostJob.fromJson(Map<String, dynamic> json) => ModelPostJob(
        // idJob: json["id_job"],
        jobName: json["job_name"],
        jobCategory: json["job_category"],
        jobDate: DateTime.parse(json["job_date"]),
        jobDescription: json["job_description"],
        fee: json["fee"],
        // distance: json["distance"],
        hours: json["hours"],
        // jobPhotovideo: json["job_photovideo"],
        street: json["street"],
        city: json["city"],
        // pin: json["pin"],
      );

  Map<String, dynamic> toJson() => {
        // "id_job": idJob,
        "job_name": jobName,
        "job_category": jobCategory,
        "job_date": jobDate.toIso8601String(),
        "job_description": jobDescription,
        "fee": fee,
        // "distance": distance,
        "hours": hours,
        // "job_photovideo": jobPhotovideo,
        "street": street,
        "city": city,
        // "pin": pin,
      };
}
