// To parse this JSON data, do
//
//     final modelProfile = modelProfileFromJson(jsonString);

import 'dart:convert';

List<ModelProfile> modelProfileFromJson(String str) => List<ModelProfile>.from(
    json.decode(str).map((x) => ModelProfile.fromJson(x)));

String modelProfileToJson(List<ModelProfile> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelProfile {
  ModelProfile({
    this.idJobseeker,
    this.idJob,
    this.name,
    this.password,
    this.ratings,
    this.reviews,
    this.jobApplied,
    this.email,
    this.specialization,
    this.mobilenumber,
    this.fotoProfil,
    this.bio,
    this.createAt,
    this.updateAt,
  });

  String idJobseeker;
  String idJob;
  String name;
  String password;
  String ratings;
  String reviews;
  String jobApplied;
  String email;
  String specialization;
  String mobilenumber;
  String fotoProfil;
  String bio;
  DateTime createAt;
  DateTime updateAt;

  factory ModelProfile.fromJson(Map<String, dynamic> json) => ModelProfile(
        idJobseeker: json["id_jobseeker"],
        idJob: json["id_job"],
        name: json["name"],
        password: json["password"],
        ratings: json["ratings"],
        reviews: json["reviews"],
        jobApplied: json["job_applied"],
        email: json["email"],
        specialization: json["specialization"],
        mobilenumber: json["mobilenumber"],
        fotoProfil: json["foto_profil"],
        bio: json["bio"],
        createAt: DateTime.parse(json["create_at"]),
        updateAt: DateTime.parse(json["update_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id_jobseeker": idJobseeker,
        "id_job": idJob,
        "name": name,
        "password": password,
        "ratings": ratings,
        "reviews": reviews,
        "job_applied": jobApplied,
        "email": email,
        "specialization": specialization,
        "mobilenumber": mobilenumber,
        "foto_profil": fotoProfil,
        "bio": bio,
        "create_at": createAt.toIso8601String(),
        "update_at": updateAt.toIso8601String(),
      };
}
