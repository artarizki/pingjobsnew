// To parse this JSON data, do
//
//     final ratingReviewModel = ratingReviewModelFromJson(jsonString);

import 'dart:convert';

List<RatingReviewModel> ratingReviewModelFromJson(String str) =>
    List<RatingReviewModel>.from(
        json.decode(str).map((x) => RatingReviewModel.fromJson(x)));

String ratingReviewModelToJson(List<RatingReviewModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RatingReviewModel {
  RatingReviewModel({
    this.idUser,
    this.idUser2,
    this.keterangan,
    this.bintang,
    this.createAt,
    this.updateAt,
  });

  String idUser;
  String idUser2;
  String keterangan;
  String bintang;
  DateTime createAt;
  DateTime updateAt;

  factory RatingReviewModel.fromJson(Map<String, dynamic> json) =>
      RatingReviewModel(
        idUser: json["id_jobseeker"],
        idUser2: json["id_jobprovider"],
        keterangan: json["keterangan"],
        bintang: json["bintang"],
        createAt: DateTime.parse(json["create_at"]),
        updateAt: DateTime.parse(json["update_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id_user": idUser,
        "id_user2": idUser2,
        "keterangan": keterangan,
        "bintang": bintang,
        "create_at": createAt.toIso8601String(),
        "update_at": updateAt.toIso8601String(),
      };
}
