// To parse this JSON data, do
//
//     final modelRatingReviewProfile = modelRatingReviewProfileFromJson(jsonString);

import 'dart:convert';

List<ModelRatingReviewProfile> modelRatingReviewProfileFromJson(String str) =>
    List<ModelRatingReviewProfile>.from(
        json.decode(str).map((x) => ModelRatingReviewProfile.fromJson(x)));

String modelRatingReviewProfileToJson(List<ModelRatingReviewProfile> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelRatingReviewProfile {
  ModelRatingReviewProfile({
    this.jobName,
    this.role,
    this.name,
    this.fotoProfil,
    this.idUser,
    this.idUser2,
    this.keterangan,
    this.bintang,
    this.endDate,
  });

  String jobName;
  String role;
  String name;
  String fotoProfil;
  String idUser;
  String idUser2;
  String keterangan;
  String bintang;
  DateTime endDate;

  factory ModelRatingReviewProfile.fromJson(Map<String, dynamic> json) =>
      ModelRatingReviewProfile(
        jobName: json["job_name"],
        role: json["role"],
        name: json["name"],
        fotoProfil: json["foto_profil"],
        idUser: json["id_user"],
        idUser2: json["id_user2"],
        keterangan: json["keterangan"],
        bintang: json["bintang"],
        endDate: DateTime.parse(json["end_date"]),
      );

  Map<String, dynamic> toJson() => {
        "job_name": jobName,
        "role": role,
        "name": name,
        "foto_profil": fotoProfil,
        "id_user": idUser,
        "id_user2": idUser2,
        "keterangan": keterangan,
        "bintang": bintang,
        "end_date": endDate.toIso8601String(),
      };
}
