// To parse this JSON data, do
//
//     final modelReport = modelReportFromJson(jsonString);

import 'dart:convert';

List<ModelReport> modelReportFromJson(String str) => List<ModelReport>.from(
    json.decode(str).map((x) => ModelReport.fromJson(x)));

String modelReportToJson(List<ModelReport> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelReport {
  ModelReport({
    this.idReport,
    this.idReporter,
    this.idTargetuser,
    this.keterangan,
    this.createAt,
    this.updateAt,
  });

  String idReport;
  String idReporter;
  String idTargetuser;
  String keterangan;
  DateTime createAt;
  DateTime updateAt;

  factory ModelReport.fromJson(Map<String, dynamic> json) => ModelReport(
        idReport: json["id_report"],
        idReporter: json["id_reporter"],
        idTargetuser: json["id_targetuser"],
        keterangan: json["keterangan"],
        createAt: DateTime.parse(json["create_at"]),
        updateAt: DateTime.parse(json["update_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id_report": idReport,
        "id_reporter": idReporter,
        "id_targetuser": idTargetuser,
        "keterangan": keterangan,
        "create_at": createAt.toIso8601String(),
        "update_at": updateAt.toIso8601String(),
      };
}
