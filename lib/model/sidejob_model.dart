// To parse this JSON data, do
//
//     final modelSideJob = modelSideJobFromJson(jsonString);

import 'dart:convert';

List<ModelSideJob> modelSideJobFromJson(String str) => List<ModelSideJob>.from(
    json.decode(str).map((x) => ModelSideJob.fromJson(x)));

String modelSideJobToJson(List<ModelSideJob> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelSideJob {
  ModelSideJob({
    this.idJob,
    this.idJobseeker,
    this.idJobprovider,
    this.name,
    this.fotoProfil,
    this.bio,
    this.jobName,
    this.jobCategory,
    this.jobDate,
    this.jobDescription,
    this.fee,
    this.persyaratan,
    this.fasilitas,
    this.distance,
    this.jadwal,
    this.hoursStart,
    this.hoursEnd,
    this.jobPhotovideo,
    this.street,
    this.city,
    this.xJPin,
    this.yJPin,
  });

  String idJob;
  String idJobseeker;
  String idJobprovider;
  String name;
  String fotoProfil;
  String bio;
  String jobName;
  String jobCategory;
  DateTime jobDate;
  String jobDescription;
  String fee;
  String persyaratan;
  String fasilitas;
  String distance;
  String jadwal;
  String hoursStart;
  String hoursEnd;
  String jobPhotovideo;
  String street;
  String city;
  String xJPin;
  String yJPin;

  factory ModelSideJob.fromJson(Map<String, dynamic> json) => ModelSideJob(
        idJob: json["id_job"],
        idJobseeker: json["id_jobseeker"],
        idJobprovider: json["id_jobprovider"],
        name: json["name"],
        fotoProfil: json["foto_profil"],
        bio: json["bio"],
        jobName: json["job_name"],
        jobCategory: json["job_category"],
        jobDate: DateTime.parse(json["job_date"]),
        jobDescription: json["job_description"],
        fee: json["fee"],
        persyaratan: json["persyaratan"],
        fasilitas: json["fasilitas"],
        distance: json["distance"],
        jadwal: json["jadwal"],
        hoursStart: json["hours_start"],
        hoursEnd: json["hours_end"],
        jobPhotovideo: json["job_photovideo"],
        street: json["street"],
        city: json["city"],
        xJPin: json["X(j.pin)"],
        yJPin: json["Y(j.pin)"],
      );

  Map<String, dynamic> toJson() => {
        "id_job": idJob,
        "id_jobseeker": idJobseeker,
        "id_jobprovider": idJobprovider,
        "name": name,
        "foto_profil": fotoProfil,
        "bio": bio,
        "job_name": jobName,
        "job_category": jobCategory,
        "job_date": jobDate.toIso8601String(),
        "job_description": jobDescription,
        "fee": fee,
        "persyaratan": persyaratan,
        "fasilitas": fasilitas,
        "distance": distance,
        "jadwal": jadwal,
        "hours_start": hoursStart,
        "hours_end": hoursEnd,
        "job_photovideo": jobPhotovideo,
        "street": street,
        "city": city,
        "X(j.pin)": xJPin,
        "Y(j.pin)": yJPin,
      };

  @override
  List<Object> get props => [
        idJob,
        idJobseeker,
        idJobprovider,
        name,
        fotoProfil,
        bio,
        jobName,
        jobCategory,
        jobDate,
        jobDescription,
        fee,
        persyaratan,
        fasilitas,
        distance,
        jadwal,
        hoursStart,
        hoursEnd,
        jobPhotovideo,
        street,
        city,
        xJPin,
        yJPin
      ];
}
