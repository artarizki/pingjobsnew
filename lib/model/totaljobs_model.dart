// To parse this JSON data, do
//
//     final modelTotalJobs = modelTotalJobsFromJson(jsonString);

import 'dart:convert';

List<ModelTotalJobs> modelTotalJobsFromJson(String str) =>
    List<ModelTotalJobs>.from(
        json.decode(str).map((x) => ModelTotalJobs.fromJson(x)));

String modelTotalJobsToJson(List<ModelTotalJobs> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelTotalJobs {
  ModelTotalJobs({
    this.name,
    this.mobilenumber,
    this.jobApplied,
    this.jobPosted,
    this.createdAt,
  });

  String name;
  String mobilenumber;
  String jobApplied;
  String jobPosted;
  DateTime createdAt;

  factory ModelTotalJobs.fromJson(Map<String, dynamic> json) => ModelTotalJobs(
        name: json['name'],
        mobilenumber: json['mobilenumber'],
        jobApplied: json["job_applied"],
        jobPosted: json["job_posted"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "mobilenumber": mobilenumber,
        "job_applied": jobApplied,
        "job_posted": jobPosted,
        "created_at": createdAt.toIso8601String(),
      };
}
