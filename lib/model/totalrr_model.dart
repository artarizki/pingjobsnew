// To parse this JSON data, do
//
//     final totalRrModel = totalRrModelFromJson(jsonString);

import 'dart:convert';

List<TotalRrModel> totalRrModelFromJson(String str) => List<TotalRrModel>.from(json.decode(str).map((x) => TotalRrModel.fromJson(x)));

String totalRrModelToJson(List<TotalRrModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TotalRrModel {
  TotalRrModel({
    this.totalrating,
    this.ratingtype,
    this.totalreview,
  });

  String totalrating;
  String ratingtype;
  String totalreview;

  factory TotalRrModel.fromJson(Map<String, dynamic> json) => TotalRrModel(
    totalrating: json["totalrating"],
    ratingtype: json["ratingtype"],
    totalreview: json["totalreview"],
  );

  Map<String, dynamic> toJson() => {
    "totalrating": totalrating,
    "ratingtype": ratingtype,
    "totalreview": totalreview,
  };
}
