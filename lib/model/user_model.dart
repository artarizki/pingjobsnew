class UserModel {
  int id;
  String name;
  String email;
  String password;
  String imageUrl;
  int ratings;
  String reviews;
  int jobApplied;
  int jobPosted;
  String specialization;
  String mobilenumber;
  String bio;
  DateTime createdAt;

  UserModel(
      {this.id,
      this.name,
      this.email,
      this.imageUrl,
      this.password,
      this.ratings,
      this.reviews,
      this.jobApplied,
      this.jobPosted,
      this.specialization,
      this.mobilenumber,
      this.bio,
      this.createdAt});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    imageUrl = json['image_url'];
    ratings = json["ratings"];
    reviews = json["reviews"];
    jobApplied = json["job_applied"];
    jobPosted = json["job_posted"];
    specialization = json["specialization"];
    mobilenumber = json["mobilenumber"];
    bio = json["bio"];
    createdAt = DateTime.parse(json["created_at"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['image_url'] = this.imageUrl;
    data['ratings'] = this.ratings;
    data['reviews'] = this.reviews;
    data['jobApplied'] = this.jobApplied;
    data['jobPosted'] = this.jobPosted;
    data['specialization'] = this.specialization;
    data['mobilenumber'] = this.mobilenumber;
    data['bio'] = this.bio;
    data['created_at'] = this.createdAt;
    return data;
  }
}
