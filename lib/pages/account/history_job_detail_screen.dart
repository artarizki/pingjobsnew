import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/money_input_enums.dart';
import 'package:optimized_cached_image/optimized_cached_image.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/history_model.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

class DetailHistoryJob extends StatefulWidget {
  const DetailHistoryJob({Key key, this.historyData, this.datasend})
      : super(key: key);

  final ModelHistory historyData;
  final Map datasend;

  @override
  _DetailHistoryJobState createState() =>
      _DetailHistoryJobState(historyData, datasend);
}

class _DetailHistoryJobState extends State<DetailHistoryJob> {
  ModelHistory historyData;
  Map datasend;

  _DetailHistoryJobState(this.historyData, this.datasend);

  String name;
  var fotoProfil;
  String media = 'assets/images/untitled.jpg';
  String mediaUrlUsers =
      'https://bertigagroup.com/pj.com/public/storage/images/users/';
  String mediaUrlSideJobs =
      'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    fetchData(datasend);
  }

  //Connection to api webservice database
  fetchData(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_namephoto.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var responsename = json.decode(response.body);
    name = responsename['name'].toString();
    fotoProfil = responsename["foto_profil"];
    setState(() {
      name = responsename['name'].toString();
      fotoProfil = responsename["foto_profil"];
    });
    return name + fotoProfil;
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Riwayat Penilaian'),
      backgroundColor: Style.primaryColor,
      brightness: Brightness.dark,
      centerTitle: true,
      automaticallyImplyLeading: true,
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Widget _nameNotNull() {
    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.only(top: 32),
                width: MediaQuery.of(context).size.width * 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(24.0),
                  ),
                  child: AspectRatio(
                    aspectRatio: 1.5,
                    child: OptimizedCacheImage(
                      imageUrl: mediaUrlSideJobs + historyData.jobPhotovideo,
                      progressIndicatorBuilder: (context, url, downloadProgress) =>
                          Container(
                            height: 48,
                            child: Center(
                              child: LinearProgressIndicator(
                                value: downloadProgress.progress,
                              ),
                            ),
                          ),
                      errorWidget: (context, url, error) => Image.asset(media),
                      fit: BoxFit.cover,
                    ),
                    // child: historyData.jobPhotovideo == null
                    //     ? Image.asset(
                    //         media,
                    //       )
                    //     : historyData.jobPhotovideo.contains(',')
                    //         ? Image.network(
                    //             mediaUrlSideJobs +
                    //                 widget.historyData.jobPhotovideo.substring(
                    //                     0,
                    //                     historyData.jobPhotovideo.indexOf(',')),
                    //             fit: BoxFit.cover)
                    //         : Image.network(mediaUrlSideJobs +
                    //             widget.historyData.jobPhotovideo),
                  ),
                ),
              ),
            ),
            Center(
              child: Container(
                padding: EdgeInsets.only(top: 16),
                child: Text(historyData.jobName, style: Style.appStyle20Bold),
              ),
            ),
            SizedBox(
              height: 24,
            ),
            ListTile(
              title: Text(
                historyData.role == 'pekerja'
                    ? 'Diposting oleh'
                    : 'Dikerjakan oleh',
              ),
              trailing: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(name, style: Style.appStyle16400),
                  SizedBox(
                    width: 8,
                  ),
                  CircleAvatar(
                    maxRadius: 16,
                    backgroundImage: fotoProfil == null
                        ? NetworkImage(
                            'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                        : NetworkImage(mediaUrlUsers + fotoProfil),
                  )
                ],
              ),
            ),
            ListTile(
              title: Text('Gaji'),
              trailing: Text(
                  'Rp. ${toCurrencyString(historyData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}'),
            ),
            ListTile(
              title: Text('Tanggal Mulai Pekerjaan'),
              trailing: Text(
                  '${historyData.jobDate.toString().substring(8, 10)}-${historyData.jobDate.toString().substring(5, 7)}-${historyData.jobDate.toString().substring(0, 4)}'),
            ),
            ListTile(
              title: Text('Jadwal/Periode'),
              trailing: Text('${historyData.jadwal}'),
            ),
            ListTile(
              title: Text('Jam'),
              trailing: Text('${historyData.hoursStart}-${historyData.hoursEnd}'),
            ),
            historyData.ketepatanWaktu != null
                ? ListTile(
                    title: Text('Ketepatan Waktu'),
                    trailing: CircleAvatar(
                      maxRadius: 16,
                      backgroundImage: AssetImage(
                          historyData.ketepatanWaktu == "1"
                              ? 'assets/icons/check.png'
                              : 'assets/icons/close.png'),
                    ),
                  )
                : SizedBox(),
            historyData.sikapPerilaku != null
                ? ListTile(
                    title: Text('Sikap dan perilaku'),
                    trailing: CircleAvatar(
                      maxRadius: 16,
                      backgroundImage: AssetImage(
                          historyData.sikapPerilaku == "1"
                              ? 'assets/icons/check.png'
                              : 'assets/icons/close.png'),
                    ),
                  )
                : SizedBox(),
            ListTile(
              title: Text('Rating'),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    historyData.bintang,
                    textAlign: TextAlign.left,
                    style: Style.appStyle16Bold,
                  ),
                  Icon(
                    Icons.star,
                    color: Colors.yellow,
                    size: 32,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Kritik dan Saran',
                    style: Style.appStyle16,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(historyData.keterangan)
                ],
              ),
            ),
            SizedBox(
              height: 8,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: name == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: _nameNotNull(),
            ),
    );
  }
}
