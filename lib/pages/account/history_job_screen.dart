import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/history_model.dart';
import 'package:pingjobs/pages/account/history_job_detail_screen.dart';
import 'package:pingjobs/provider/history_provider.dart';
import 'package:pingjobs/provider/ratingreview_profile_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:translator/translator.dart';

class History extends StatefulWidget {
  const History({Key key, this.idUser}) : super(key: key);

  final String idUser;

  @override
  _HistoryState createState() => _HistoryState(idUser);
}

class _HistoryState extends State<History> {
  String idUser;
  GoogleTranslator translator = GoogleTranslator();
  var date;

  _HistoryState(this.idUser);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
  }

  Future<String> translateDate(String startDate) async {
    var dateResult =
        await translator.translate(startDate, from: 'en', to: 'id');
    print("TANGGAL : ");
    print(dateResult.text);
    return dateResult.text;
  }

  void trans(var startDate) {
    translator.translate(startDate, to: "id").then((value) {
      setState(() {
        date = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = {"id_user": idUser};
    final HistoryVM historyVM = Provider.of<HistoryVM>(context);
    final RatingReviewProfileVM ratingReviewProfileVM =
        Provider.of<RatingReviewProfileVM>(context);
    historyVM.fetchData(data);
    ratingReviewProfileVM.fetchData(data);
    // print("Data RR Profil : ${ratingReviewProfileVM.listRatingReviewProfile}");
    // print("Data RHistory : ${historyVM.listHistory}");
    var provider = Provider.of<UserProvider>(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('Riwayat Pekerjaan'),
          backgroundColor: Style.primaryColor,
          brightness: Brightness.dark,
          centerTitle: true,
          automaticallyImplyLeading: true,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: (ratingReviewProfileVM?.listRatingReviewProfile?.isEmpty ?? true) ||
              ratingReviewProfileVM?.listRatingReviewProfile == null
          ? Center(
              child: Container(
                  width: 250,
                  height: 250,
                  child: Image.asset(
                      'assets/images/dataerror_riwayatpekerjaan.png')),
            )
          : provider.busy || historyVM.listHistory == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  color: Style.bgColor,
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 8, bottom: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              CircleAvatar(
                                maxRadius: 8,
                                backgroundColor: Colors.blueAccent,
                              ),
                              Text('Pencari Kerja'),
                              SizedBox(width: 24),
                              CircleAvatar(
                                maxRadius: 8,
                                backgroundColor: Colors.green,
                              ),
                              Text('Penyedia Kerja'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Padding(
                              padding: EdgeInsets.only(top: 0, bottom: 0.00001),
                              child: historyVM.listHistory == null
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : Container(
                                      color: Style.bgColor,
                                      child: ListView.builder(
                                        itemCount:
                                            historyVM?.listHistory?.length ?? 0,
                                        padding: const EdgeInsets.only(
                                            top: 8, bottom: 8),
                                        scrollDirection: Axis.vertical,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          final ModelHistory data =
                                              historyVM.listHistory[index];
                                          data.jobPhotovideo =
                                              data.jobPhotovideo.contains(',')
                                                  ? data.jobPhotovideo
                                                      .substring(
                                                          0,
                                                          data.jobPhotovideo
                                                              .indexOf(','))
                                                  : data.jobPhotovideo;
                                          // trans(data.startDate);
                                          var datasend = {
                                            "id": data.role == 'pekerja'
                                                ? data.idJobprovider
                                                : data.idJobseeker
                                          };
                                          return Card(
                                              elevation: 10,
                                              color: Colors.white,
                                              child: ListTile(
                                                leading: Padding(
                                                  padding:
                                                      EdgeInsets.only(left: 4),
                                                  child: Text(
                                                    data.startDate == null
                                                        ? 'Memuat\n...'
                                                        : data.startDate[1] ==
                                                                ' '
                                                            ? '${data.startDate.substring(0, 2)}\n${data.startDate.substring(2, 5)}'
                                                            : '${data.startDate.substring(0, 3)}\n${data.startDate.substring(3, 5)}',
                                                    textAlign: TextAlign.center,
                                                    style: Style
                                                        .appStyle20BoldpColor,
                                                  ),
                                                ),
                                                title: Text(
                                                  data.jobName,
                                                  style: TextStyle(
                                                      color: data.role ==
                                                              'pekerja'
                                                          ? Colors.blueAccent
                                                          : Colors.green,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                subtitle:
                                                    Text(data.jobDescription),
                                                trailing: Icon(
                                                    Icons.chevron_right,
                                                    color: Colors.black),
                                                onTap: () => Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (_) =>
                                                            DetailHistoryJob(
                                                              historyData: data,
                                                              datasend:
                                                                  datasend,
                                                            ))),
                                              ));
                                        },
                                      ),
                                    )),
                        )
                      ],
                    ),
                  ),
                ),
    );
  }
}
