import 'package:flutter/material.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/activity/detail_activity_jobseeker_screen.dart';
import 'package:pingjobs/pages/activity/detail_activity_jobprovider_screen.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/sidejobs/activity/activity_info.dart';

class AktivitasListView extends StatelessWidget {
  const AktivitasListView(
      {Key key,
      this.callback,
      this.aktivitasData,
      this.animationController,
      this.animation})
      : super(key: key);

  final VoidCallback callback;
  final ModelAktivitas aktivitasData;
  final AnimationController animationController;
  final Animation<dynamic> animation;

  checkStatus(Color color) {
    switch (aktivitasData.status) {
      case "berlangsung":
        {
          color = Colors.blueAccent;
        }
        break;
      case "selesai":
        {
          color = Colors.green;
        }
        break;
      case "batal":
        {
          color = Colors.red;
        }
        break;
      default:
        {
          color = Colors.black;
        }
        break;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    String status = aktivitasData.status;
    status = status[0].toUpperCase() + status.substring(1);
    Color color;
    color = checkStatus(color);
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, top: 8, bottom: 16),
              child: InkWell(
                splashColor: Colors.transparent,
                onTap: () {
                  callback();
                  aktivitasData.role == 'pekerja'
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailAktivitasPekerja(
                              idJob: aktivitasData.idJob,
                              aktivitasData: aktivitasData,
                            ),
                          ),
                        )
                      : Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailAktivitasPenyedia(
                              idJob: aktivitasData.idJob,
                              aktivitasData: aktivitasData,
                            ),
                          ),
                        );
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: <BoxShadow>[Style.shadowElement],
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    child: Stack(
                      children: <Widget>[
                        ActivityInfo(aktivitasData: aktivitasData),
                        Positioned(
                          top: 8,
                          right: 16,
                          bottom: 8,
                          child: Center(
                            child: Text(
                              status,
                              style: TextStyle(
                                color: color,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
