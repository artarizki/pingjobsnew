import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/activity/activity_list_view.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/provider/activity_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AktivitasPage extends StatefulWidget {
  const AktivitasPage({Key key, this.animationController, this.backStatus})
      : super(key: key);

  Size get preferredSize => Size.fromHeight(56.0);

  final AnimationController animationController;
  final bool backStatus;

  @override
  _AktivitasPageState createState() =>
      _AktivitasPageState(animationController, backStatus);
}

class _AktivitasPageState extends State<AktivitasPage>
    with TickerProviderStateMixin {
  AnimationController animationController;
  bool backStatus;
  String token;

  _AktivitasPageState(this.animationController, this.backStatus);

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
    super.initState();
    getPref();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(
      const Duration(milliseconds: 200),
    );
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    final AktivitasVM aktivitasVM = Provider.of<AktivitasVM>(context);
    var data = {"id_user": userProvider.user.id};
    aktivitasVM.fetchData(data);
    // print("Data Aktivitas : ${aktivitasVM.listAktivitas}");
    return ChangeNotifierProvider<AktivitasVM>.value(
      value: AktivitasVM(),
      child: Scaffold(
        appBar: AppBar(
            title: Text('Aktivitas'),
            backgroundColor: Style.primaryColor,
            brightness: Brightness.dark,
            centerTitle: true,
            automaticallyImplyLeading: backStatus),
        body: token != null
            ? (aktivitasVM?.listAktivitas?.isEmpty ?? true)
                ? Center(
                    child: Container(
                      width: 250,
                      height: 250,
                      child:
                          Image.asset('assets/images/dataerror_aktivitas.png'),
                    ),
                  )
                : aktivitasVM.listAktivitas == null
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : WillPopScope(
                        onWillPop: () async => backStatus,
                        child: Stack(
                          children: <Widget>[
                            InkWell(
                              splashColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              onTap: () {
                                SnackBar(
                                  content: Text(
                                    'Ini List Aktivitas',
                                    style: Style.appStyleDefault,
                                  ),
                                );
                              },
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 0.0, bottom: 0.00001),
                                      child: Container(
                                        color: Style.bgColor,
                                        child: ListView.builder(
                                          itemCount: aktivitasVM
                                                  ?.listAktivitas?.length ??
                                              0,
                                          padding: const EdgeInsets.only(
                                              top: 8, bottom: 8),
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            final ModelAktivitas data =
                                                aktivitasVM
                                                    .listAktivitas[index];
                                            data.jobPhotovideo =
                                                data.jobPhotovideo.contains(',')
                                                    ? data.jobPhotovideo
                                                        .substring(
                                                            0,
                                                            data.jobPhotovideo
                                                                .indexOf(','))
                                                    : data.jobPhotovideo;
                                            final int count = aktivitasVM
                                                        .listAktivitas.length >
                                                    10
                                                ? 10
                                                : aktivitasVM
                                                    .listAktivitas.length;
                                            final Animation<double> animation =
                                                Tween<double>(
                                                        begin: 0.0, end: 1.0)
                                                    .animate(CurvedAnimation(
                                              parent: animationController,
                                              curve: Interval(
                                                  (1 / count) * index, 1.0,
                                                  curve: Curves.fastOutSlowIn),
                                            ));
                                            animationController.forward();
                                            return AktivitasListView(
                                              callback: () {},
                                              aktivitasData: data,
                                              animation: animation,
                                              animationController:
                                                  animationController,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 250,
                      child:
                          Image.asset('assets/images/aktivitasloginerror.png'),
                    ),
                    Container(
                      width: 200,
                      child: ElevatedButton.icon(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Login(),
                          ),
                        ),
                        icon: Icon(
                          Icons.login_outlined,
                          color: Colors.white,
                        ),
                        label: Text('Login'),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(10),
                          primary: Style.primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
