import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/report/report_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/jp/detail_activity_jp_aboutjob.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/jp/detail_activity_jp_anotheruser.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/jp/detail_activity_jp_note.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/jp/detail_activity_jp_ratingreview.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/jp/detail_activity_jp_ratingreview2.dart';
import 'package:pingjobs/provider/postjob_provider.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetailAktivitasPenyedia extends StatefulWidget {
  const DetailAktivitasPenyedia({Key key, this.idJob, this.aktivitasData})
      : super(key: key);

  final String idJob;
  final ModelAktivitas aktivitasData;

  @override
  _DetailAktivitasPenyediaState createState() =>
      _DetailAktivitasPenyediaState(idJob, aktivitasData);
}

class _DetailAktivitasPenyediaState extends State<DetailAktivitasPenyedia> {
  TextEditingController _textController = new TextEditingController();
  FocusNode nodeText = FocusNode();

  final String idJob;
  final ModelAktivitas aktivitasData;
  String fotoProfil;
  bool tap = false;

  _DetailAktivitasPenyediaState(this.idJob, this.aktivitasData);

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("IDJOBSEEKER : ");
    print(aktivitasData.idJobseeker);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    _textController.addListener(_printLatestValue);
  }

  _printLatestValue() {
    print("Text field: ${_textController.text}");
  }

  getUserPhoto(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_userphoto.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var result = json.decode(response.body);
    fotoProfil = result["foto_profil"];
    // print("FOTOPROFIL");
    // print(fotoProfil);
    // setState(() {
    //   fotoProfil = result["foto_profil"];
    // });
    return fotoProfil;
  }

  checkStatus(Color color) {
    switch (aktivitasData.status) {
      case "berlangsung":
        {
          color = Colors.blueAccent;
        }
        break;
      case "selesai":
        {
          color = Colors.green;
        }
        break;
      case "batal":
        {
          color = Colors.red;
        }
        break;
      default:
        {
          color = Colors.black;
        }
        break;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    final PostJobVM postJobVM = Provider.of<PostJobVM>(context);
    final SideJobVM sideJobVM = Provider.of<SideJobVM>(context);
    final TotalJobsVM totalJobsVM = Provider.of<TotalJobsVM>(context);
    final RatingReviewVM rrVM = Provider.of<RatingReviewVM>(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);

    totalJobsVM.getTotalJobs(aktivitasData.idJobseeker);
    var data2 = {
      "id_job": aktivitasData.idJob,
      "id_user": userProvider.user.id,
    };
    rrVM.fetchData(data2);
    var data3 = {
      "id": aktivitasData.idJobseeker,
    };
    if (aktivitasData.idJobseeker == null) getUserPhoto(data3);

    Color color;
    color = checkStatus(color);

    return Container(
      height: MediaQuery.of(context).size.height,
      color: Style.bgColor,
      child: Scaffold(
        appBar: _buildAppBar(context),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: rrVM.listRatingReview == null ||
                  aktivitasData == null ||
                  totalJobsVM.listTotalJobs == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        DetailActivityJpAboutJob(
                          aktivitasData: aktivitasData,
                          totalJobsVM: totalJobsVM,
                          fotoProfil: fotoProfil,
                        ),
                        DetailActivityJpNote(aktivitasData: aktivitasData),
                        DetailActivityJpAnotherUser(
                          aktivitasData: aktivitasData,
                          totalJobsVM: totalJobsVM,
                          fotoProfil: fotoProfil,
                        ),
                        DetailActivityJpRatingReview(
                          aktivitasData: aktivitasData,
                          rrVM: rrVM,
                        ),
                        DetailActivityJpRatingReview2(
                          aktivitasData: aktivitasData,
                          rrVM: rrVM,
                        ),
                        _buildConfirmButton(sideJobVM),
                        _buildCancelButton(postJobVM),
                        _buildReportButton()
                      ],
                    )
                  ],
                ),
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Detail Aktivitas'),
      backgroundColor: Style.primaryColor,
      brightness: Brightness.dark,
      centerTitle: true,
      automaticallyImplyLeading: true,
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Widget _buildConfirmButton(SideJobVM sideJobVM) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: aktivitasData.status == 'selesai' ||
              aktivitasData.status == 'batal' ||
              aktivitasData.idJobseeker == null
          ? SizedBox(height: 0)
          : SizedBox(
              child: Container(
                width: double.infinity,
                child: ElevatedButton.icon(
                  onPressed: () {
                    var data = {"id_job": idJob, "id_jobseeker": aktivitasData.idJobseeker};
                    return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text('Anda yakin pekerjaan ini selesai ?'),
                            actions: <Widget>[
                              ElevatedButton(
                                child: Text(
                                  "Ya",
                                  style: Style.appStylewhiteDefaultBold,
                                ),
                                onPressed: () async => sideJobVM.finishData(
                                    data, context, aktivitasData),
                                style: ElevatedButton.styleFrom(
                                  primary: Style.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                child: Text(
                                  "Batal",
                                  style: Style.appStyleDefaultBoldRed,
                                ),
                                onPressed: () => Navigator.pop(context),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side:
                                        BorderSide(color: Style.secondaryColor),
                                  ),
                                ),
                              ),
                            ],
                          );
                        });
                  },
                  icon: Icon(FontAwesomeIcons.check),
                  label: Text('Konfirmasi Penyediaan Selesai'),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.lightGreen,
                  ),
                ),
              ),
            ),
    );
  }

  Widget _buildReportButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: Container(
        width: double.infinity,
        child: ElevatedButton.icon(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) {
                return Report(aktivitasData: aktivitasData);
              }),
            );
          },
          icon: Icon(Icons.warning_amber_rounded),
          label: Text('Laporkan'),
          style: ElevatedButton.styleFrom(
            primary: Style.primaryColor,
          ),
        ),
      ),
    );
  }

  Widget _buildCancelButton(PostJobVM postJobVM) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
      child:
          aktivitasData.status == 'selesai' || aktivitasData.status == 'batal'
              ? SizedBox(height: 0)
              : SizedBox(
                  child: Container(
                    width: double.infinity,
                    child: ElevatedButton.icon(
                      onPressed: () {
                        var cancel = {"id_job": idJob};
                        print("ID_JOB CANCEL");
                        print(cancel);
                        var message = postJobVM.cancelData(cancel, context);
                      },
                      icon: Icon(Icons.cancel_outlined),
                      label: Text('Batalkan Pekerjaan'),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.redAccent,
                      ),
                    ),
                  ),
                ),
    );
  }
}
