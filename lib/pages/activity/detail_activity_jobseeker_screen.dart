import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/report/report_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/js/detail_activity_js_aboutjob.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/js/detail_activity_js_anotheruser.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/js/detail_activity_js_note.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/js/detail_activity_js_ratingreview.dart';
import 'package:pingjobs/pages/sidejobs/detailactivity/js/detail_activity_js_ratingreview2.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetailAktivitasPekerja extends StatefulWidget {
  const DetailAktivitasPekerja({Key key, this.idJob, this.aktivitasData})
      : super(key: key);

  final String idJob;
  final ModelAktivitas aktivitasData;

  @override
  _DetailAktivitasPekerjaState createState() =>
      _DetailAktivitasPekerjaState(idJob, aktivitasData);
}

class _DetailAktivitasPekerjaState extends State<DetailAktivitasPekerja> {
  final String idJob;
  final ModelAktivitas aktivitasData;
  var fotoProfil;

  _DetailAktivitasPekerjaState(this.idJob, this.aktivitasData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
  }

  getUserPhoto(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_userphoto.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var result = jsonDecode(response.body);
    fotoProfil = result["foto_profil"];
    return fotoProfil;
  }

  checkStatus(Color color) {
    switch (aktivitasData.status) {
      case "berlangsung":
        {
          color = Colors.blueAccent;
        }
        break;
      case "selesai":
        {
          color = Colors.green;
        }
        break;
      case "batal":
        {
          color = Colors.red;
        }
        break;
      default:
        {
          color = Colors.black;
        }
        break;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    final TotalJobsVM totalJobsVM = Provider.of<TotalJobsVM>(context);
    final RatingReviewVM rrVM = Provider.of<RatingReviewVM>(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    var data = {"id": aktivitasData.idJobprovider};
    totalJobsVM.getTotalJobs(data);
    var data2 = {
      "id_job": aktivitasData.idJob,
      "id_user": userProvider.user.id,
    };
    rrVM.fetchData(data2);
    var data3 = {
      "id": aktivitasData.idJobprovider,
    };
    getUserPhoto(data3);

    Color color;
    color = checkStatus(color);

    return Container(
      height: MediaQuery
          .of(context)
          .size
          .height,
      color: Style.bgColor,
      child: Scaffold(
        appBar: _buildAppBar(context),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: rrVM.listRatingReview == null ||
              aktivitasData == null ||
              totalJobsVM.listTotalJobs == null
              ? Center(
            child: CircularProgressIndicator(),
          )
              : Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  DetailActivityJsAboutJob(
                    aktivitasData: aktivitasData,
                    totalJobsVM: totalJobsVM,
                    fotoProfil: fotoProfil,
                    color: color,
                  ),
                  DetailActivityJsNote(aktivitasData: aktivitasData),
                  DetailActivityJsAnotherUser(
                    aktivitasData: aktivitasData,
                    totalJobsVM: totalJobsVM,
                    fotoProfil: fotoProfil,
                  ),
                  DetailActivityJsRatingReview(
                    aktivitasData: aktivitasData,
                    rrVM: rrVM,
                  ),
                  DetailActivityJsRatingReview2(
                    aktivitasData: aktivitasData,
                    rrVM: rrVM,
                  ),
                  // _buildConfirmButton(sideJobVM),
                  _buildReportButton(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Detail Aktivitas'),
      backgroundColor: Style.primaryColor,
      brightness: Brightness.dark,
      centerTitle: true,
      automaticallyImplyLeading: true,
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Widget _buildConfirmButton(SideJobVM sideJobVM) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: aktivitasData.status == 'selesai' ||
          aktivitasData.status == 'batal'
          ? SizedBox(height: 0)
          : SizedBox(
        child: Container(
          width: double.infinity,
          child: ElevatedButton.icon(
            onPressed: () {
              var data = {"id_job": idJob};
              sideJobVM.finishData(
                  data, context, aktivitasData);
            },
            icon: Icon(FontAwesomeIcons.check),
            label: Text(
                'Konfirmasi Pekerjaan Selesai'),
            style: ElevatedButton.styleFrom(
              primary: Colors.lightGreen,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildReportButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: Container(
        width: double.infinity,
        child: ElevatedButton.icon(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) {
                return Report(aktivitasData: aktivitasData);
              }),
            );
          },
          icon: Icon(Icons.warning_amber_rounded),
          label: Text('Laporkan'),
          style: ElevatedButton.styleFrom(
            primary: Style.primaryColor,
          ),
        ),
      ),
    );
  }
}
