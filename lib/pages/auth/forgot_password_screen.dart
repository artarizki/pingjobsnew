import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:pingjobs/pages/auth/register_screen.dart';
import 'package:pingjobs/provider/auth_provider2.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  String email, password, konfirmasipassword;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final konfirmasipasswordController = TextEditingController();

  bool _secureText = true;
  bool _secureText2 = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  showHide2() {
    setState(() {
      _secureText2 = !_secureText2;
    });
  }

  Future<void> showProgress(BuildContext context, var forgotpassword) async {
    final AuthVM authVM = Provider.of<AuthVM>(context, listen: false);
    await showDialog(
      context: context,
      builder: (context) => FutureProgressDialog(
        authVM.userForgotPassword(context, forgotpassword),
        message: Text('Loading...'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Center(
                child: Container(
                  height: 150,
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/logos/logobl.png'),
                        fit: BoxFit.fill),
                  ),
                ),
              ),
              SizedBox(height: 16),
              Text('Masukkan Form Untuk Buat Password Baru'),
              SizedBox(height: 8),
              TextFormField(
                controller: emailController,
                validator: (value) {
                  String response;
                  if (value.isEmpty) response = "Harap Masukkan Email Anda";
                  return response;
                },
                onSaved: (value) => email = value,
                decoration: InputDecoration(
                  labelText: 'Email',
                  // fillColor: Color(0xfff3f3f4),
                  // filled: true
                ),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: passwordController,
                validator: (value) {
                  String response;
                  if (value.isEmpty) response = "Harap Masukkan Password Anda";
                  return response;
                },
                onSaved: (value) => password = value,
                obscureText: _secureText,
                decoration: InputDecoration(
                  labelText: "Password",
                  suffixIcon: IconButton(
                    onPressed: showHide,
                    icon: Icon(
                        _secureText ? Icons.visibility_off : Icons.visibility),
                  ),
                  // fillColor: Color(0xfff3f3f4),
                  //     filled: true
                ),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: konfirmasipasswordController,
                validator: (value) {
                  String response;
                  if (value.isEmpty)
                    response = "Harap masukkan Konfirmasi Password Anda";
                  return response;
                },
                onSaved: (value) => konfirmasipassword = value,
                obscureText: _secureText2,
                decoration: InputDecoration(
                  labelText: "Konfirmasi Password",
                  suffixIcon: IconButton(
                    onPressed: showHide2,
                    icon: Icon(
                        _secureText2 ? Icons.visibility_off : Icons.visibility),
                  ),
                  // fillColor: Color(0xfff3f3f4),
                  // filled: true
                ),
              ),
              SizedBox(height: 24),
              SizedBox(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                  ),
                  width: double.infinity,
                  child: ElevatedButton(
                    style:
                        ElevatedButton.styleFrom(primary: Style.primaryColor),
                    onPressed: () {
                      var forgotpassword = {
                        "email": emailController.text,
                        "password": passwordController.text,
                        "confirmpassword": konfirmasipasswordController.text,
                      };
                      showProgress(context, forgotpassword);
                    },
                    child: Text('Buat Password Baru'),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16, 16, 8, 8),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Belum Punya Akun ?',
                      style: TextStyle(fontSize: 12),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Register(),
                          ),
                        );
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(
                            color: Style.secondaryColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16, 8, 8, 8),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Lupa Password  ?',
                      style: TextStyle(fontSize: 12),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    InkWell(
                      onTap: () {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Berhasil Ganti Password'),
                          ),
                        );
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Register(),
                          ),
                        );
                      },
                      child: Text(
                        'Klik di sini',
                        style: TextStyle(
                            color: Style.secondaryColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
