import 'package:flutter/material.dart';
import 'package:pingjobs/pages/auth/forgot_password_screen.dart';
import 'package:pingjobs/pages/auth/register_screen.dart';
import 'package:pingjobs/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  String email, password;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  static bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scafoldKey,
      body: Container(
        padding: EdgeInsets.all(16),
        height: MediaQuery.of(context).size.height,
        child: Provider.of<AuthProvider>(context).busy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Form(
                  key: _key,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 75),
                      Center(
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/logos/logobl.png'),
                                fit: BoxFit.fill),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      Text('Masukkan Form Untuk Login'),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap Masukkan Email Anda";
                          else if (!isEmail(value)) return 'email invalide';
                          return null;
                        },
                        onSaved: (value) => email = value,
                        decoration: InputDecoration(
                          labelText: 'Email Anda',
                        ),
                      ),
                      SizedBox(height: 16),
                      TextFormField(
                        keyboardType: TextInputType.visiblePassword,
                        controller: passwordController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Harap masukkan Password Anda";
                          } else if (value.trim().length < 6) {
                            return 'Password terlalu pendek';
                          } else
                            return null;
                        },
                        onSaved: (value) => password = value,
                        obscureText: _secureText,
                        decoration: InputDecoration(
                          labelText: "Password Anda",
                          suffixIcon: IconButton(
                            onPressed: showHide,
                            icon: Icon(_secureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      SizedBox(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                          ),
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Style.primaryColor),
                            onPressed: () async {
                              if (_key.currentState.validate()) {
                                _key.currentState.save();
                                print('is validate');
                                var login = await Provider.of<AuthProvider>(
                                        context,
                                        listen: false)
                                    .login(emailController.text,
                                        passwordController.text);
                                print("SUDAHH?");
                                if (login) {
                                  await Provider.of<AuthProvider>(context,
                                          listen: false)
                                      .getUser();
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Berhasil Login'),
                                    ),
                                  );
                                  Navigator.pushReplacementNamed(context, '/');
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(Provider.of<AuthProvider>(
                                              context,
                                              listen: false)
                                          .message),
                                    ),
                                  );
                                }
                              } else
                                print('is not validate');
                            },
                            child: Text('Login'),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(16, 16, 8, 8),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Belum Punya Akun ?',
                              style: TextStyle(fontSize: 12),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Register(),
                                  ),
                                );
                              },
                              child: Text(
                                'Register',
                                style: TextStyle(
                                    color: Style.secondaryColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(16, 8, 8, 8),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Lupa Password ?',
                              style: TextStyle(fontSize: 12),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ForgotPassword(),
                                  ),
                                );
                              },
                              child: Text(
                                'Klik di sini',
                                style: TextStyle(
                                    color: Style.secondaryColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}