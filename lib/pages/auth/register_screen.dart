import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  String nama, email, password, konfirmasipassword, spesialis, nomorHP, bio;

  final namaController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final konfirmasipasswordController = TextEditingController();
  final spesialisController = TextEditingController();
  final nomorHPController = TextEditingController();
  final bioController = TextEditingController();

  FocusNode nodeNama;
  FocusNode nodeEmail;
  FocusNode nodePassword;
  FocusNode nodeKonfirmasiPassword;
  FocusNode nodeSpesialis;
  FocusNode nodeNomorHP;
  FocusNode nodeBio;

  @override
  void initState() {
    super.initState();
    nodeNama = FocusNode();
    nodeEmail = FocusNode();
    nodePassword = FocusNode();
    nodeKonfirmasiPassword = FocusNode();
    nodeSpesialis = FocusNode();
    nodeNomorHP = FocusNode();
    nodeBio = FocusNode();
  }

  @override
  void dispose() {
    nodeNama.dispose();
    nodeEmail.dispose();
    nodePassword.dispose();
    nodeKonfirmasiPassword.dispose();
    nodeSpesialis.dispose();
    nodeNomorHP.dispose();
    nodeBio.dispose();
    super.dispose();
  }

  bool _secureText = true;
  bool _secureText2 = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  showHide2() {
    setState(() {
      _secureText2 = !_secureText2;
    });
  }

  static bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<AuthProvider>(context);
    return Scaffold(
      key: _scafoldKey,
      body: Container(
        padding: EdgeInsets.all(16),
        height: MediaQuery.of(context).size.height,
        child: Provider.of<AuthProvider>(context).busy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Form(
                  key: _key,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 32),
                      Center(
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/logos/logobl.png'),
                                fit: BoxFit.fill),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      Text('Masukkan Form Untuk Daftar'),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.name,
                        controller: namaController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap Masukkan Nama Anda";
                          else
                            return null;
                        },
                        onSaved: (value) => provider.user.name = value.trim(),
                        decoration: InputDecoration(
                          labelText: 'Nama',
                        ),
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap Masukkan Email Anda";
                          else if (!isEmail(value)) return 'email invalid';
                          return null;
                        },
                        onSaved: (value) => provider.user.email = value,
                        decoration: InputDecoration(
                          labelText: 'Email',
                        ),
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.visiblePassword,
                        controller: passwordController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap masukkan Password Anda";
                          else if (value.trim().length < 6)
                            return 'this password is too short';
                          else
                            return null;
                        },
                        onSaved: (value) => provider.user.password = value,
                        obscureText: _secureText,
                        decoration: InputDecoration(
                          labelText: "Password",
                          suffixIcon: IconButton(
                            onPressed: showHide,
                            icon: Icon(_secureText
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.visiblePassword,
                        controller: konfirmasipasswordController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap masukkan Konfirmasi Password Anda";
                          else if (value.trim() !=
                              passwordController.text.trim())
                            return ' passwords not match';
                          return null;
                        },
                        onSaved: (value) => konfirmasipassword = value,
                        obscureText: _secureText2,
                        decoration: InputDecoration(
                          labelText: "Konfirmasi Password",
                          suffixIcon: IconButton(
                            onPressed: showHide2,
                            icon: Icon(_secureText2
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        ),
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          PhoneInputFormatter(
                            allowEndlessPhone: true,
                          )
                        ],
                        controller: nomorHPController,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Harap Masukkan Nomor HP Anda";
                          else
                            return null;
                        },
                        onSaved: (value) =>
                            provider.user.mobilenumber = value.trim(),
                        decoration: InputDecoration(
                          labelText: 'Nomor HP',
                        ),
                      ),
                      SizedBox(height: 24),
                      SizedBox(
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            ),
                          ),
                          width: double.infinity,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Style.primaryColor),
                            onPressed: () async {
                              if (_key.currentState.validate()) {
                                _key.currentState.save();
                                var result = await provider.register();
                                if (result) {
                                  print('done');
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Berhasil Daftar'),
                                    ),
                                  );
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Login(),
                                    ),
                                  );
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(Provider.of<AuthProvider>(
                                              context,
                                              listen: false)
                                          .message),
                                    ),
                                  );
                                  print('error');
                                }
                              } else
                                print('is not validate');
                            },
                            child: Text('Daftar'),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(16, 16, 8, 8),
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Sudah Punya Akun ?',
                              style: TextStyle(fontSize: 12),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Login(),
                                  ),
                                );
                              },
                              child: Text(
                                'Login',
                                style: TextStyle(
                                    color: Style.secondaryColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
