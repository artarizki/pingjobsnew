import 'package:flutter/services.dart';
import 'package:pingjobs/component/size_config.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/component/cards/conversation_card.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConversationsScreen extends StatefulWidget {
  @override
  _ConversationsScreenState createState() => _ConversationsScreenState();
}

class _ConversationsScreenState extends State<ConversationsScreen> {
  String token;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    super.initState();
    getPref();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var provider = Provider.of<ConversationProvider>(context);
    print("JUMLAH PROVIDER");
    print(provider.concersations.length);
    return Scaffold(
        backgroundColor: Style.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          // leading: new IconButton(
          //   icon: new Icon(Icons.arrow_back_ios),
          //   onPressed: () => Navigator.of(context).pop(),
          // ),
          brightness: Brightness.dark,
          backgroundColor: Style.primaryColor,
          title: Text('Percakapan'),
          centerTitle: true,
          // leading: IconButton(
          //   onPressed: () async {
          //     Provider.of<AuthProvider>(context, listen: false).logout();
          //     Navigator.push(context, MaterialPageRoute(builder: (context) {
          //       return Login();
          //     }));
          //     // Navigator.of(context).pushNamedAndRemoveUntil(
          //     //     WelcomeScreen.routeName, (Route<dynamic> route) => false);
          //   },
          //   icon: Icon(
          //     Icons.exit_to_app_outlined,
          //     color: Colors.white,
          //   ),
          // ),
        ),
        body: token != null
            ? (provider?.concersations?.isEmpty ?? true)
                ? Center(
                    child: Container(
                        width: 250,
                        height: 250,
                        child: Image.asset(
                            'assets/images/dataerror_percakapan.png')),
                  )
                : Center(
                    child: provider.busy
                        ? CircularProgressIndicator()
                        : ListView.builder(
                            padding: EdgeInsets.only(
                                top: SizeConfig.blockSizeVertical * 2),
                            itemCount: provider?.concersations?.length ?? 0,
                            itemBuilder: (context, index) => ConversationCard(
                                conversation: provider.concersations[index],
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => ChatScreen(
                                            conversation:
                                                provider.concersations[index],
                                          )));
                                }),
                          ),
                  )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 250,
                      child: Image.asset('assets/images/chatloginerror.png'),
                    ),
                    Container(
                      width: 200,
                      child: ElevatedButton.icon(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Login(),
                          ),
                        ),
                        icon: Icon(
                          Icons.login_outlined,
                          color: Colors.white,
                        ),
                        label: Text('Login'),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(10),
                          primary: Style.primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ));
  }
}
