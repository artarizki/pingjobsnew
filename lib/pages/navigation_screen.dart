import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:pingjobs/component/navigation.dart';
import 'package:pingjobs/model/message_model.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:pingjobs/service/message.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:pingjobs/component/style.dart';

/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);

/// Initialize the [FlutterLocalNotificationsPlugin] package.
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
}

class NavigationPage extends StatefulWidget {
  static final routeName = 'main';

  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  // deklarasi indeks halaman
  int _currentIndex = 0;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  @override
  initState() {
    super.initState();
    print("INIT");

    _firebaseMessaging.getToken().then((token) {
      print("TOKEN : " + token);
      Provider.of<UserProvider>(context, listen: false).setFcmToken(token);
    });
    notification();
  }

  notification() {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        Navigator.pushNamed(context, '/message',
            arguments: MessageArguments(message, true));
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      // print("ID MESSAGE : ${message.messageId.toString()}");
      print("MESSAGE NOTIFIKASI : ${message.data}");
      // print("ID NOTIFIKASI : ${message.data['id'].toString()}");
      // print("BODY NOTIFIKASI : ${message.data['body'].toString()}");
      // print("HASHCODE NOTIFIKASI : " + notification.hashCode.toString());
      // print("TITLE NOTIFIKASI : " + notification.title.toString());
      // print("BODY NOTIFIKASI : " + notification.body.toString());
      handleNotification(message.data, false);
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      Navigator.pushNamed(context, '/message',
          arguments: MessageArguments(message, true));
    });
  }

  handleNotification(data, bool push) {
    print("INI HANDLE");
    var messageJson = json.decode(data['message']);
    print("MESSAGE JSON : ");
    print(messageJson);
    var message = MessageModal.fromJson(messageJson);
    print("Message Modal From JSON : $message");
    Provider.of<ConversationProvider>(context, listen: false)
        .addMessageToConversation(message.conversationId, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: [
          for (final tabItem in TabNavigationItem.items) tabItem.pages,
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (int index) => setState(() => _currentIndex = index),
        items: [
          for (final tabItem in TabNavigationItem.items)
            BottomNavigationBarItem(
              backgroundColor: Style.primaryColor,
              icon: tabItem.icon,
              label: tabItem.title,
            )
        ],
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        selectedLabelStyle: Style.appStyleDefault,
        unselectedLabelStyle: Style.appStyleDefault,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        selectedItemColor: Style.secondaryColor,
        unselectedItemColor: Style.primaryColor,
        backgroundColor: Colors.white,
        elevation: 12,
      ),
    );
    // return ChangeNotifierProvider<SideJobVM>.value(
    //   value: SideJobVM(),
    //
    // );
  }
}
