import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/totaljobs_model.dart';
import 'package:pingjobs/pages/profile/ratingreview_profile_screen.dart';
import 'package:pingjobs/pages/report/report_screen.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:http/http.dart' as http;

class AnotherProfile extends StatefulWidget {
  const AnotherProfile({Key key, this.aktivitasData, this.totalJobsData})
      : super(key: key);
  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobsData;

  @override
  _AnotherProfileState createState() =>
      _AnotherProfileState(aktivitasData, totalJobsData);
}

class _AnotherProfileState extends State<AnotherProfile> {
  // final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobsData;
  var idUser;
  String name;
  var fotoProfil;
  String mediaUrlUsers =
      'https://bertigagroup.com/pj.com/public/storage/images/users/';

  _AnotherProfileState(this.aktivitasData, this.totalJobsData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkRole();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
  }

  checkRole() async {
    idUser = aktivitasData.role == "pekerja"
        ? aktivitasData.idJobprovider
        : aktivitasData.idJobseeker;
    await fetchData({"id": idUser});
  }

  fetchData(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_namephoto.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var responsename = json.decode(response.body);
    name = responsename['name'].toString();
    fotoProfil = responsename["foto_profil"];
    setState(() {
      name = responsename['name'].toString();
      fotoProfil = responsename["foto_profil"];
    });
    return name + fotoProfil;
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
        title: Text(name == null ? 'Memuat nama profil ...' : 'Profil $name'),
        backgroundColor: Style.primaryColor,
        brightness: Brightness.dark,
        centerTitle: true,
        automaticallyImplyLeading: true,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: totalJobsData == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              color: Style.bgColor,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 32, 16, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: CircleAvatar(
                            maxRadius: 60,
                            backgroundImage: fotoProfil == null
                                ? NetworkImage(
                                    'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                                : NetworkImage(mediaUrlUsers + fotoProfil)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                        child: Text('${totalJobsData.name}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: Text(
                            'Bergabung sejak ${totalJobsData.createdAt.toString().substring(0, 4)}',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                            totalJobsData?.jobPosted == null
                                ? '0 pekerjaan terposting'
                                : '${totalJobsData.jobPosted} pekerjaan terposting',
                            style: Style.appStyleDefaultVeryLight),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Text(
                            totalJobsData?.jobApplied == null
                                ? '0 pekerjaan terselesaikan'
                                : '${totalJobsData.jobApplied} pekerjaan terselesaikan',
                            style: Style.appStyleDefaultVeryLight),
                      ),
                      Card(
                        elevation: 10,
                        color: Style.bgColor,
                        child: ListTile(
                          // contentPadding: EdgeInsets.all(2),
                          // dense: true,
                          visualDensity:
                              VisualDensity(horizontal: -4, vertical: -2),
                          title: Text("Review & Rating"),
                          subtitle: Text("Ulasan dan rating riwayat pekerjaan"),
                          leading: Icon(FontAwesomeIcons.solidCalendarAlt),
                          trailing: Icon(
                            Icons.chevron_right,
                            color: Colors.black,
                          ),
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => RatingReviewProfile(
                                        idUser: aktivitasData.role == 'pekerja'
                                            ? aktivitasData.idJobprovider
                                            : aktivitasData.idJobseeker,
                                      ))),
                        ),
                      ),
                      Divider(),
                      Card(
                        elevation: 10,
                        color: Style.bgColor,
                        child: ListTile(
                          visualDensity:
                              VisualDensity(horizontal: -4, vertical: -2),
                          title: Text('Laporkan'),
                          subtitle: Text('Laporkan pengguna ini ke admin'),
                          leading: Icon(FontAwesomeIcons.personBooth),
                          trailing:
                              Icon(Icons.chevron_right, color: Colors.black),
                          onTap: () => Navigator.push(context,
                              MaterialPageRoute(builder: (_) => Report(aktivitasData: aktivitasData))),
                        ),
                      ),
                      Divider(),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
