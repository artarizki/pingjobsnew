import 'dart:convert';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();

  static bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  final namaLengkapController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final spesialisasiController = TextEditingController();
  final nomorHPController = TextEditingController();
  final bioController = TextEditingController();

  String namaLengkap, email, password, spesialisasi, nomorHp, bio;

  FocusNode nodeNamaLengkap;
  FocusNode nodeEmail;
  FocusNode nodePassword;
  FocusNode nodeSpesialisasi;
  FocusNode nodeNomorHP;
  FocusNode nodeBio;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    nodeNamaLengkap = FocusNode();
    nodeEmail = FocusNode();
    nodePassword = FocusNode();
    nodeSpesialisasi = FocusNode();
    nodeNomorHP = FocusNode();
    nodeBio = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    nodeNamaLengkap.dispose();
    nodeEmail.dispose();
    nodePassword.dispose();
    nodeSpesialisasi.dispose();
    nodeNomorHP.dispose();
    nodeBio.dispose();
  }

  Future<String> deleteData(var data) async {
    // print("DATA");
    // print(data);
    // listPostJob = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/delete_profile.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/delete_profile.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // print("Message: ${message}");
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
    return message.toString();
  }

  @override
  Widget build(BuildContext context) {
    // final ProfilVM profilVM = Provider.of<ProfilVM>(context);
    // profilVM.fetchData();
    var provider = Provider.of<UserProvider>(context);
    // print("Data Edit Profil : ${profilVM.listProfil}");
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
          title: Text('Edit Profil'),
          backgroundColor: Style.primaryColor,
          brightness: Brightness.dark,
          centerTitle: true,
          automaticallyImplyLeading: true,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: Container(
        color: Style.bgColor,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: provider.busy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.all(8),
                child: ListView(
                  children: <Widget>[
                    Form(
                      key: _key,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: new Stack(
                                fit: StackFit.loose,
                                children: <Widget>[
                                  new Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width: 150.0,
                                        height: 150.0,
                                        child: CircleAvatar(
                                          maxRadius: 60,
                                          backgroundImage: NetworkImage(provider
                                                      .user.imageUrl ==
                                                  null
                                              ? 'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
                                              : provider.user.imageUrl),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 110.0, left: 100.0, bottom: 30),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            provider.pickImage();
                                          },
                                          child: CircleAvatar(
                                            backgroundColor: Colors.blueAccent,
                                            radius: 25.0,
                                            child: new Icon(
                                              Icons.camera_alt,
                                              color: Colors.white,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                          ),
                          new Container(
                            color: Style.bgColor,
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 0.0, top: 0.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              new Text('Nama Lengkap',
                                                  style: Style.appStyle16Bold),
                                            ],
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Flexible(
                                            child: new TextFormField(
                                              keyboardType: TextInputType.name,
                                              // initialValue: '${provider.user.name}',
                                              controller: namaLengkapController,
                                              validator: (value) {
                                                if (value?.isEmpty ?? true)
                                                  return "Harap masukkan nama Anda";
                                                else
                                                  return null;
                                              },
                                              onSaved: (value) => provider
                                                  .user.name = value.trim(),
                                              decoration: InputDecoration(
                                                hintText:
                                                    "${provider.user.name}",
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 16),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              new Text('Email',
                                                  style: Style.appStyle16Bold),
                                            ],
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Flexible(
                                            child: new TextFormField(
                                              keyboardType:
                                                  TextInputType.emailAddress,
                                              // initialValue: '${provider.user.email}',
                                              controller: emailController,
                                              validator: (value) {
                                                if (value?.isEmpty ?? true)
                                                  return "Masukkan Email Anda";
                                                else if (!isEmail(value))
                                                  return 'email invalid';
                                                return null;
                                              },
                                              onSaved: (value) =>
                                                  provider.user.email = value,
                                              decoration: new InputDecoration(
                                                  hintText:
                                                      '${provider.user.email}'),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 16),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              new Text('Spesialisasi',
                                                  style: Style.appStyle16Bold),
                                            ],
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Flexible(
                                            child: new TextFormField(
                                              keyboardType: TextInputType.name,
                                              // initialValue: '${provider.user.specialization}',
                                              controller:
                                                  spesialisasiController,
                                              validator: (value) {
                                                if (value?.isEmpty ?? true)
                                                  return "Harap masukkan spesialisasi Anda";
                                                else
                                                  return null;
                                              },
                                              onSaved: (value) =>
                                                  provider.user.specialization =
                                                      value.trim(),
                                              decoration: InputDecoration(
                                                hintText: provider
                                                        .user.specialization ??
                                                    "Belum ada spesialisasi Anda",
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 16),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              new Text('Nomor Handphone',
                                                  style: Style.appStyle16Bold),
                                            ],
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Flexible(
                                            child: new TextFormField(
                                              keyboardType:
                                                  TextInputType.phone,
                                              inputFormatters: [
                                                PhoneInputFormatter(
                                                  allowEndlessPhone: true,
                                                )
                                              ],
                                              // initialValue: '${provider.user.mobilenumber}',
                                              controller: nomorHPController,
                                              validator: (value) {
                                                if (value?.isEmpty ?? true)
                                                  return "Harap masukkan nomor handphone Anda";
                                                else
                                                  return null;
                                              },
                                              onSaved: (value) => provider.user
                                                  .mobilenumber = value.trim(),
                                              decoration: InputDecoration(
                                                hintText:
                                                    "${provider.user.mobilenumber}",
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 16),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              new Text('Bio Anda',
                                                  style: Style.appStyle16Bold),
                                            ],
                                          ),
                                        ],
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 16, right: 16, top: 0),
                                      child: new Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Flexible(
                                            child: new TextFormField(
                                              maxLines: 5,
                                              keyboardType:
                                                  TextInputType.multiline,
                                              // initialValue: '${provider.user.bio}',
                                              controller: bioController,
                                              validator: (value) {
                                                if (value?.isEmpty ?? true)
                                                  return "Harap masukkan bio Anda";
                                                else
                                                  return null;
                                              },
                                              onSaved: (value) => provider
                                                  .user.bio = value.trim(),
                                              decoration: InputDecoration(
                                                hintText: provider.user.bio ??
                                                    "Belum ada bio Anda",
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Padding(
                                    padding:
                                        EdgeInsets.fromLTRB(16, 24, 16, 16),
                                    child: SizedBox(
                                      child: Container(
                                        height: 35,
                                        width: double.infinity,
                                        child: ElevatedButton.icon(
                                          onPressed: () async {
                                            if (_key.currentState.validate()) {
                                              _key.currentState.save();
                                              // print(provider.user.toJson());
                                              // return;
                                              var result =
                                                  await provider.updateUser();
                                              if (!result) {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(SnackBar(
                                                  content: Text(
                                                      Provider.of<UserProvider>(
                                                              context)
                                                          .message),
                                                ));
                                                print('error');
                                              }
                                            } else
                                              print('is not validate');
                                          },
                                          icon: Icon(FontAwesomeIcons.check),
                                          label: Text('Selesai',
                                              style: Style.appStyle16),
                                          style: ElevatedButton.styleFrom(
                                            primary: Colors.lightGreen,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                                    child: SizedBox(
                                      child: Container(
                                        height: 35,
                                        width: double.infinity,
                                        child: ElevatedButton.icon(
                                          onPressed: () async {
                                            var deleteProfile = {
                                              "id": provider.user.id
                                            };
                                            return showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: new Text(
                                                      'Anda yakin menghapus akun ini ?'),
                                                  actions: <Widget>[
                                                    ElevatedButton(
                                                      child: new Text("Ya"),
                                                      onPressed: () async {
                                                        await deleteData(
                                                            deleteProfile);
                                                      },
                                                    ),
                                                    ElevatedButton(
                                                      child: new Text(
                                                        "Batal",
                                                        style: Style
                                                            .appStyleDefaultBoldRed,
                                                      ),
                                                      onPressed: () =>
                                                          Navigator.pop(context),
                                                    ),
                                                  ],
                                                );
                                              },
                                            );
                                          },
                                          icon: Icon(Icons.cancel_outlined),
                                          label: Text('Hapus Akun',
                                              style: Style.appStyle16),
                                          style: ElevatedButton.styleFrom(
                                            primary: Colors.redAccent,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
