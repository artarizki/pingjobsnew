import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/pages/account/history_job_screen.dart';
import 'package:pingjobs/pages/activity/activity_screen.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/profile/edit_profile_screen.dart';
import 'package:pingjobs/pages/splash_screen.dart';
import 'package:pingjobs/provider/auth_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  // final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  String token;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
  }

  @override
  Widget build(BuildContext context) {
    // final ProfilVM profilVM = Provider.of<ProfilVM>(context);
    // profilVM.fetchData();
    // print("Data Profil :
    // ${profilVM.listProfil}");
    var provider = Provider.of<UserProvider>(context);
    return Scaffold(
        key: _scafoldKey,
        appBar: AppBar(
          title: Text('Profil Anda'),
          backgroundColor: Style.primaryColor,
          brightness: Brightness.dark,
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: /*profilVM.listProfil == null*/ token != null
            ? provider.busy
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container(
                    color: Style.bgColor,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(16, 32, 16, 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: CircleAvatar(
                                maxRadius: 60,
                                backgroundImage: NetworkImage(provider
                                            .user.imageUrl ==
                                        null
                                    ? 'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
                                    : provider.user.imageUrl),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                              child: Text('${provider.user.name}',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                            Padding(
                              padding: EdgeInsets.all(0),
                              child: Text(
                                  'Bergabung sejak ${provider.user.createdAt.toString().substring(0, 4)}',
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                  )),
                            ),
                            Text(
                                provider.user.jobPosted == null
                                    ? '0 pekerjaan terposting'
                                    : '${provider.user.jobPosted} pekerjaan terposting',
                                style: TextStyle(
                                    fontWeight: FontWeight.w100,
                                    color: Colors.black45)),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
                              child: Text(
                                  provider.user.jobApplied == null
                                      ? '0 pekerjaan terselesaikan'
                                      : '${provider.user.jobApplied} pekerjaan terselesaikan',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w100,
                                      color: Colors.black45)),
                            ),
                            Card(
                              elevation: 10,
                              color: Colors.white,
                              child: ListTile(
                                // contentPadding: EdgeInsets.all(2),
                                // dense: true,
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -2),
                                title: Text(
                                  "Aktivitas",
                                  style: Style.appStyleDefaultBold,
                                ),
                                subtitle:
                                    Text("Layanan yang sedang anda pakai"),
                                leading:
                                    Icon(FontAwesomeIcons.solidCalendarAlt),
                                trailing: Icon(
                                  Icons.chevron_right,
                                  color: Colors.black,
                                ),
                                onTap: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) =>
                                            AktivitasPage(backStatus: true))),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              color: Colors.white,
                              child: ListTile(
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -2),
                                title: Text('Edit Profile',
                                    style: Style.appStyleDefaultBold),
                                subtitle: Text(
                                    'Ubah nama, username, email, dan password Anda'),
                                leading: Icon(FontAwesomeIcons.personBooth),
                                trailing: Icon(Icons.chevron_right,
                                    color: Colors.black),
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => EditProfile())),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              color: Colors.white,
                              child: ListTile(
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -2),
                                title: Text("Riwayat Pekerjaan Sampingan",
                                    style: Style.appStyleDefaultBold),
                                subtitle: Text(
                                    "Riwayat pekerjaan sampingan yang pernah anda gunakan"),
                                leading: Icon(FontAwesomeIcons.history),
                                trailing: Icon(
                                  Icons.chevron_right,
                                  color: Colors.black,
                                ),
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => History(
                                            idUser:
                                                provider.user.id.toString()))),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              color: Colors.white,
                              child: ListTile(
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -2),
                                title: Text("FAQ",
                                    style: Style.appStyleDefaultBold),
                                subtitle: Text("Pertanyaan dan Jawaban Umum"),
                                leading: Icon(FontAwesomeIcons.search),
                                trailing: Icon(
                                  Icons.chevron_right,
                                  color: Colors.black,
                                ),
                                // onTap:()=> Navigator.of(context).push(
                                //     MaterialPageRoute(
                                //         builder: (_) => HistoryScreen(animationController: animationController,))
                                // ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              color: Colors.redAccent,
                              child: ListTile(
                                  visualDensity: VisualDensity(
                                      horizontal: -4, vertical: -2),
                                  title: Text("LOGOUT",
                                      style: Style.appStyle14white900),
                                  leading: Icon(Icons.exit_to_app_outlined,
                                      size: 30, color: Colors.white),
                                  onTap: () async {
                                    await Provider.of<AuthProvider>(context,
                                            listen: false)
                                        .logout();
                                    Navigator.pushAndRemoveUntil(context,
                                        MaterialPageRoute(builder: (context) {
                                      return SplashScreen();
                                    }), (Route<dynamic> route) => false);
                                  }),
                            ),
                            Divider()
                          ],
                        ),
                      ),
                    ),
                  )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 250,
                      child: Image.asset('assets/images/profilloginerror.png'),
                    ),
                    Container(
                      width: 200,
                      child: ElevatedButton.icon(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Login(),
                          ),
                        ),
                        icon: Icon(
                          Icons.login_outlined,
                          color: Colors.white,
                        ),
                        label: Text('Login'),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(10),
                          primary: Style.primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ));
  }
}
