import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/provider/ratingreview_profile_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:pingjobs/model/ratingreview_profile_model.dart';

class RatingReviewProfile extends StatefulWidget {
  const RatingReviewProfile({Key key, this.idUser}) : super(key: key);

  final String idUser;

  @override
  _RatingReviewProfileState createState() => _RatingReviewProfileState(idUser);
}

class _RatingReviewProfileState extends State<RatingReviewProfile> {
  // final _key = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  String idUser;
  String name;

  _RatingReviewProfileState(this.idUser);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
  }

  fetchData(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_namephoto.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var responsename = json.decode(response.body);
    name = responsename['name'].toString();
    setState(() {
      name = responsename['name'].toString();
    });
    return name;
  }

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    String mediaUrlUsers =
        'https://bertigagroup.com/pj.com/public/storage/images/users/';
    var data = {"id_user": idUser};
    final RatingReviewProfileVM ratingReviewProfileVM =
        Provider.of<RatingReviewProfileVM>(context);
    ratingReviewProfileVM.fetchData(data);
    print("Data RR Profil : ${ratingReviewProfileVM.listRatingReviewProfile}");
    var provider = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
          title: Text('Riwayat Pekerjaan'),
          backgroundColor: Style.primaryColor,
          brightness: Brightness.dark,
          centerTitle: true,
          automaticallyImplyLeading: true,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: /*profilVM.listProfil == null*/ ratingReviewProfileVM
                  ?.listRatingReviewProfile ==
              null
          ? Center(
              child: Container(
                  width: 250,
                  height: 250,
                  child: Image.asset('assets/images/dataerror.png')),
            )
          : provider.busy
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  color: Style.bgColor,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 8, bottom: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              CircleAvatar(
                                maxRadius: 8,
                                backgroundColor: Colors.blueAccent,
                              ),
                              Text('Pencari Kerja'),
                              SizedBox(width: 24),
                              CircleAvatar(
                                maxRadius: 8,
                                backgroundColor: Colors.green,
                              ),
                              Text('Penyedia Kerja'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(top: 0, bottom: 0.00001),
                            child: ratingReviewProfileVM
                                        .listRatingReviewProfile ==
                                    null
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : Container(
                                    color: Style.bgColor,
                                    child: ListView.builder(
                                      itemCount: ratingReviewProfileVM
                                              ?.listRatingReviewProfile
                                              ?.length ??
                                          0,
                                      padding: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      scrollDirection: Axis.vertical,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final ModelRatingReviewProfile data =
                                            ratingReviewProfileVM
                                                .listRatingReviewProfile[index];
                                        return Card(
                                          elevation: 10,
                                          color: Colors.white,
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 8, horizontal: 0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                ExpansionTile(
                                                  leading: CircleAvatar(
                                                      maxRadius: 24,
                                                      backgroundImage: ratingReviewProfileVM
                                                                  ?.listRatingReviewProfile[
                                                                      index]
                                                                  ?.fotoProfil ==
                                                              null
                                                          ? NetworkImage(
                                                              'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                                                          : NetworkImage(mediaUrlUsers +
                                                              ratingReviewProfileVM
                                                                  .listRatingReviewProfile[
                                                                      index]
                                                                  .fotoProfil)),
                                                  title: Text(
                                                    '${data.jobName}\n(${data.name})',
                                                    style: TextStyle(
                                                        color: data.role ==
                                                                'pekerja'
                                                            ? Colors.blueAccent
                                                            : Colors.green,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  subtitle: Text(
                                                    data.endDate != null
                                                        ? 'Selesai : ${data.endDate.toString().substring(5, 7)}-${data.endDate.toString().substring(8, 10)}-${data.endDate.toString().substring(0, 4)}'
                                                        : '',
                                                  ),
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 8,
                                                                  horizontal:
                                                                      16),
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Padding(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(8),
                                                                child: Text(
                                                                  data.idUser2 ==
                                                                          userProvider
                                                                              .user
                                                                              .id
                                                                              .toString()
                                                                      ? 'Rating dari Anda'
                                                                      : 'Rating dari pekerja',
                                                                  style: Style
                                                                      .appStyle16Bold,
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(8),
                                                                child:
                                                                    SmoothStarRating(
                                                                  isReadOnly:
                                                                      true,
                                                                  allowHalfRating:
                                                                      true,
                                                                  filledIconData:
                                                                      Icons
                                                                          .star,
                                                                  halfFilledIconData:
                                                                      Icons
                                                                          .star_half,
                                                                  defaultIconData:
                                                                      Icons
                                                                          .star_border,
                                                                  starCount: 5,
                                                                  rating: double
                                                                      .parse(data
                                                                          .bintang),
                                                                  size: 20,
                                                                  color: Colors
                                                                      .yellow,
                                                                  borderColor:
                                                                      Colors
                                                                          .yellow,
                                                                  spacing: 1,
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(8),
                                                                child: Text(
                                                                  data.keterangan,
                                                                  style: Style
                                                                      .appStyle16,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                  trailing: Icon(
                                                      Icons
                                                          .arrow_drop_down_rounded,
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
    );
  }
}
