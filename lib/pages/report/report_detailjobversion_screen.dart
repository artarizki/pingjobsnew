import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/report/report_success_screen.dart';
import 'package:pingjobs/provider/report_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class ReportDJV extends StatefulWidget {
  const ReportDJV({Key key, this.sidejobData}) : super(key: key);

  final ModelSideJob sidejobData;

  @override
  _ReportDJVState createState() => _ReportDJVState(sidejobData);
}

class _ReportDJVState extends State<ReportDJV> with TickerProviderStateMixin {
  var _contentController = TextEditingController();
  ModelSideJob sidejobData;

  _ReportDJVState(this.sidejobData);

  final _formKey = GlobalKey<FormState>();

  FocusNode nodeTargetUser;
  FocusNode nodeEmailTargetUser;
  FocusNode nodeContent;
  var _jenisLaporan = [
    "Akun",
    "Pekerjaan",
  ];

  var _contohLaporan = [
    "Penipuan atau penggelapan",
    "Informasi palsu",
    "Perundungan (bullying) atau pelecehan",
    "Kekerasan atau organisasi berbahaya",
    "Pelanggaran hak kekayaan intelektual",
    "Menyesatkan atau kemungkinan menipu",
    "Lainnya",
  ];

  String _currentSelectedValue;
  String _reportValue;

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    nodeTargetUser = FocusNode();
    nodeEmailTargetUser = FocusNode();
    nodeContent = FocusNode();
    // inisilalisasi datetime sekarang ke string
  }

  @override
  void dispose() {
    nodeTargetUser.dispose();
    nodeEmailTargetUser.dispose();
    nodeContent.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Style.bgColor,
        child: Scaffold(
            appBar: getAppBar(context),
            backgroundColor: Colors.transparent,
            body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                        child: Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16.0, bottom: 32.0),
                            child: Text(
                              'Laporkan masalah-masalah kepada kami disini',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          DropdownSearch<String>(
                            dropdownSearchDecoration: InputDecoration(
                              hintText:
                              'Laporkan Akun / Pekerjaan ?',
                              errorStyle: TextStyle(color: Colors.redAccent),
                              labelText:
                              'Pilih Jenis Laporan',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            searchBoxDecoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            mode: Mode.BOTTOM_SHEET,
                            showSelectedItem: true,
                            showClearButton: true,
                            showSearchBox: true,
                            items: _jenisLaporan,
                            label: "Pilih Jenis Laporan",
                            hint: "Laporkan Akun / Pekerjaan ?",
                            // popupItemDisabled: (String s) => s.startsWith('A'),
                            onChanged: (String value) {
                              _currentSelectedValue = value;
                              setState(() {
                                _currentSelectedValue = value;
                              });
                            },
                            onSaved: (String value) {
                              _currentSelectedValue = value;
                              setState(() {
                                _currentSelectedValue = value;
                              });
                            },
                            validator: (String item) {
                              if (item == null || item == "" || item == " ") {
                                return 'Harap pilih jenis laporan Anda';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 16),
                          DropdownSearch<String>(
                            dropdownSearchDecoration: InputDecoration(
                              hintText:
                              'Mengapa Anda Melaporkan ${_currentSelectedValue} ini?',
                              errorStyle: TextStyle(color: Colors.redAccent),
                              labelText:
                              'Pilih salah satu',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            searchBoxDecoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            mode: Mode.BOTTOM_SHEET,
                            showSelectedItem: true,
                            showClearButton: true,
                            showSearchBox: true,
                            items: _contohLaporan,
                            label: "Pilih salah satu",
                            hint: "Mengapa Anda Melaporkan ${_currentSelectedValue} ini?",
                            // popupItemDisabled: (String s) => s.startsWith('A'),
                            onChanged: (String value) {
                              _reportValue = value;
                              setState(() {
                                _reportValue = value;
                              });
                            },
                            onSaved: (String value) {
                              _reportValue = value;
                              setState(() {
                                _reportValue = value;
                              });
                            },
                            validator: (String item) {
                              if (item == null || item == "" || item == " ") {
                                return 'Harap pilih salah satu laporan Anda';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 8),
                          _reportValue == 'Lainnya' ? TextFormField(
                            controller: _contentController,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 8.0),
                                hintText: 'Apa saja keluhan Anda ?',
                                hintStyle: TextStyle(
                                  color: Colors.black26,
                                )),
                            maxLength: 70,
                            maxLines: 4,
                            textCapitalization: TextCapitalization.none,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.multiline,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Tulis keluhan Anda disini';
                              }
                              return null;
                            },
                            focusNode: nodeContent,
                          ) : SizedBox(height: 8,),
                          SizedBox(height: 8),
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Container(
                              width: double.infinity,
                              child: ElevatedButton.icon(
                                onPressed: () {
                                  final ReportVM reportVM =
                                      Provider.of<ReportVM>(context,
                                          listen: false);
                                  final UserProvider userProvider =
                                      Provider.of<UserProvider>(context,
                                          listen: false);
                                  var reportData = {
                                    "id_reporter": userProvider.user.id,
                                    "id_targetuser": sidejobData.idJobprovider,
                                    "id_job": _jenisLaporan == 'Pekerjaan' ? sidejobData.idJob : null,
                                    "report_type": _currentSelectedValue,
                                    "keterangan": _reportValue == 'Lainnya' ? _contentController.text : _reportValue
                                  };
                                  print("ReportDJVDATA : $reportData");
                                  reportVM.addData(reportData);
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(builder: (_) {
                                      return ReportSuccess();
                                    }),
                                  );
                                },
                                icon: Icon(FontAwesomeIcons.check),
                                label: Text('Selesai',
                                    style: Style.appStyle16White),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.lightGreen,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.circular(8)),
                                  padding: EdgeInsets.all(10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))))),
      ),
    );
  }

  Widget getAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Stack(
        children: <Widget>[
          AppBar(
            brightness: Brightness.dark,
            centerTitle: true,
            automaticallyImplyLeading: true,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            elevation: 0.0,
            backgroundColor: Style.primaryColor,
            title: Text(
              'Form Laporkan',
            ),
          ),
        ],
      ),
    );
  }
}
