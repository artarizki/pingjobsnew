import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/navigation_screen.dart';

class ReportSuccess extends StatefulWidget {
  const ReportSuccess({Key key}) : super(key: key);

  Size get preferredSize => Size.fromHeight(50.0);

  @override
  _ReportSuccessState createState() => _ReportSuccessState();
}

class _ReportSuccessState extends State<ReportSuccess>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 120.0,
          ),
          Center(
            child: Image.asset(
              'assets/images/success/checkmark.png',
              width: 100,
              height: 100,
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Text("Report Berhasil !", style: Style.appStyle22800),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              child: Text(
                "Terima Kasih telah report di PingJobs. Permintaan Anda akan kami proses, mohon tunggu notifikasi dari kami.\n Semoga Selamat dan sukses sampai tujuan)",
                style: Style.appStyleDefault,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(50, 5, 50, 5),
            child: SizedBox(
              width: double.infinity,
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (_) {
                      return NavigationPage();
                    }),
                  );
                },
                textColor: Colors.white,
                color: Style.primaryColor,
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  'Kembali ke Beranda',
                  style: Style.appStylewhiteDefault,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
