import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/profile/another_profile_screen.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';

class DetailActivityJpAnotherUser extends StatelessWidget {
  final ModelAktivitas aktivitasData;
  final TotalJobsVM totalJobsVM;
  final String fotoProfil;

  const DetailActivityJpAnotherUser(
      {Key key, this.aktivitasData, this.totalJobsVM, this.fotoProfil})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String mediaUrlUsers =
        'https://bertigagroup.com/pj.com/public/storage/images/users/';

    return InkWell(
      child: Card(
        color: Style.bgColor,
        elevation: 10,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: 16, bottom: 8),
                  child: Text('Pekerja Sampingan')),
              totalJobsVM.listTotalJobs == null
                  ? Center(child: CircularProgressIndicator())
                  : aktivitasData.idJobseeker == null
                      ? Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: Text('Belum ada pelamar pekerjaan ini',
                                style: Style.appStyle16Bold),
                          ),
                        )
                      : ListTile(
                          leading: CircleAvatar(
                              maxRadius: 24,
                              backgroundImage: fotoProfil == null
                                  ? NetworkImage(
                                      'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                                  : NetworkImage(mediaUrlUsers + fotoProfil)),
                          title: Text('${totalJobsVM.listTotalJobs[0].name}'),
                          trailing: (totalJobsVM
                                      ?.listTotalJobs[0]?.jobApplied ==
                                  null)
                              ? Text('0 total pekerjaan terselesaikan')
                              : Text(
                                  '${totalJobsVM.listTotalJobs[0].jobApplied} total pekerjaan terselesaikan'),
                        )
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AnotherProfile(
                      aktivitasData: aktivitasData,
                      totalJobsData: totalJobsVM.listTotalJobs[0],
                    )));
      },
    );
  }
}
