import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/provider/activity_provider.dart';
import 'package:provider/provider.dart';

class DetailActivityJpNote extends StatefulWidget {
  final ModelAktivitas aktivitasData;

  const DetailActivityJpNote({Key key, this.aktivitasData}) : super(key: key);

  @override
  _DetailActivityJpNoteState createState() => _DetailActivityJpNoteState();
}

class _DetailActivityJpNoteState extends State<DetailActivityJpNote> {
  TextEditingController _textController = new TextEditingController();
  FocusNode nodeText = FocusNode();
  bool tap = false;

  @override
  Widget build(BuildContext context) {
    final AktivitasVM aktivitasVM = Provider.of<AktivitasVM>(context);
    String status = widget.aktivitasData.status;
    return Card(
      color: Style.bgColor,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Catatan'),
            SizedBox(
              height: 8,
            ),
            status == 'selesai' || status == 'batal'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(widget.aktivitasData.note == null
                          ? 'Anda belum diberi catatan oleh pekerja'
                          : widget.aktivitasData.note)
                    ],
                  )
                : TextField(
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    focusNode: nodeText,
                    onChanged: (text) {
                      print("First text field: $text");
                    },
                    onTap: () {
                      tap = true;
                      setState(() {
                        tap = true;
                      });
                    },
                    controller: _textController,
                    maxLines: 6,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: widget.aktivitasData.note == null
                            ? 'Masukkan Catatan Untuk Pekerja'
                            : '${widget.aktivitasData.note}'),
                  ),
            tap == true
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.check),
                          color: Style.primaryColor,
                          onPressed: () {
                            tap = false;
                            setState(() {
                              tap = false;
                            });
                            var data = {
                              "id": widget.aktivitasData.idActivityHistory,
                              "note": _textController.text
                            };
                            var message = aktivitasVM.setNote(data);
                            String response = message.toString();
                            if (response == 'Insert Note Success') {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text("Isi Catatan Berhasil"),
                              ));
                              _textController.dispose();
                            } else {
                              return ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(response),
                              ));
                            }
                          }),
                      IconButton(
                          icon: Icon(Icons.clear),
                          color: Colors.red,
                          onPressed: () {
                            _textController.clear();
                            tap = false;
                            setState(() {
                              tap = false;
                              // _textController.dispose();
                            });
                          }),
                    ],
                  )
                : SizedBox(height: 0),
            SizedBox(
              height: 8,
            )
          ],
        ),
      ),
    );
  }
}
