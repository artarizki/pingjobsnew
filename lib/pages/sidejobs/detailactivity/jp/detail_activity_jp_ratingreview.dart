import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/sidejobs/ratingreview/ratingreview_screen.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DetailActivityJpRatingReview extends StatelessWidget {
  final ModelAktivitas aktivitasData;
  final RatingReviewVM rrVM;

  const DetailActivityJpRatingReview({Key key, this.aktivitasData, this.rrVM})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Style.bgColor,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 16, bottom: 8),
                child: Text('Rating Anda untuk pekerja sampingan')),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: rrVM.listRatingReview.length == 0
                    ? Text('Anda belum memberikan rating')
                    : rrVM?.listRatingReview[0]?.bintang == null
                        ? Center(child: CircularProgressIndicator())
                        : SmoothStarRating(
                            isReadOnly: true,
                            allowHalfRating: true,
                            filledIconData: Icons.star,
                            halfFilledIconData: Icons.star_half,
                            defaultIconData: Icons.star_border,
                            starCount: 5,
                            rating: double.parse(
                                rrVM?.listRatingReview[0]?.bintang ?? 0),
                            size: 40,
                            color: Colors.yellow,
                            borderColor: Colors.yellow,
                            spacing: 1,
                          ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8, left: 16, bottom: 8),
              child: Text('Ulasan Anda Untuk Pekerjaan Ini'),
            ),
            rrVM.listRatingReview.length == 0
                ? Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Text('Tidak ada ulasan dari anda'),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                    child: Text(
                        rrVM?.listRatingReview[0]?.keterangan ?? 'Memuat...'),
                  ),
            rrVM.listRatingReview.length == 0 &&
                    aktivitasData.status == 'selesai'
                ? Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton.icon(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RatingReview(
                                aktivitasData: aktivitasData, backStatus: true),
                          ),
                        ),
                        icon: Icon(
                          Icons.star,
                          color: Colors.white,
                        ),
                        label: Text('Beri Rating & Review'),
                        style: ElevatedButton.styleFrom(
                          primary: Color(0xFFFFD700),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
