import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DetailActivityJpRatingReview2 extends StatelessWidget {
  final ModelAktivitas aktivitasData;
  final RatingReviewVM rrVM;

  const DetailActivityJpRatingReview2({Key key, this.aktivitasData, this.rrVM})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Style.bgColor,
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: Padding(
        padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 16, bottom: 8),
                child: Text('Rating Dari Pekerja')),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: rrVM.listRatingReview.length == 1 ||
                        rrVM.listRatingReview.length == 0
                    ? Text('Anda belum diberikan rating')
                    : rrVM?.listRatingReview[1]?.bintang == null
                        ? Center(child: CircularProgressIndicator())
                        : SmoothStarRating(
                            isReadOnly: true,
                            allowHalfRating: true,
                            filledIconData: Icons.star,
                            halfFilledIconData: Icons.star_half,
                            defaultIconData: Icons.star_border,
                            starCount: 5,
                            rating:
                                double.parse(rrVM.listRatingReview[1].bintang),
                            size: 40,
                            color: Colors.yellow,
                            borderColor: Colors.yellow,
                            spacing: 1,
                          ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(top: 8, left: 16, bottom: 8),
                child: Text('Ulasan dari pekerja')),
            rrVM.listRatingReview.length == 1 ||
                    rrVM.listRatingReview.length == 0
                ? Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Text('Tidak ada ulasan dari pekerja'),
                    ))
                : Padding(
                    padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                    child:
                        Text(rrVM?.listRatingReview[1]?.keterangan ?? 'Memuat'),
                  )
          ],
        ),
      ),
    );
  }
}
