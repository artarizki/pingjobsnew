import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:optimized_cached_image/optimized_cached_image.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/pages/sidejobs/jobseeker/jobseeker_location_screen.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';

class DetailActivityJsAboutJob extends StatelessWidget {
  final ModelAktivitas aktivitasData;
  final TotalJobsVM totalJobsVM;
  final String fotoProfil;
  final Color color;

  const DetailActivityJsAboutJob(
      {Key key,
      this.aktivitasData,
      this.totalJobsVM,
      this.fotoProfil,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String status = aktivitasData.status;
    String media = 'assets/images/untitled.jpg';
    String mediaUrlSideJobs =
        'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';
    final UserProvider userProvider = Provider.of<UserProvider>(context);

    return Card(
      color: Style.bgColor,
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
            child: AspectRatio(
              aspectRatio: 2.5,
              child: OptimizedCacheImage(
                imageUrl: mediaUrlSideJobs + aktivitasData.jobPhotovideo,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    LinearProgressIndicator(value: downloadProgress.progress),
                errorWidget: (context, url, error) => Image.asset(media),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            title: Text(
              aktivitasData.jobName,
              style: Style.appStyle20900,
            ),
            trailing: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Style.primaryColor),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LokasiPekerja(
                        aktivitasData: aktivitasData,
                        totalJobs: totalJobsVM.listTotalJobs[0],
                        fotoProfil: fotoProfil,
                        id: userProvider.user.id.toString(),
                      ),
                    ));
              },
              child: Text(
                'Lihat Lokasi',
                style: Style.appStyleDefault,
              ),
            ),
          ),
          ListTile(
            title: Text(
              'Gaji',
              style: Style.appStyleDefault,
            ),
            trailing: Text(
              'Rp. ${toCurrencyString(aktivitasData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
              style: Style.appStyleDefault,
            ),
          ),
          ListTile(
            title: Text('Tanggal Mulai Pekerjaan'),
            trailing: Text(
                '${aktivitasData.jobDate.toString().substring(8, 10)}-${aktivitasData.jobDate.toString().substring(5, 7)}-${aktivitasData.jobDate.toString().substring(0, 4)}'),
          ),
          ListTile(
            title: Text('Jadwal/Periode'),
            trailing: Text('${aktivitasData.jadwal}'),
          ),
          ListTile(
              title: Text('Jam'),
              trailing: Text('${aktivitasData.hoursStart}-${aktivitasData.hoursEnd}')
          ),
          ListTile(
              title: Text(
                'Status',
                style: Style.appStyleDefault,
              ),
              trailing: Text(
                status[0].toUpperCase() + status.substring(1),
                style: TextStyle(
                  color: color,
                  fontWeight: FontWeight.bold,
                ),
              )),
          ListTile(
            title: Text(
              'Deskripsi Pekerjaan',
              style: Style.appStyleDefault,
            ),
            trailing: TextButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        title: Text(aktivitasData.jobName,
                            style: Style.appStyleDefault),
                        content: Text(aktivitasData.jobDescription,
                            style: Style.appStyleDefault),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Oke',
                              style: Style.appStyleDefault,
                            ),
                          )
                        ],
                      );
                    });
              },
              child: Text(
                'Lihat Selengkapnya>>',
                style: Style.appStyleDefault,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}
