import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';

class DetailActivityJsNote extends StatelessWidget {
  final ModelAktivitas aktivitasData;

  const DetailActivityJsNote({Key key, this.aktivitasData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Style.bgColor,
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Catatan', style: Style.appStyleDefault),
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment:
              CrossAxisAlignment.start,
              children: [
                Text(
                    aktivitasData.note == null
                        ? 'Anda belum diberi catatan oleh penyedia'
                        : aktivitasData.note,
                    style: Style.appStyleDefault)
              ],
            )
          ],
        ),
      ),
    );
  }
}
