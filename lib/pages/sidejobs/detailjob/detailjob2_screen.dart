import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_centermap.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_info_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_map.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_photo_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_tabbar.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pingjobs/component/style.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 0;

class DetailJob extends StatefulWidget {
  const DetailJob({Key key, this.sidejobData, this.index}) : super(key: key);

  final ModelSideJob sidejobData;
  final int index;

  @override
  _DetailJobState createState() => _DetailJobState(sidejobData, index);
}

class _DetailJobState extends State<DetailJob>
    with SingleTickerProviderStateMixin {
  final double infoHeight = 364.0;
  final ModelSideJob sidejobData;

  _DetailJobState(this.sidejobData, this.index);

  TabController _tabController;

  // url API
  final String url = 'https://api.openrouteservice.org/v2/directions/';

// deklarasi pin custom marker
  BitmapDescriptor sourceIcon;
  BitmapDescriptor trackingIcon;
  BitmapDescriptor destinationIcon;

// lokasi awal pengguna dan lokasi saat ini saat berpindah tempat
  LocationData currentLocation;
  LocationData startLocation;

// referensi ke lokasi tujuan
  LocationData destinationLocation;

// wrapper API Location
  Location location;
  Completer<GoogleMapController> _controller = Completer();

  // deklarasi marker, polyline untuk rute, dan koordinat
  Set<Marker> _markers = Set<Marker>();
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  Polyline polyline;

  // data rute dari request http
  var data;
  int index;
  String name;
  var fotoProfil;
  String mediaUrlUsers =
      'https://bertigagroup.com/pj.com/public/storage/images/users/';
  String totalRating;
  String totalReview;

  fetchData(var data) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_count_rr.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    var result = json.decode(response.body);
    totalRating = result['totalrating'];
    totalReview = result["totalreview"];
    setState(() {
      totalRating = result['totalrating'];
      totalReview = result["totalreview"];
    });
    print("TOTALRR : ${totalRating} ${totalReview}");
    return totalReview;
  }


  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
    fetchData(
        {
          "id_job": sidejobData.idJob,
          "id_user": sidejobData.idJobprovider
        }
    );
    setInitialLocation();
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Detail Pekerjaan'),
      backgroundColor: Style.primaryColor,
      brightness: Brightness.dark,
      centerTitle: true,
      automaticallyImplyLeading: true,
      leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () {
            List<int> list = <int>[];
            list.add(null);
            Navigator.pop(context, list);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    fetchData(
        {"id_job": sidejobData.idJob, "id_user": sidejobData.idJobprovider});
    var provider = Provider.of<ConversationProvider>(context);
    provider.getConversations();
    return Container(
      color: Style.bgColor,
      child: Scaffold(
        appBar: _buildAppBar(context),
        backgroundColor: Colors.transparent,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              child: DetailJobTabBar(tabController: _tabController),
            ),
            Container(
              child: Expanded(
                child: new TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        DetailJobPhoto(sidejobData: sidejobData),
                        DetailJobInfo(
                          index: index,
                          sidejobData: sidejobData,
                        )
                      ],
                    ),
                    Stack(
                      children: <Widget>[
                        DetailJobMap(sidejobData: sidejobData),
                        DetailJobInfo(
                          index: index,
                          sidejobData: sidejobData,
                        ),
                        Positioned(
                          top: (MediaQuery.of(context).size.width / 1.2) -
                              24.0 -
                              35,
                          right: 35,
                          child: InkWell(
                            child: DetailJobCenterMap(),
                            onTap: () {
                              centerOnMap();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();
    print(
      "CurrentLng : " +
          currentLocation.longitude.toString() +
          "CurrentLat : " +
          currentLocation.latitude.toString(),
    );
    startLocation = LocationData.fromMap({
      "longitude": currentLocation.longitude,
      "latitude": currentLocation.latitude,
    });

    print(
      "StartLng : " +
          startLocation.longitude.toString() +
          "StartLat : " +
          startLocation.latitude.toString(),
    );

    destinationLocation = LocationData.fromMap({
      "latitude": double.parse(sidejobData.xJPin),
      "longitude": double.parse(sidejobData.yJPin)
    });

    print(
      "DestLng : " +
          destinationLocation.longitude.toString() +
          "DestLat : " +
          destinationLocation.latitude.toString(),
    );
  }

  // memusatkan tampilan ke lokasi terkini
  void centerOnMap() async {
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(
      CameraUpdate.newCameraPosition(cPosition),
    );
  }
}
