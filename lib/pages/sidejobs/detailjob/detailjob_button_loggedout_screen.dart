import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/apply_job_screen.dart';
import 'package:pingjobs/pages/sidejobs/success/apply_success_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/postjob_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';

class DetailJobLoggedOut extends StatefulWidget {
  const DetailJobLoggedOut({Key key, this.index, this.sidejobData})
      : super(key: key);

  final ModelSideJob sidejobData;
  final int index;

  @override
  _DetailJobLoggedOutState createState() =>
      _DetailJobLoggedOutState(index, sidejobData);
}

class _DetailJobLoggedOutState extends State<DetailJobLoggedOut> {
  VoidCallback callback;
  ModelSideJob sidejobData;
  int index;

  _DetailJobLoggedOutState(this.index, this.sidejobData);

  @override
  void initState() {
    super.initState();
  }

  loginFirst() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Login untuk mengirim pesan ?'),
            actions: <Widget>[
              ElevatedButton(
                child: Text(
                  "Login",
                  style: Style.appStylewhiteDefaultBold,
                ),
                onPressed: () => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Style.primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              ElevatedButton(
                child: Text(
                  "Tidak",
                  style: Style.appStyleDefaultBoldRed,
                ),
                onPressed: () => Navigator.pop(context),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side:
                    BorderSide(color: Style.secondaryColor),
                  ),
                ),
              ),
            ],
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: const Duration(milliseconds: 500),
      opacity: 1.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, bottom: 0, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 48,
              height: 48,
              child: Container(
                decoration: BoxDecoration(
                  color: Style.bgColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ),
                child: InkWell(
                  onTap: loginFirst,
                  child: Container(
                    height: 48,
                    decoration: BoxDecoration(
                      color: Style.primaryColor,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16.0),
                      ),
                      boxShadow: <BoxShadow>[
                        Style.shadowElement,
                      ],
                    ),
                    child: Icon(
                      Icons.send_outlined,
                      color: Style.bgColor,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Container(
                height: 48,
                decoration: BoxDecoration(
                  color: Style.primaryColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  boxShadow: <BoxShadow>[
                    Style.shadowElement,
                  ],
                ),
                child: Center(
                  child: InkWell(
                    onTap: loginFirst,
                    child: Text(
                      'Apply Sekarang',
                      textAlign: TextAlign.left,
                      style: Style.appStyle18white600,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
