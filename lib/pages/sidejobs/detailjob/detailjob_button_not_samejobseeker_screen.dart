import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/apply_job_screen.dart';
import 'package:pingjobs/pages/sidejobs/success/apply_success_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/postjob_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';

class DetailJobNotSameJobSeeker extends StatefulWidget {
  const DetailJobNotSameJobSeeker({Key key, this.index, this.sidejobData})
      : super(key: key);

  final ModelSideJob sidejobData;
  final int index;

  @override
  _DetailJobNotSameJobSeekerState createState() =>
      _DetailJobNotSameJobSeekerState(index, sidejobData);
}

class _DetailJobNotSameJobSeekerState extends State<DetailJobNotSameJobSeeker> {
  VoidCallback callback;
  ModelSideJob sidejobData;
  int index;

  _DetailJobNotSameJobSeekerState(this.index, this.sidejobData);

  @override
  void initState() {
    super.initState();
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  Future<int> checkId() async {
    var provider;
    provider = await Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    provider.asMap().forEach((indexData, item) {
      if (sidejobData.idJobprovider == item.user.id.toString()) {
        index = indexData;
        setState(() {
          index = indexData;
        });
        print("INDEXSAME = $index");
      }
    });
    print("INDEKS SETELAH DICOCOKKAN : $index");
    return index;
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ConversationProvider>(context, listen: true);
    final PostJobVM postJobVM = Provider.of<PostJobVM>(context);

    getC(BuildContext context) {
      var provider = Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
      return provider;
    }

    Future sendMessage() async {
      print("KE CHAT");
      await checkId();
      var providerGet;
      print(
        "INDEX : " + index.toString(),
      );
      print("ID JOB : " + sidejobData.idJob + sidejobData.jobName);
      String message = "Halo, Saya ingin bertanya tentang pekerjaan ini";
      if (index == null) {
        print("MASUK INDEKS NULL");
        await provider.newMessage(message, sidejobData.idJobprovider);
        providerGet = await getC(context);
        providerGet.asMap().forEach((indexData, item) async {
          print(
            "INDEKS USER ID CONVERSATION2 : " + item.user.id.toString(),
          );
          if (sidejobData.idJobprovider == item.user.id.toString()) {
            index = indexData;
            setState(() {
              index = indexData;
            });
            print("INDEXSAME = $index");
          }
        });
        final result = await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ChatScreen(
              conversation: providerGet[index],
            );
          }),
        );
        index = result[0];
        setState(() {
          index = result[0];
        });
        print(
          "PANJANG IDJOBPROV: " + providerGet.length.toString(),
        );
        print(
          "INDEXSETELAH ISI : " + index.toString(),
        );
      } else {
        print("GAK MASUK INDEKS NULL");
        print(
          "INDEKS CONVERSATION KE :" + index.toString(),
        );
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => ChatScreen(
              conversation: provider.concersations[index],
            ),
          ),
        );
      }
      ;
    }

    return AnimatedOpacity(
      duration: const Duration(milliseconds: 500),
      opacity: 1.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, bottom: 0, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 48,
              height: 48,
              child: Container(
                decoration: BoxDecoration(
                  color: Style.bgColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.2),
                  ),
                ),
                child: InkWell(
                  onTap: sendMessage,
                  child: Container(
                    height: 48,
                    decoration: BoxDecoration(
                      color: Style.primaryColor,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16.0),
                      ),
                      boxShadow: <BoxShadow>[
                        Style.shadowElement,
                      ],
                    ),
                    child: Icon(
                      Icons.send_outlined,
                      color: Style.bgColor,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Container(
                height: 48,
                decoration: BoxDecoration(
                  color: Style.primaryColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  boxShadow: <BoxShadow>[
                    Style.shadowElement,
                  ],
                ),
                child: Center(
                  child: InkWell(
                    onTap: () {
                      if (sidejobData?.persyaratan == null) {
                        final UserProvider userProvider =
                            Provider.of<UserProvider>(context, listen: false);
                        var jobSeekerData = {
                          // "id_jobseeker": '1',
                          "id_jobseeker": userProvider.user.id,
                          "id_job_applied": sidejobData.idJob,
                          "job_date": sidejobData.jobDate.toString()
                        };
                        print("jobDATA");
                        print(jobSeekerData);
                        postJobVM.applyData(jobSeekerData);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => ApplySuccess(),
                          ),
                        );
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => ApplyJob(
                                persyaratan:
                                    sidejobData.persyaratan.toLowerCase()),
                          ),
                        );
                      }
                    },
                    child: Text(
                      'Apply Sekarang',
                      textAlign: TextAlign.left,
                      style: Style.appStyle18white600,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
