import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';

class DetailJobSameJobSeeker extends StatefulWidget {
  const DetailJobSameJobSeeker({Key key, this.index}) : super(key: key);

  final int index;

  @override
  _DetailJobSameJobSeekerState createState() => _DetailJobSameJobSeekerState(index);
}

class _DetailJobSameJobSeekerState extends State<DetailJobSameJobSeeker> {
  int index;

  _DetailJobSameJobSeekerState(this.index);

  @override
  void initState() {
    super.initState();
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ConversationProvider>(context);
    provider.getConversations();

    return AnimatedOpacity(
      duration: const Duration(milliseconds: 500),
      opacity: 1.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, bottom: 0, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 48,
              height: 48,
              child: Container(
                  decoration: BoxDecoration(
                    color: Style.bgColor,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.2),
                    ),
                  ),
                  child: InkWell(
                    onTap: () async {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => ChatScreen(
                          conversation: provider.concersations[index],
                        ),
                      ));
                    },
                    child: Container(
                      height: 48,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16.0),
                        ),
                        boxShadow: <BoxShadow>[
                          Style.shadowElement,
                        ],
                      ),
                      child: Icon(
                        Icons.send_outlined,
                        color: Style.bgColor,
                        size: 30,
                      ),
                    ),
                  )),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Container(
                height: 48,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  boxShadow: <BoxShadow>[
                    Style.shadowElement,
                  ],
                ),
                child: Center(
                  child: InkWell(
                    onTap: () {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(
                              'Anda Sudah Mengambil Pekerjaan Sampingan ini'),
                        ),
                      );
                    },
                    child: Text(
                      'Apply Sekarang',
                      textAlign: TextAlign.left,
                      style: Style.appStyle18white600,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
