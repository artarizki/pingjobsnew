import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';

class DetailJobCenterMap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Style.primaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50.0),
      ),
      elevation: 10.0,
      child: Container(
        width: 60,
        height: 60,
        child: Center(
          child: Icon(
            Icons.my_location,
            color: Style.bgColor,
            size: 30,
          ),
        ),
      ),
    );
  }
}
