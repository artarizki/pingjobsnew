import 'dart:convert';

import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/provider/totalrr_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class DetailJobFeeNamePhoto extends StatefulWidget {
  const DetailJobFeeNamePhoto({Key key, this.sidejobData}) : super(key: key);
  final ModelSideJob sidejobData;

  @override
  _DetailJobFeeNamePhotoState createState() =>
      _DetailJobFeeNamePhotoState(sidejobData);
}

class _DetailJobFeeNamePhotoState extends State<DetailJobFeeNamePhoto> {
  final ModelSideJob sidejobData;

  String media = 'assets/images/untitled.jpg';
  String mediaUrl =
      'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';
  String mediaUrlUsers =
      'https://bertigagroup.com/pj.com/public/storage/images/users/';

  _DetailJobFeeNamePhotoState(this.sidejobData);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Rp. ${toCurrencyString(sidejobData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
            textAlign: TextAlign.left,
            style: Style.appStyle22pColorLight,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Text(
                  sidejobData.name == null
                      ? 'Memuat...'
                      : sidejobData.name == userProvider.user.name
                      ? sidejobData.name + ' (Anda)'
                      : sidejobData.name,
                  textAlign: TextAlign.left,
                  style: Style.appStyle18,
                ),
                SizedBox(width: 8),
                CircleAvatar(
                  maxRadius: 18,
                  backgroundImage: sidejobData.fotoProfil == null
                      ? NetworkImage(
                      'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                      : NetworkImage(mediaUrlUsers + sidejobData.fotoProfil),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
