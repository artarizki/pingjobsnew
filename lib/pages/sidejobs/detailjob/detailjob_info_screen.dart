import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/ratingreview_profile_model.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/profile/another_profile_detailjobversion_screen.dart';
import 'package:pingjobs/pages/profile/ratingreview_profile_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_button_jobseeker_notnull_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_button_loggedout_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_button_not_samejobseeker_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_button_samejobprovider_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_button_samejobseeker_screen.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob_fee_name_photo_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/ratingreview_profile_provider.dart';
import 'package:pingjobs/provider/totaljobs_provider.dart';
import 'package:pingjobs/provider/totalrr_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

class DetailJobInfo extends StatefulWidget {
  const DetailJobInfo({Key key, this.index, this.sidejobData})
      : super(key: key);

  final int index;
  final ModelSideJob sidejobData;

  @override
  _DetailJobInfoState createState() => _DetailJobInfoState(index, sidejobData);
}

class _DetailJobInfoState extends State<DetailJobInfo> {
  int index;
  ModelSideJob sidejobData;
  List<String> perPersyaratan = <String>[];
  List<String> perFasilitas = <String>[];
  String token;
  String mediaUrlUsers =
      'https://bertigagroup.com/pj.com/public/storage/images/users/';

  _DetailJobInfoState(this.index, this.sidejobData);

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    super.initState();
    getPref();
    perPersyaratan = sidejobData?.persyaratan?.split(';');
    perFasilitas = sidejobData?.fasilitas?.split(';');
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  loginFirst() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Login untuk apply pekerjaan ?'),
            actions: <Widget>[
              ElevatedButton(
                child: Text(
                  "Login",
                  style: Style.appStylewhiteDefaultBold,
                ),
                onPressed: () => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Style.primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              ElevatedButton(
                child: Text(
                  "Tidak",
                  style: Style.appStyleDefaultBoldRed,
                ),
                onPressed: () => Navigator.pop(context),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: Style.secondaryColor),
                  ),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final TotalRRVM totalRRVM = Provider.of<TotalRRVM>(context);
    var data2 = {"id_user": sidejobData.idJobprovider};
    totalRRVM?.fetchData(data2);
    var data3 = {"id_user": sidejobData.idJobprovider};
    final RatingReviewProfileVM ratingReviewProfileVM =
        Provider.of<RatingReviewProfileVM>(context);
    ratingReviewProfileVM.fetchData(data3);
    print("Data RR Profil : ${ratingReviewProfileVM.listRatingReviewProfile}");
    final TotalJobsVM totalJobsVM = Provider.of<TotalJobsVM>(context);
    var data = {"id": sidejobData.idJobprovider};
    totalJobsVM.getTotalJobs(data);
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    var provider = Provider.of<ConversationProvider>(context);
    provider.getConversations();

    return Positioned(
      top: (MediaQuery.of(context).size.width / 1.2) - 30.0,
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        decoration: BoxDecoration(
          color: Style.bgColor,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
          boxShadow: <BoxShadow>[
            Style.shadowElement,
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 16, left: 18, right: 16),
                          child: sidejobData.idJobseeker == null
                              ? Text('[TERSEDIA]',
                                  style: Style.appStyleDefaultBoldGreen)
                              : Text(
                                  '[TERAMBIL]',
                                  style: Style.appStyleDefaultBoldRed,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 16.0, left: 18, right: 16),
                        child: Text(
                          '${sidejobData.jobName}',
                          textAlign: TextAlign.left,
                          style: Style.appStyleJobTitle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 2.0, left: 18, right: 16),
                        child: Text(
                          '${sidejobData.street}',
                          textAlign: TextAlign.left,
                          style: Style.appStyle18black45,
                        ),
                      ),
                      DetailJobFeeNamePhoto(sidejobData: sidejobData),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 8),
                        child: Text(
                          'Tanggal Mulai Pekerjaan Sampingan',
                          textAlign: TextAlign.justify,
                          style: Style.appStyle16Bold,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 0, bottom: 8),
                        child: Text(
                          '${sidejobData.jobDate.toString().substring(8, 10)}-${sidejobData.jobDate.toString().substring(5, 7)}-${sidejobData.jobDate.toString().substring(0, 4)}',
                          textAlign: TextAlign.justify,
                          style: Style.appStyle16,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 8, bottom: 8),
                                child: Text(
                                  'Jadwal/Periode',
                                  textAlign: TextAlign.justify,
                                  style: Style.appStyle16Bold,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 0, bottom: 8),
                                child: Text(
                                  '${sidejobData.jadwal}',
                                  textAlign: TextAlign.justify,
                                  style: Style.appStyle16,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 8, bottom: 8),
                                child: Text(
                                  'Jam',
                                  textAlign: TextAlign.justify,
                                  style: Style.appStyle16Bold,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 0, bottom: 8),
                                child: Text(
                                  '${sidejobData.hoursStart.substring(0, 5)}-${sidejobData.hoursEnd.substring(0, 5)}',
                                  textAlign: TextAlign.justify,
                                  style: Style.appStyle16,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16, right: 16, top: 8, bottom: 8),
                                  child: Text(
                                    'Persyaratan',
                                    textAlign: TextAlign.left,
                                    style: Style.appStyle16Bold,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                perPersyaratan?.length == null
                                    ? Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 0),
                                        child: Text('Tidak ada Persyaratan'),
                                      )
                                    : ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: perPersyaratan?.length ?? 0,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: EdgeInsets.only(
                                                left: 16, right: 16),
                                            child: Text(
                                              perPersyaratan[index],
                                              textAlign: TextAlign.justify,
                                              style: Style.appStyle16,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          );
                                        },
                                      ),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16, right: 16, top: 8, bottom: 8),
                                  child: Text(
                                    'Fasilitas',
                                    textAlign: TextAlign.justify,
                                    style: Style.appStyle16Bold,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                perFasilitas?.length == null
                                    ? Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 0),
                                        child: Text('Tidak ada Fasilitas'),
                                      )
                                    : ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: perFasilitas?.length ?? 0,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 16, right: 16),
                                                child: Text(
                                                  perFasilitas[index],
                                                  textAlign: TextAlign.justify,
                                                  style: Style.appStyle16,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              )
                                            ],
                                          );
                                        },
                                      )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 0, bottom: 0),
                        child: Text(
                          'Deskripsi Pekerjaan',
                          textAlign: TextAlign.justify,
                          style: Style.appStyle16Bold,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 0),
                        child: Text(
                          '${sidejobData.jobDescription}',
                          textAlign: TextAlign.justify,
                          style: Style.appStyleDefault,
                          maxLines: 11,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 16, bottom: 0),
                        child: Text(
                          'Tentang Penyedia Kerja Sampingan',
                          textAlign: TextAlign.justify,
                          style: Style.appStyle16Bold,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 0),
                        child: ListTile(
                          contentPadding: EdgeInsets.zero,
                          isThreeLine: true,
                          leading: CircleAvatar(
                            maxRadius: 20,
                            backgroundImage: sidejobData.fotoProfil == null
                                ? NetworkImage(
                                    'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                                : NetworkImage(
                                    mediaUrlUsers + sidejobData.fotoProfil),
                          ),
                          title: Text(
                            sidejobData.name,
                            style: Style.appStyle18b,
                          ),
                          subtitle: (totalRRVM?.listTotalRR?.isEmpty ?? true)
                              ? Text('Memuat Rating Pengguna')
                              : Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    totalRRVM.listTotalRR == null
                                        ? Text('Memuat Rating Pengguna')
                                        : Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              SmoothStarRating(
                                                isReadOnly: true,
                                                allowHalfRating: true,
                                                filledIconData: Icons.star,
                                                halfFilledIconData:
                                                    Icons.star_half,
                                                defaultIconData:
                                                    Icons.star_border,
                                                starCount: 5,
                                                rating: double.parse(totalRRVM
                                                        ?.listTotalRR[0]
                                                        ?.totalrating ??
                                                    0.0),
                                                size: 24,
                                                color: Colors.yellow,
                                                borderColor: Colors.yellow,
                                                spacing: 1,
                                              ),
                                              SizedBox(
                                                width: 4,
                                              ),
                                              Text(
                                                '${totalRRVM?.listTotalRR[0]?.totalrating ?? '0.0'}',
                                                style:
                                                    Style.appStyle18yelloww900,
                                              ),
                                              SizedBox(
                                                width: 4,
                                              ),
                                              Text(
                                                '(${totalRRVM?.listTotalRR[0]?.totalreview ?? '0'}) ',
                                                style: Style.appStyle18,
                                              ),
                                            ],
                                          ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      totalRRVM?.listTotalRR[0]?.ratingtype ??
                                          'Memuat ...',
                                      style: Style.appStyle18b,
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                  ],
                                ),
                          trailing: ElevatedButton(
                            child: Text(
                              "Lihat Profil",
                              style: Style.appStyleDefaultP,
                            ),
                            onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AnotherProfileDJV(
                                  sidejobData: sidejobData,
                                  totalJobsData: totalJobsVM.listTotalJobs[0],
                                ),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.transparent,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                side: BorderSide(color: Style.primaryColor),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, top: 0, bottom: 0),
                            child: Text(
                              'Riwayat Penyedia',
                              textAlign: TextAlign.justify,
                              style: Style.appStyle16Bold,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, top: 0, bottom: 0),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RatingReviewProfile(
                                      idUser: sidejobData.idJobprovider,
                                    ),
                                  ),
                                );
                              },
                              child: Text(
                                'Lihat Selengkapnya>>',
                                style: Style.appStyleDefaultP,
                              ),
                            ),
                          ),
                        ],
                      ),
                      ratingReviewProfileVM?.listRatingReviewProfile == null ? Center(child: CircularProgressIndicator()) : Container(
                        color: Style.bgColor,
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: ratingReviewProfileVM
                                      ?.listRatingReviewProfile?.length ==
                                  null
                              ? 0
                              : ratingReviewProfileVM
                                          ?.listRatingReviewProfile?.length <
                                      3
                                  ? ratingReviewProfileVM
                                      ?.listRatingReviewProfile?.length
                                  : 3,
                          padding: const EdgeInsets.only(bottom: 0),
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) {
                            final ModelRatingReviewProfile data =
                                ratingReviewProfileVM
                                    .listRatingReviewProfile[index];
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ExpansionTile(
                                  leading: CircleAvatar(
                                      maxRadius: 24,
                                      backgroundImage: ratingReviewProfileVM
                                                  ?.listRatingReviewProfile[
                                                      index]
                                                  ?.fotoProfil ==
                                              null
                                          ? NetworkImage(
                                              'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                                          : NetworkImage(mediaUrlUsers +
                                              ratingReviewProfileVM
                                                  .listRatingReviewProfile[
                                                      index]
                                                  .fotoProfil)),
                                  title: Text(
                                    '${data.jobName}\n(${data.name})',
                                    style: TextStyle(
                                        color: data.role == 'pekerja'
                                            ? Colors.blueAccent
                                            : Colors.green,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                  subtitle: Text(
                                    data.endDate != null
                                        ? 'Selesai : ${data.endDate.toString().substring(5, 7)}-${data.endDate.toString().substring(8, 10)}-${data.endDate.toString().substring(0, 4)}'
                                        : '',
                                  ),
                                  initiallyExpanded: true,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 8, horizontal: 16),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.all(8),
                                                child: Text(
                                                  data.idUser2 ==
                                                          userProvider.user.id
                                                              .toString()
                                                      ? 'Rating dari Anda'
                                                      : 'Rating dari pekerja',
                                                  style: Style.appStyle16Bold,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(8),
                                                child: SmoothStarRating(
                                                  isReadOnly: true,
                                                  allowHalfRating: true,
                                                  filledIconData: Icons.star,
                                                  halfFilledIconData:
                                                      Icons.star_half,
                                                  defaultIconData:
                                                      Icons.star_border,
                                                  starCount: 5,
                                                  rating: double.parse(
                                                      data.bintang),
                                                  size: 20,
                                                  color: Colors.yellow,
                                                  borderColor: Colors.yellow,
                                                  spacing: 1,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(8),
                                                child: Text(
                                                  data.keterangan,
                                                  style: Style.appStyle16,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                  trailing: Icon(Icons.arrow_drop_down_rounded,
                                      color: Colors.black),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                      SizedBox(height: 32),
                      SizedBox(height: 32),
                    ],
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 16, top: 16),
                    child: Container(
                      color: Style.bgColor,
                      child: token != null
                          ? sidejobData.idJobseeker != null
                              ? DetailJobButtonJSNotNull()
                              : userProvider.user.id.toString() ==
                                      sidejobData.idJobprovider
                                  ? DetailJobSameJobProvider()
                                  : sidejobData.idJobseeker ==
                                          userProvider.user.id.toString()
                                      ? DetailJobSameJobSeeker(index: index)
                                      : DetailJobNotSameJobSeeker(
                                          index: index,
                                          sidejobData: sidejobData,
                                        )
                          : DetailJobLoggedOut(
                              index: index, sidejobData: sidejobData),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),
            ],
          ),
        ),
      ),
    );
  }
}
