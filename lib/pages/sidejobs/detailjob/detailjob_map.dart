import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:pingjobs/component/line_string.dart';
import 'package:pingjobs/component/network_helper.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 0;

class DetailJobMap extends StatefulWidget {
  final ModelSideJob sidejobData;

  const DetailJobMap({
    Key key,
    this.sidejobData,
  }) : super(key: key);

  @override
  _DetailJobMapState createState() => _DetailJobMapState(
        sidejobData,
      );
}

class _DetailJobMapState extends State<DetailJobMap>
    with SingleTickerProviderStateMixin {
  // posisi kamera
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(-7.1143938, 112.4200467),
    zoom: 15,
  );

  ModelSideJob sidejobData;

  // deklarasi pin custom marker
  BitmapDescriptor sourceIcon;
  BitmapDescriptor trackingIcon;
  BitmapDescriptor destinationIcon;

  // lokasi awal pengguna dan lokasi saat ini saat berpindah tempat
  LocationData currentLocation;
  LocationData startLocation;

  // referensi ke lokasi tujuan
  LocationData destinationLocation;

  Location location;

  // data rute dari request http
  var data;

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set<Marker>();
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  Polyline polyline;
  double xJPin;
  double yJPin;
  double southWestLat;
  double southWestLng;
  double northEastLat;
  double northEastLng;

  _DetailJobMapState(this.sidejobData);

  @override
  void initState() {
    super.initState();
    xJPin = double.parse(sidejobData.xJPin);
    yJPin = double.parse(sidejobData.yJPin);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
    // inisialisasi lokasi
    location = new Location();
    // membuat pin
    setSourceAndDestinationIcons();
    // request rute dari API
    getJsonData();
    // listen perubahan lokasi dari event onLocationChanged
    location.onLocationChanged.listen((LocationData cLocation) {
      // currentLocation terdapat Lat dan Longtude dari posisi user scr realtime
      currentLocation = cLocation;
      if (currentLocation.latitude <= destinationLocation.latitude &&
          currentLocation.longitude <= destinationLocation.longitude) {
        southWestLat = currentLocation.latitude;
        northEastLat = destinationLocation.latitude;
        southWestLng = currentLocation.longitude;
        northEastLng = destinationLocation.longitude;
      } else if(currentLocation.longitude <= destinationLocation.longitude){
        southWestLat = destinationLocation.latitude;
        northEastLat = currentLocation.latitude;
        southWestLng = currentLocation.longitude;
        northEastLng = destinationLocation.longitude;
      } else if(currentLocation.latitude <= destinationLocation.latitude){
        southWestLat = currentLocation.latitude;
        northEastLat = destinationLocation.latitude;
        southWestLng = destinationLocation.longitude;
        northEastLng = currentLocation.longitude;
      } else {
        southWestLat = destinationLocation.latitude;
        northEastLat = currentLocation.latitude;
        southWestLng = destinationLocation.longitude;
        northEastLng = currentLocation.longitude;
      }
      updatePinOnMap();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.2,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              myLocationEnabled: true,
              compassEnabled: true,
              tiltGesturesEnabled: false,
              mapType: MapType.normal,
              // cameraTargetBounds: CameraTargetBounds(
              //   LatLngBounds(
              //     southwest: LatLng(southWestLat ?? 0, southWestLng ?? 0),
              //     northeast: LatLng(northEastLat ?? 0, northEastLng ?? 0),
              //   ),
              // ),
              initialCameraPosition: CameraPosition(
                target: LatLng(xJPin ?? 0, yJPin ?? 0),
                zoom: 15,
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
                showPinsonMap();
              },
              markers: _markers,
              polylines: _polylines,
            ),
          ),
        ),
      ],
    );
  }

  void getJsonData() async {
    // Buat instance Class NetworkHelper yang menggunakan paket http
    // untuk meminta data ke server dan menerima respons sebagai format JSON
    await setInitialLocation();
    NetworkHelper network;
    network = NetworkHelper(
      sourceLng: startLocation.longitude,
      sourceLat: startLocation.latitude,
      destLng: destinationLocation.longitude,
      destLat: destinationLocation.latitude,
    );
    print(
      "CurrentLng : " +
          currentLocation.longitude.toString() +
          "CurrentLat : " +
          currentLocation.latitude.toString(),
    );
    print(
      "StartLng : " +
          startLocation.longitude.toString() +
          "StartLat : " +
          startLocation.latitude.toString(),
    );
    print(
      "DestLng : " +
          destinationLocation.longitude.toString() +
          "DestLat : " +
          destinationLocation.latitude.toString(),
    );
    try {
      // getData () mengembalikan data decoded json
      data = await network.getData();

      // dapat mencapai data JSON yang diinginkan secara manual
      LineString ls =
          LineString(data['features'][0]['geometry']['coordinates']);

      for (int i = 0; i < ls.lineString.length; i++) {
        polylineCoordinates.add(
          LatLng(ls.lineString[i][1], ls.lineString[i][0]),
        );
      }
      //removePolyLines();
      if (sidejobData.idJobprovider !=
          Provider.of<UserProvider>(context, listen: false).user.id.toString())
        setPolyLines();
    } catch (e) {
      print(e);
    }
  }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();
    print(
      "CurrentLng : " +
          currentLocation.longitude.toString() +
          "CurrentLat : " +
          currentLocation.latitude.toString(),
    );
    startLocation = LocationData.fromMap({
      "longitude": currentLocation.longitude,
      "latitude": currentLocation.latitude,
    });

    print(
      "StartLng : " +
          startLocation.longitude.toString() +
          "StartLat : " +
          startLocation.latitude.toString(),
    );

    destinationLocation = LocationData.fromMap({
      "latitude": double.parse(sidejobData.xJPin),
      "longitude": double.parse(sidejobData.yJPin)
    });

    print(
      "DestLng : " +
          destinationLocation.longitude.toString() +
          "DestLat : " +
          destinationLocation.latitude.toString(),
    );

    if (startLocation.latitude <= destinationLocation.latitude &&
        startLocation.longitude <= destinationLocation.longitude) {
      southWestLat = startLocation.latitude;
      northEastLat = destinationLocation.latitude;
      southWestLng = startLocation.longitude;
      northEastLng = destinationLocation.longitude;
    } else if(startLocation.longitude <= destinationLocation.longitude){
      southWestLat = destinationLocation.latitude;
      northEastLat = startLocation.latitude;
      southWestLng = startLocation.longitude;
      northEastLng = destinationLocation.longitude;
    } else if(startLocation.latitude <= destinationLocation.latitude){
      southWestLat = startLocation.latitude;
      northEastLat = destinationLocation.latitude;
      southWestLng = destinationLocation.longitude;
      northEastLng = startLocation.longitude;
    } else {
      southWestLat = destinationLocation.latitude;
      northEastLat = startLocation.latitude;
      southWestLng = destinationLocation.longitude;
      northEastLng = startLocation.longitude;
    }
  }

  // membuat pin
  void setSourceAndDestinationIcons() {
    sourceIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange);
    trackingIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure);
    destinationIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen);
  }

  // memusatkan tampilan ke lokasi terkini
  void centerOnMap() async {
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(
      CameraUpdate.newCameraPosition(cPosition),
    );
  }

  // memperbarui pin
  void updatePinOnMap() async {
    // membuat posisi baru di map dan kamera mengikutinya
    // CameraPosition cPosition = CameraPosition(
    //   zoom: CAMERA_ZOOM,
    //   tilt: CAMERA_TILT,
    //   bearing: CAMERA_BEARING,
    //   target: LatLng(currentLocation.latitude, currentLocation.longitude),
    // );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(southWestLat ?? 0, southWestLng ?? 0),
          northeast: LatLng(northEastLat ?? 0, northEastLng ?? 0),
        ),
        75));
    // controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));

    // agar flutter diberi tahu untuk mengubah posisi kamera
    setState(() {
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      // hapus penanda dan tambahkan lagi untuk pembaruan
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      _markers.add(
        Marker(
          markerId: MarkerId('trackingPin'),
          position: pinPosition,
          icon: trackingIcon,
        ),
      );
    });
  }

  // membuat rute dari 2 titik
  setPolyLines() {
    polyline = Polyline(
      polylineId: PolylineId("polyline"),
      color: Style.primaryColor,
      points: polylineCoordinates,
      width: 4,
    );
    _polylines.remove(polyline);
    _polylines.add(polyline);
    setState(() {});
  }

  void showPinsonMap() {
    // set lokasi rute
    setPolyLines();
    setState(() {
      // set lokasi rute
      setPolyLines();
      _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
      _markers.removeWhere((m) => m.markerId.value == 'destPin');
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      // add pin lokasi tujuan
      _markers.add(
        Marker(
          markerId: MarkerId('destPin'),
          position: LatLng(xJPin, yJPin),
          icon: destinationIcon,
          infoWindow: InfoWindow(
            title: "Lokasi Pekerjaan Sampingan",
            snippet: sidejobData.jobName,
          ),
        ),
      );

      // add pin lokasi terkini/awal
      _markers.add(
        Marker(
          markerId: MarkerId('sourcePin'),
          position: LatLng(startLocation.latitude, startLocation.longitude),
          icon: sourceIcon,
          infoWindow: InfoWindow(
            title: "Posisi Awal Anda",
            snippet: "Posisi awal Kamu Disini !",
          ),
        ),
      );

      _markers.add(
        Marker(
          markerId: MarkerId('trackingPin'),
          position: LatLng(currentLocation.latitude, currentLocation.longitude),
          icon: trackingIcon,
          infoWindow: InfoWindow(
            title: "Posisi Anda Terkini",
            snippet: "Kamu Sekarang Disini !",
          ),
        ),
      );
    });
  }
}
