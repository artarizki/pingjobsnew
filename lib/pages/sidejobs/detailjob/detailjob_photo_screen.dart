import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:optimized_cached_image/optimized_cached_image.dart';
import 'package:pingjobs/model/sidejob_model.dart';

class DetailJobPhoto extends StatefulWidget {
  const DetailJobPhoto({Key key, this.sidejobData}) : super(key: key);
  final ModelSideJob sidejobData;

  @override
  _DetailJobPhotoState createState() => _DetailJobPhotoState(sidejobData);
}

class _DetailJobPhotoState extends State<DetailJobPhoto> {
  final ModelSideJob sidejobData;
  String media = 'assets/images/untitled.jpg';
  String mediaUrl =
      'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';

  _DetailJobPhotoState(this.sidejobData);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.2,
          child: OptimizedCacheImage(
            imageUrl: mediaUrl + sidejobData.jobPhotovideo,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                Container(
                  height: 48,
                  child: Center(
                    child: LinearProgressIndicator(
                      value: downloadProgress.progress,
                    ),
                  ),
                ),
            errorWidget: (context, url, error) => Image.asset(media),
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
}
