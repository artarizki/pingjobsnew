import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/sidejobs/job/apply_job_screen.dart';

class DetailJob extends StatefulWidget {
  const DetailJob({Key key, this.sidejobData}) : super(key: key);

  final ModelSideJob sidejobData;

  @override
  _DetailJobState createState() => _DetailJobState(sidejobData);
}

class _DetailJobState extends State<DetailJob> {
  final double infoHeight = 364.0;
  final ModelSideJob sidejobData;

  _DetailJobState(this.sidejobData);

  @override
  Widget build(BuildContext context) {
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;
    return Container(
      color: Style.bgColor,
      child: Scaffold(
//        appBar: getAppBar(context),
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1.2,
                  child: Image.asset(sidejobData.jobPhotovideo),
                ),
              ],
            ),
            Positioned(
              top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: Style.bgColor,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0)),
                  boxShadow: <BoxShadow>[
                    Style.shadowElement,
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: SingleChildScrollView(
                    child: Container(
                      constraints: BoxConstraints(
                          minHeight: infoHeight,
                          maxHeight: tempHeight > infoHeight
                              ? tempHeight
                              : infoHeight),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 32.0, left: 18, right: 16),
                            child: Text(
                              '${sidejobData.jobName}',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                letterSpacing: 0.27,
                                color: Colors.black87,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, left: 18, right: 16),
                            child: Text(
                              '${sidejobData.street}',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 18,
                                letterSpacing: 0.27,
                                color: Colors.black45,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, bottom: 8, top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Rp. ${toCurrencyString(sidejobData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    fontSize: 22,
                                    letterSpacing: 0.27,
                                    color: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        '4.4',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w200,
                                          fontSize: 22,
                                          letterSpacing: 0.27,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: Colors.blue,
                                        size: 24,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
//                          AnimatedOpacity(
//                            duration: const Duration(milliseconds: 500),
//                            opacity: opacity1,
//                            child: Padding(
//                              padding: const EdgeInsets.all(8),
//                              child: Row(
//                                children: <Widget>[
//                                  getTimeBoxUI('24', 'Classe'),
//                                  getTimeBoxUI('2hours', 'Time'),
//                                  getTimeBoxUI('24', 'Seat'),
//                                ],
//                              ),
//                            ),
//                          ),
                          Expanded(
                            child: AnimatedOpacity(
                              duration: const Duration(milliseconds: 200),
                              opacity: 1.0,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 16, right: 16, top: 8, bottom: 8),
                                child: Text(
                                  '${sidejobData.jobDescription}',
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14,
                                    letterSpacing: 0.27,
                                    color: Colors.black,
                                  ),
                                  maxLines: 11,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                          ),
                          AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: 1.0,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, bottom: 16, right: 16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 48,
                                    height: 48,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          color: Style.bgColor,
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(16.0),
                                          ),
                                          border: Border.all(
                                              color:
                                                  Colors.grey.withOpacity(0.2)),
                                        ),
                                        child: InkWell(
                                            onTap: () {
                                              // if(isWhatsAppInstalled) {
                                              FlutterOpenWhatsapp.sendSingleMessage(
                                                  '+62 856-5553-6949',
                                                  'Halo, saya ingin bertanya tentang lakurek.com ini');
                                              // } else {
                                              //   Platform.isAndroid ? StoreRedirect.redirect(androidAppId: "net.whatsapp.WhatsApp") : _launchWhatsAppStore();
                                              // }
                                            },
                                            child: Container(
                                              height: 48,
                                              decoration: BoxDecoration(
                                                color: Style.primaryColor,
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  Radius.circular(16.0),
                                                ),
                                                boxShadow: <BoxShadow>[
                                                  Style.shadowElement,
                                                ],
                                              ),
                                              child: Icon(
                                                FontAwesomeIcons.whatsapp,
                                                color: Colors.white,
                                                size: 40,
                                              ),
                                            ))),
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 48,
                                      decoration: BoxDecoration(
                                        color: Style.primaryColor,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(16.0),
                                        ),
                                        boxShadow: <BoxShadow>[
                                          Style.shadowElement,
                                        ],
                                      ),
                                      child: Center(
                                          child: InkWell(
                                        onTap: () {
                                          var persyaratan = sidejobData.persyaratan.toLowerCase();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => ApplyJob(),
                                            ),
                                          );
                                          // Navigator.push(
                                          //   context,
                                          //   MaterialPageRoute(
                                          //     builder: (_) => ApplySuccess(),
                                          //   ),
                                          // );
                                        },
                                        child: Text(
                                          'Apply Pekerjaan',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            letterSpacing: 0.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      )),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).padding.bottom,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: SizedBox(
                width: AppBar().preferredSize.height,
                height: AppBar().preferredSize.height,
                child: RawMaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  elevation: 0.0,
                  fillColor: Colors.black54,
//                  color: Colors.black54,
                  child: InkWell(
                    borderRadius:
                        BorderRadius.circular(AppBar().preferredSize.height),
                    child: Icon(
                      Icons.arrow_back,
                      size: 30.0,
                      color: Colors.white,
                    ),
                  ),
//                  padding: EdgeInsets.all(0.0),
                  shape: CircleBorder(),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
