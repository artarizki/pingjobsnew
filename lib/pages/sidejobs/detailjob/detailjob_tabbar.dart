import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/component/style.dart';

class DetailJobTabBar extends StatefulWidget {
  final TabController tabController;

  const DetailJobTabBar({Key key, this.tabController}) : super(key: key);
  @override
  _DetailJobTabBarState createState() => _DetailJobTabBarState(tabController);
}

class _DetailJobTabBarState extends State<DetailJobTabBar> {
  TabController tabController;

  _DetailJobTabBarState(this.tabController);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBar(
        unselectedLabelColor: Colors.black38,
        labelColor: Colors.black87,
        tabs: [
          new Tab(
            child: Text(
              'Image View',
              textAlign: TextAlign.center,
              style: Style.appStyleDefault,
            ),
          ),
          new Tab(
            child: Text(
              'Map View',
              textAlign: TextAlign.center,
              style: Style.appStyleDefault,
            ),
          )
        ],
        controller: tabController,
        indicatorColor: Colors.brown,
      ),
    );
  }
}
