import 'dart:async';
import 'dart:io';

import 'package:csc_picker/csc_picker.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/sidejobs/job/post_job_form2_screen.dart';
import 'package:pingjobs/pages/sidejobs/success/apply_success_screen.dart';

class ApplyJob extends StatefulWidget {
  const ApplyJob({Key key, this.persyaratan}) : super(key: key);

  final String persyaratan;

  @override
  _ApplyJobState createState() => _ApplyJobState(persyaratan);
}

class _ApplyJobState extends State<ApplyJob> with TickerProviderStateMixin {
  final _pengalamanController = TextEditingController();
  final _akunController = TextEditingController();
  String persyaratan;
  File fileKTP;
  File fileSIM;
  File fileX;
  File fileSertif;

  List<Asset> images1 = <Asset>[];
  List<String> filename1 = <String>[];

  _ApplyJobState(this.persyaratan);

  Future pickImageKTP() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    fileKTP = File(image.path);
    await cropImageKTP(File(image.path).path);
  }

  Future cropImageKTP(filePath) async {
    File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      maxWidth: 150,
      maxHeight: 150,
    );
    if (croppedImage != null) {
      fileKTP = croppedImage;
      setState(() {});
    }
  }

  Future pickImageSIM() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    fileSIM = File(image.path);
    await cropImageSIM(File(image.path).path);
  }

  Future cropImageSIM(filePath) async {
    File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      maxWidth: 150,
      maxHeight: 150,
    );
    if (croppedImage != null) {
      fileSIM = croppedImage;
      setState(() {});
    }
  }

  Future pickImageX() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    fileX = File(image.path);
    await cropImageX(File(image.path).path);
  }

  Future cropImageX(filePath) async {
    File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      maxWidth: 150,
      maxHeight: 150,
    );
    if (croppedImage != null) {
      fileX = croppedImage;
      setState(() {});
    }
  }

  Future pickImageSertif() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    fileSertif = File(image.path);
    await cropImageSertif(File(image.path).path);
  }

  Future cropImageSertif(filePath) async {
    File croppedImage = await ImageCropper.cropImage(
      sourcePath: filePath,
      maxWidth: 150,
      maxHeight: 150,
    );
    if (croppedImage != null) {
      fileSertif = croppedImage;
      setState(() {});
    }
  }

  Future<void> uploadSurat() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images1,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images1 = resultList;
    });
  }

  Widget buildGridView() {
    return Align(
        alignment: Alignment.center,
        child: GridView.count(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          crossAxisCount: 3,
          mainAxisSpacing: 2,
          children: List.generate(images1.length, (index) {
            Asset asset = images1[index];
            // print("IAMGES1 LENGTH : " + images1.length.toString());
            // print("ASSET NAME : " + asset.name);
            filename1.add(asset.name);
            return AssetThumb(
              asset: asset,
              width: 300,
              height: 300,
            );
          }),
        ));
  }

  final _formKey = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();

  FocusNode NodePengalaman;
  FocusNode NodeAkun;

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    NodePengalaman = FocusNode();
    NodeAkun = FocusNode();
  }

  @override
  void dispose() {
    NodePengalaman.dispose();
    NodeAkun.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Style.bgColor,
        child: Scaffold(
            key: _scafoldKey,
            appBar: getAppBar(context),
            backgroundColor: Colors.transparent,
            body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                        child: Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16.0, bottom: 32.0),
                            child: Text(
                              'Lengkapi data persyaratan',
                              style: Style.appStyle16,
                            ),
                          ),
                          persyaratan.contains('ktp')
                              ? fileKTP == null
                                  ? Text('Anda belum mengupload KTP Anda')
                                  : Image.file(fileKTP)
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('ktp')
                              ? MaterialButton(
                                  height: 48,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  elevation: 8,
                                  color: Style.primaryColor,
                                  child: Text(
                                    fileKTP == null
                                        ? 'Upload KTP'
                                        : 'Ganti Gambar',
                                    style: Style.appStyle16White,
                                  ),
                                  onPressed: pickImageKTP,
                                )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('sim')
                              ? fileSIM == null
                                  ? Text('Anda belum mengupload SIM Anda')
                                  : SizedBox(
                                      height: 100,
                                      width: 100,
                                      child: Image.file(fileSIM))
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('sim')
                              ? MaterialButton(
                                  height: 48,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  elevation: 8,
                                  color: Style.primaryColor,
                                  child: Text(
                                    fileSIM == null
                                        ? 'Upload SIM'
                                        : 'Ganti Gambar',
                                    style: Style.appStyle16White,
                                  ),
                                  onPressed: pickImageSIM,
                                )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('x')
                              ? fileX == null
                                  ? Text('Anda belum mengupload Foto Anda')
                                  : SizedBox(
                                      height: 100,
                                      width: 100,
                                      child: Image.file(fileX),
                                    )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('x')
                              ? MaterialButton(
                                  height: 48,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  elevation: 8,
                                  color: Style.primaryColor,
                                  child: Text(
                                    fileX == null
                                        ? 'Upload Foto'
                                        : 'Ganti Gambar',
                                    style: Style.appStyle16White,
                                  ),
                                  onPressed: pickImageX,
                                )
                              : SizedBox(),
                          persyaratan.contains('sertif')
                              ? fileSertif == null
                                  ? Text(
                                      'Anda belum mengupload Foto Sertifikat Anda')
                                  : SizedBox(
                                      height: 100,
                                      width: 100,
                                      child: Image.file(fileSertif),
                                    )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('sertif')
                              ? MaterialButton(
                                  height: 48,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  elevation: 8,
                                  color: Style.primaryColor,
                                  child: Text(
                                    fileSertif == null
                                        ? 'Upload Foto Sertifikat'
                                        : 'Ganti Gambar',
                                    style: Style.appStyle16White,
                                  ),
                                  onPressed: pickImageSertif,
                                )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('surat')
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16, horizontal: 0),
                                  // child: _image1 == null ? Text('Anda dapat upload referensi desain di bawah ini') : Image.file(_image1)
                                  child: images1.isEmpty
                                      ? Text(
                                          'Anda dapat upload / mengganti gambar sertifikat untuk persyaratan pekerjaan sampingan di bawah ini')
                                      : buildGridView() /*()Image.file(_image)*/,
                                )
                              : SizedBox(),
                          persyaratan.contains('surat')
                              ? Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: double.infinity,
                                    child: MaterialButton(
                                      height: 48,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      elevation: 8,
                                      color: Style.primaryColor,
                                      child: Text(
                                        images1.length == 0
                                            ? 'Upload Gambar'
                                            : 'Ganti Gambar',
                                        style: Style.appStyle16White,
                                      ),
                                      onPressed: uploadSurat,
                                    ),
                                  ),
                                )
                              : SizedBox(),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 0),
                          ),
                          SizedBox(height: 16),
                          persyaratan.contains('pengalaman')
                              ? TextFormField(
                                  controller: _pengalamanController,
                                  decoration: InputDecoration(
                                    labelText: 'Pengalaman Pekerja Sampingan',
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 8.0),
                                    hintText:
                                        '3 tahun bekerja di perusahaan A, ...',
                                    hintStyle: TextStyle(
                                      color: Colors.black26,
                                    ),
                                  ),
                                  maxLength: 50,
                                  maxLines: 3,
                                  textCapitalization:
                                      TextCapitalization.sentences,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.multiline,
                                  validator: (value) {
                                    if (value?.isEmpty ?? true) {
                                      return 'Harap isi Pengalaman Anda';
                                    }
                                    return null;
                                  },
                                  focusNode: NodePengalaman,
                                )
                              : SizedBox(),
                          SizedBox(height: 16),
                          persyaratan.contains('akun')
                              ? TextFormField(
                                  controller: _akunController,
                                  decoration: InputDecoration(
                                    labelText: 'Nama Akun',
                                    contentPadding:
                                        EdgeInsets.symmetric(vertical: 8.0),
                                    hintText: 'Masukkan nama akun Anda, ...',
                                    hintStyle: TextStyle(
                                      color: Colors.black26,
                                    ),
                                  ),
                                  maxLength: 50,
                                  maxLines: 3,
                                  textCapitalization:
                                      TextCapitalization.sentences,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.multiline,
                                  validator: (value) {
                                    if (value?.isEmpty ?? true) {
                                      return 'Harap isi Akun Anda';
                                    }
                                    return null;
                                  },
                                  focusNode: NodeAkun,
                                )
                              : SizedBox(),
                          Padding(
                              padding: const EdgeInsets.only(top: 32.0),
                              child: Container(
                                width: double.infinity,
                                child: MaterialButton(
                                  height: 48,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  color: Style.primaryColor,
                                  child: Text(
                                    'Selanjutnya',
                                    style: Style.appStyle16White,
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      print('is validate');
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (_) {
                                          return ApplySuccess();
                                        }),
                                      );
                                    } else
                                      print('is not validate');
                                  },
                                ),
                              ))
                        ],
                      ),
                    ))))),
      ),
    );
  }

  Widget getAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Stack(
        children: <Widget>[
          AppBar(
            brightness: Brightness.dark,
            centerTitle: true,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Style.primaryColor,
            title: Text(
              'Data Lamaran',
            ),
          ),
        ],
      ),
    );
  }
}
