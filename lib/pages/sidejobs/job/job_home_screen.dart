import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/sidejobs/job/side_job_list_view.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SideJobHomeScreen extends StatefulWidget {
  @override
  _SideJobHomeScreenState createState() => _SideJobHomeScreenState();
}

class _SideJobHomeScreenState extends State<SideJobHomeScreen>
    with TickerProviderStateMixin {
  // controller animasi
  AnimationController animationController;
  Timer searchOnStoppedTyping;
  bool tapped = false;
  // list menyimpan data dummy pekerjaan sampingan
  final ScrollController _scrollController = ScrollController();

  TextEditingController _textController = new TextEditingController();

  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(const Duration(days: 5));

  List<ModelSideJob> _searchResult = [];
  LocationData currentLocation;
  Location location;
  List<int> perIndexJob;
  List<ModelSideJob> list = [];

  int get count => list.length;

  // inisialisasi state saat aplikasi dijalankan
  @override
  void initState() {
    // durasi animasi
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    // inisialisasi lokasi
    location = new Location();
    setLoc();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  void setLoc() async {
    await setInitialLocation();
  }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();
  }

  @override
  Widget build(BuildContext context) {
    final SideJobVM sideJobVM = Provider.of<SideJobVM>(context);
    sideJobVM.fetchData();
    return Scaffold(
      appBar: _buildAppBar(context),
      body: (sideJobVM?.listSideJob?.isEmpty ?? true)
          ? _sideJobNull(sideJobVM)
          : _sideJobNotNull(sideJobVM),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      brightness: Brightness.dark,
      title: Text('Beranda', style: Style.appStyleAppBar),
      backgroundColor: Style.primaryColor,
      centerTitle: true,
      automaticallyImplyLeading: false,
    );
  }

  Widget _textFieldNull(SideJobVM sideJobVM) {
    _searchResult.clear();
    sideJobVM.clearData();
    sideJobVM?.listSideJobfound?.clear();
    sideJobVM.fetchData();
    return Container(
      color: Style.bgColor,
      child: ListView.builder(
        itemCount: sideJobVM?.listSideJob?.length ?? 0,
        padding: const EdgeInsets.only(top: 8),
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          final ModelSideJob data = sideJobVM.listSideJob[index];
          data.jobPhotovideo = data.jobPhotovideo.contains(',')
              ? data.jobPhotovideo.substring(0, data.jobPhotovideo.indexOf(','))
              : data.jobPhotovideo;
          final int count = sideJobVM.listSideJob.length > 10
              ? 10
              : sideJobVM.listSideJob.length;
          final Animation<double> animation =
              Tween<double>(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
              parent: animationController,
              curve: Interval((1 / count) * index, 1.0,
                  curve: Curves.fastOutSlowIn),
            ),
          );
          animationController.forward();
          return SideJobListView(
            callback: () {},
            sidejobData: data,
            animation: animation,
            animationController: animationController,
            currentLocation: currentLocation,
          );
        },
      ),
    );
  }

  Widget _textFieldNotNull(SideJobVM sideJobVM) {
    return Container(
      color: Style.bgColor,
      child: (sideJobVM?.listSideJobfound?.isNotEmpty ?? true)
          ? ListView.builder(
              itemCount: sideJobVM?.listSideJobfound?.length ?? 0,
              padding: const EdgeInsets.only(top: 8),
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                final ModelSideJob data = sideJobVM.listSideJobfound[index];
                final int count = sideJobVM.listSideJobfound.length > 10
                    ? 10
                    : sideJobVM.listSideJobfound.length;
                final Animation<double> animation =
                    Tween<double>(begin: 0.0, end: 1.0).animate(
                  CurvedAnimation(
                    parent: animationController,
                    curve: Interval((1 / count) * index, 1.0,
                        curve: Curves.fastOutSlowIn),
                  ),
                );
                animationController.forward();
                return SideJobListView(
                  callback: () {},
                  sidejobData: data,
                  animation: animation,
                  animationController: animationController,
                );
              },
            )
          : Center(
              child: Text('Pekerjaan "${_textController.text}" Tidak Ditemukan',
                  style: Style.appStyle32),
            ),
    );
  }

  Widget _sideJobNotNull(SideJobVM sideJobVM) {
    return Stack(
      children: <Widget>[
        InkWell(
          splashColor: Colors.transparent,
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Column(
            children: <Widget>[
              Expanded(
                  child: NestedScrollView(
                      controller: _scrollController,
                      headerSliverBuilder:
                          (BuildContext context, bool innerBoxIsScrolled) {
                        return <Widget>[
                          SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  getSearchBarUI(sideJobVM),
                                ],
                              );
                            }, childCount: 1),
                          ),
                          SliverPersistentHeader(
                            pinned: true,
                            floating: true,
                            delegate: ContestTabHeader(
                              getFilterBarUI(),
                            ),
                          ),
                        ];
                      },
                      body: sideJobVM.listSideJob == null
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : /*sideJobVM.listSideJobfound.length == 0 ||*/ tapped == true ||_textController
                                      .text.isEmpty ||
                                  _textController.text == ""
                              ? _textFieldNull(sideJobVM)
                              : _textFieldNotNull(sideJobVM)))
            ],
          ),
        ),
      ],
    );
  }

  Widget _sideJobNull(SideJobVM sideJobVM) {
    return Center(
      child: Container(
        width: 250,
        height: 250,
        child: Image.asset('assets/images/dataerror.png'),
      ),
    );
  }

  // menampilkan tombol search
  Widget getSearchBarUI(SideJobVM sideJobVM) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16, top: 8, bottom: 8),
              child: Container(
                decoration: BoxDecoration(
                  color: Style.bgColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(16.0),
                  ),
                  boxShadow: <BoxShadow>[
                    Style.shadowElement,
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 4, bottom: 4),
                  child: TextField(
                    controller: _textController,
                    onChanged: (String txt) {
                      _searchResult.clear();
                      const duration = Duration(seconds: 1);
                      if(searchOnStoppedTyping != null) {
                        setState(() => searchOnStoppedTyping.cancel());
                      }
                      setState(() => searchOnStoppedTyping = new Timer(duration, (){
                        tapped = false;
                        _searchResult = sideJobVM.listSideJob.where((listJob) =>
                            listJob.jobName
                                .toLowerCase()
                                .contains(txt.toLowerCase())).toList();
                        sideJobVM.searchData(_searchResult);
                      }));
                    },
                    style: Style.appStyle18,
                    cursorColor: Style.primaryColor,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Cari Pekerjaan Sampingan...',
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Style.primaryColor,
              borderRadius: const BorderRadius.all(
                Radius.circular(16.0),
              ),
              boxShadow: <BoxShadow>[
                Style.shadowElement,
              ],
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: const BorderRadius.all(
                  Radius.circular(16.0),
                ),
                onTap: () {
                  if(_textController.text.isEmpty || _textController.text == "") {
                    FocusScope.of(context).requestFocus(FocusNode());
                  } else {
                    tapped = true;
                    _textController.clear();
                    _searchResult.clear();
                    sideJobVM.clearData();
                    // if (txt.isEmpty) {
                    //   setState(() {});
                    //   return;
                    // }
                    //
                    // setState(() {});
                    setState(() {
                      tapped = true;
                      _textController.clear();
                      _searchResult.clear();
                      sideJobVM.clearData();
                    });
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: _textController.text.isEmpty
                      ? Icon(FontAwesomeIcons.search,
                          size: 20, color: Style.bgColor)
                      : Icon(FontAwesomeIcons.times,
                          size: 20, color: Style.bgColor),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getFilterBarUI() {
    return Stack(
      children: <Widget>[
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            height: 24,
            decoration: BoxDecoration(
              color: Style.bgColor,
              boxShadow: <BoxShadow>[
                Style.shadowElement,
              ],
            ),
          ),
        ),
        Container(
          color: Style.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 4),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Pilih Job Sampingan Anda',
                      style: TextStyle(
                        fontWeight: FontWeight.w100,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        const Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Divider(
            height: 1,
          ),
        )
      ],
    );
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.searchUI,
  );

  final Widget searchUI;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return searchUI;
  }

  @override
  double get maxExtent => 52.0;

  @override
  double get minExtent => 52.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
