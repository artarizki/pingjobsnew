import 'dart:async';
import 'package:flutter/services.dart';

// import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:pingjobs/component/style.dart';

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 0;
const LatLng SOURCE_LOCATION = LatLng(-7.11439382, 112.4200467);
const double sourceLat = -7.11439382;
const double sourceLng = 112.4200467;
const LatLng DEST_LOCATION = LatLng(-7.1150422, 112.4158926);

// const double destLat = -7.1150422;
// const double destLng = 112.4158926;

class JobLocationPicker extends StatefulWidget {
  const JobLocationPicker({Key key}) : super(key: key);

  @override
  _JobLocationPickerState createState() => _JobLocationPickerState();
}

class _JobLocationPickerState extends State<JobLocationPicker> {
  _JobLocationPickerState();

  // getAddress
  List<Address> addresses;
  var first;

  // url API
  final String url = 'https://api.openrouteservice.org/v2/directions/';

  // parameter route to destination (driving car, bicycle, etc)
  final String pathParam = 'driving-car'; // Change it if you want

  // Key API
  String openrouteAPIKey =
      "5b3ce3597851110001cf6248d5988198424d4dfda2ea6d1bffbbf7a4";
  String googleAPIKey = "AIzaSyDeh5BUkFoByEsCMZgJHg02D6XpIn_i5n8";

// deklarasi pin custom marker
  BitmapDescriptor sourceIcon;

// lokasi awal pengguna dan lokasi saat ini saat berpindah tempat
  LocationData currentLocation;
  LocationData selectedLocation;

// wrapper API Location
  Location location;
  Completer<GoogleMapController> _controller = Completer();

  // deklarasi marker, polyline untuk rute, dan koordinat
  List<Marker> _markers = [];

  // data rute dari request http
  double pickLat, pickLng, pickk, pLat, pLng;

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    // inisialisasi lokasi
    location = new Location();
    setSourceAndDestinationIcons();
  }

  // membuat pin
  void setSourceAndDestinationIcons() async {
    await setInitialLocation();
    sourceIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue);
  }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();
    await getAddress(currentLocation.latitude, currentLocation.longitude);
    print("CurrentLng : " +
        currentLocation.longitude.toString() +
        "CurrentLat : " +
        currentLocation.latitude.toString());
  }

  // memusatkan tampilan ke lokasi terkini
  void centerOnMap() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 15.0,
      ),
    ));
  }

  // memperbarui pin
  void updatePinOnMap() async {
    // membuat posisi baru di map dan kamera mengikutinya
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));

    // agar flutter diberi tahu untuk mengubah posisi kamera
    setState(() {
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      // hapus penanda dan tambahkan lagi untuk pembaruan
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      _markers.add(Marker(
        markerId: MarkerId('trackingPin'),
        position: pinPosition,
        icon: sourceIcon,
      ));
    });
  }

  // void _updateMarker(CameraPosition position) {
  //   Position newMarkerPosition = Position(
  //       latitude: position.target.latitude,
  //       longitude: position.target.longitude);
  //   var markers;
  //   Marker marker = markers["1"];
  //
  //   setState(() {
  //     markers["1"] = marker.copyWith(
  //       positionParam: LatLng(newMarkerPosition.latitude, newMarkerPosition.longitude));
  //   });
  // }

  Future getAddress(lat, long) async {
    try {
      pickLat = lat;
      pickLng = long;
      final coordinates = new Coordinates(lat, long);
      addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      first = addresses.first.addressLine;
      setState(() {
        pickLat = lat;
        pickLng = long;
        first = addresses.first.addressLine;
      });
      print("${first.featureName} : ${first.addressLine}");
    } catch (e) {
      print("Error occured: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    // setInitLoc();
    return Container(
        color: Style.bgColor,
        child: Scaffold(
          appBar: AppBar(
              title: Text('Pilih Lokasi Pekerjaan'),
              backgroundColor: Style.primaryColor,
              brightness: Brightness.dark,
              centerTitle: true,
              automaticallyImplyLeading: true,
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.of(context).pop(),
              )),
          backgroundColor: Style.bgColor,
          body: currentLocation == null
              ? Center(child: CircularProgressIndicator())
              : Stack(
                  children: <Widget>[
                    Container(
                        // height: MediaQuery.of(context).size.height*0.7,
                        height: MediaQuery.of(context).size.height * 0.6,
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          children: <Widget>[
                            GoogleMap(
                              myLocationEnabled: true,
                              compassEnabled: true,
                              tiltGesturesEnabled: false,
                              mapType: MapType.normal,
                              initialCameraPosition: CameraPosition(
                                target: LatLng(currentLocation.latitude,
                                    currentLocation.longitude),
                                zoom: 15,
                              ),
                              onMapCreated: (GoogleMapController controller) {
                                _controller.complete(controller);
                              },
                              onCameraIdle: () {
                                getAddress(pLat, pLng);
                              },
                              onCameraMove: (CameraPosition position) {
                                // setState(() {
                                //   pLat = position.target.latitude;
                                //   pLng = position.target.longitude;
                                // });
                                setState(() {
                                  pLat = position.target.latitude;
                                  pLng = position.target.longitude;
                                  _markers.first = _markers.first
                                      .copyWith(positionParam: position.target);
                                });
                              },
                              markers: _markers.toSet(),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.location_pin,
                                color: Style.secondaryColor,
                                size: 40,
                              ),
                            ),
                          ],
                        )),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          // margin: EdgeInsets.symmetric(horizontal: 16),
                          color: Style.bgColor,
                          height: MediaQuery.of(context).size.height * 0.275,
                          width: double.infinity,
                          child: Card(
                            color: Style.bgColor,
                            elevation: 10,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0))),
                            shadowColor: Colors.grey.withOpacity(0.4),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(
                                        top: 8, left: 16, right: 16, bottom: 8),
                                    child: Text('Alamat Lokasi yang Dipilih',
                                        style: Style.appStyle16Light)),
                                first == null
                                    ? Center(child: CircularProgressIndicator())
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 8, left: 16, right: 16),
                                        child: Text(
                                          first.toString(),
                                          style: Style.appStyle16Bold,
                                        ),
                                      ),
                                SizedBox(height: 8),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: Style.primaryColor),
                                  child: Text('Pilih Alamat'),
                                  onPressed: () {
                                    List<String> list = <String>[];
                                    list.add(pickLat.toString());
                                    list.add(pickLng.toString());
                                    list.add(first.toString());
                                    Navigator.pop(context, list);
                                    print(
                                        "PICK : ${pickLat.toString()} ${pickLng.toString()}");
                                    // print("PICKLATLNG : ${selectedLocation.latitude.toString()} ${selectedLocation.longitude.toString()}");
                                    // print("PICK : ${pickLat.toString()} ${pickLng.toString()}");
                                  },
                                )
                              ],
                            ),
                          ),
                        ))
                  ],
                ),
          // floatingActionButton: FloatingActionButton(
          //   onPressed: () {
          //     // tombol untuk memusatkan tampilan ke lokasi user terkini
          //     centerOnMap();
          //   },
          //   child: Icon(Icons.accessibility_new),
          // ),
        ));
  }
}
