import 'dart:io';
import 'dart:async';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http_parser/http_parser.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pingjobs/component/fasilitas_controller.dart';
import 'package:pingjobs/component/persyaratan_controller.dart';
import 'package:pingjobs/pages/sidejobs/job/job_location_picker.dart';
import 'package:pingjobs/provider/postjob_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class PostJobForm2 extends StatefulWidget {
  const PostJobForm2(
      {Key key,
      this.category,
      this.alamat,
      this.kota,
      this.date,
      this.jadwal,
      this.startTime,
      this.endTime,
      this.pickLat,
      this.pickLng})
      : super(key: key);

  final String category, alamat, kota, date, jadwal, startTime, endTime;
  final double pickLat, pickLng;

  @override
  _PostJobForm2State createState() => _PostJobForm2State(category, alamat, kota,
      date, jadwal, startTime, endTime, pickLat, pickLng);
}

class _PostJobForm2State extends State<PostJobForm2>
    with TickerProviderStateMixin {
  final _judulController = TextEditingController();
  final _deskripsiController = TextEditingController();
  final _gajiController = MoneyMaskedTextController(
    precision: 0,
    decimalSeparator: '',
    thousandSeparator: '.',
    leftSymbol: 'Rp. ',
  );

  final List<PersyaratanControllers> persyaratanControllers =
      <PersyaratanControllers>[];
  final List<FasilitasControllers> fasilitasControllers =
      <FasilitasControllers>[];
  String allPersyaratan;
  String allFasilitas;

  File tmpFile;
  var tmpimage1;
  final picker = ImagePicker();
  List<Asset> images1 = <Asset>[];
  List<String> filename1 = <String>[];
  var pathimage;
  PickResult selectedPlace;
  double pickLatData, pickLngData;
  var message;

  final String category, alamat, kota, date, jadwal, startTime, endTime;
  double pickLat, pickLng;
  String pinAddress;

  _PostJobForm2State(this.category, this.alamat, this.kota, this.date,
      this.jadwal, this.startTime, this.endTime, this.pickLat, this.pickLng);

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String pathFile = appDocDir.uri.resolve("temp.jpg").path;
    File tmpFile = File(pickedFile.path);
    File destFile = await tmpFile.copy(pathFile);
    tmpimage1 = destFile.path;
    // print("PATHTMP");
    // print(tmpimage1);
    pathimage = pickedFile.path;
    // print("PATHIMAGE NYA");
    // print(pathimage);

    setState(() {
      tmpimage1 = destFile.path;
//      _tmpImage = tmpFile;
      pathimage = pickedFile.path;
    });
  }

  //bawah ini pake mulit image picker
  Widget buildGridView() {
    return Align(
        alignment: Alignment.center,
        child: GridView.count(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          crossAxisCount: 3,
          mainAxisSpacing: 2,
          children: List.generate(images1.length, (index) {
            Asset asset = images1[index];
            // print("IAMGES1 LENGTH : " + images1.length.toString());
            // print("ASSET NAME : " + asset.name);
            filename1.add(asset.name);
            return AssetThumb(
              asset: asset,
              width: 300,
              height: 300,
            );
          }),
        ));
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images1,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images1 = resultList;
    });
  }

  final _formKey = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();

  FocusNode nodeJudul;
  FocusNode nodeDeskripsi;
  FocusNode nodeGaji;
  bool tap = false;

  @override
  void initState() {
    super.initState();
    persyaratanControllers.add(PersyaratanControllers(TextEditingController()));
    fasilitasControllers.add(FasilitasControllers(TextEditingController()));
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    nodeJudul = FocusNode();
    nodeDeskripsi = FocusNode();
    nodeGaji = FocusNode();
  }

  @override
  void dispose() {
    nodeJudul.dispose();
    nodeDeskripsi.dispose();
    nodeGaji.dispose();
    super.dispose();
  }

  uploadImages() async {
    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/add_images.php");
    // create multipart request
    MultipartRequest request = http.MultipartRequest("POST", url);

    for (Asset assets in images1) {
      ByteData byteData = await assets.getByteData();
      List<int> imageData = byteData.buffer.asUint8List();
      MultipartFile multipartFile = new MultipartFile.fromBytes(
        'photo',
        imageData,
        filename: assets.name,
        contentType: MediaType("image", "jpg"),
      );

      // add file to multipart
      request.files.add(multipartFile);
      // send
      var response = await request.send();
      // print(response.statusCode);
      if (response.statusCode == 200) {
        message = "Uploaded";
        // print(message);
      } else {
        message = "Upload Failed";
        // print(message);
      }
      return message;
    }
  }

  Widget buildTextField(PersyaratanControllers controllers) {
    return TextField(
      controller: controllers.persyaratan,
      decoration: InputDecoration(
        labelText: 'Persyaratan Pekerja Sampingan',
        contentPadding: EdgeInsets.symmetric(vertical: 8.0),
        hintText: 'KTP, SIM C, Sertifikat, ...',
        hintStyle: TextStyle(
          color: Colors.black26,
        ),
      ),
      maxLines: 1,
      textCapitalization: TextCapitalization.sentences,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.text,
    );
  }

  Widget buildTextField2(FasilitasControllers controllers) {
    return TextField(
      controller: controllers.fasilitas,
      decoration: InputDecoration(
        labelText: 'Fasilitas Pekerja Sampingan',
        contentPadding: EdgeInsets.symmetric(vertical: 8.0),
        hintText: 'Makan Siang, Kursus Gratis, Sertifikat, ...',
        hintStyle: TextStyle(
          color: Colors.black26,
        ),
      ),
      maxLines: 1,
      textCapitalization: TextCapitalization.sentences,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.text,
    );
  }

  @override
  Widget build(BuildContext context) {
    print("PICKKKKK : ${pickLat.toString()} ${pickLng.toString()}");
    final PostJobVM postJobVM = Provider.of<PostJobVM>(context);
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Style.bgColor,
        child: Scaffold(
            key: _scafoldKey,
            appBar: getAppBar(context),
            backgroundColor: Colors.transparent,
            body: Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                        child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16, bottom: 16),
                            child: Text(
                              'Isikan Data Postingan Pekerjaan Sampingan Anda',
                              style: Style.appStyle16,
                            ),
                          ),
                          TextFormField(
                            controller: _judulController,
                            decoration: InputDecoration(
                                labelText: 'Judul Pekerjaan Sampingan',
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 8.0),
                                hintText: 'Memasak, bersih-bersih, dll',
                                hintStyle: TextStyle(
                                  color: Colors.black26,
                                )),
                            maxLength: 50,
                            maxLines: 1,
                            textCapitalization: TextCapitalization.words,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.multiline,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Harap isi Judul Pekerjaan Sampingan Anda';
                              }
                              return null;
                            },
                            focusNode: nodeJudul,
                          ),
                          TextFormField(
                            controller: _deskripsiController,
                            decoration: InputDecoration(
                                labelText: 'Deskripsi Pekerjaan Sampingan',
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 8.0),
                                hintText: 'Saya membutuhkan orang untuk ...',
                                hintStyle: TextStyle(
                                  color: Colors.black26,
                                )),
                            maxLength: 100,
                            maxLines: 3,
                            textCapitalization: TextCapitalization.none,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.multiline,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Harap isi Deskripsi Pekerjaan Sampingan Anda';
                              }
                              return null;
                            },
                            focusNode: nodeDeskripsi,
                          ),
                          tap == true
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    IconButton(
                                        icon: Icon(Icons.check),
                                        color: Style.primaryColor,
                                        onPressed: () {
                                          nodeDeskripsi.unfocus();
                                          tap = false;
                                          setState(() {
                                            nodeDeskripsi.unfocus();
                                            tap = false;
                                          });
                                        }),
                                    IconButton(
                                        icon: Icon(Icons.clear),
                                        color: Colors.red,
                                        onPressed: () {
                                          _deskripsiController.clear();
                                          nodeDeskripsi.unfocus();
                                          tap = false;
                                          setState(() {
                                            _deskripsiController.clear();
                                            nodeDeskripsi.unfocus();
                                            tap = false;
                                          });
                                        }),
                                  ],
                                )
                              : SizedBox(height: 0),
                          TextFormField(
                            controller: _gajiController,
                            decoration: InputDecoration(
                              labelText: 'Gaji Pekerjaan Sampingan',
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 8.0),
                              hintText: 'Rp. 20.000, 10.000, 5.000 ...',
                              hintStyle: TextStyle(
                                color: Colors.black26,
                              ),
                            ),
                            maxLength: 24,
                            maxLines: 1,
                            textCapitalization: TextCapitalization.none,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Harap isi Jumlah Gaji';
                              }
                              return null;
                            },
                            focusNode: nodeGaji,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          ...persyaratanControllers.map(
                              (persyaratanController) =>
                                  buildTextField(persyaratanController)),
                          SizedBox(height: 16),
                          Container(
                            width: double.infinity,
                            child: ElevatedButton.icon(
                                onPressed: () {
                                  setState(() {
                                    allPersyaratan =
                                        '${allPersyaratan ?? ''}${persyaratanControllers[persyaratanControllers.length - 1].persyaratan.text};';
                                    persyaratanControllers.add(
                                        PersyaratanControllers(
                                            TextEditingController()));
                                  });
                                  print("ALL PERSYARATAN : $allPersyaratan");
                                },
                                icon: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                ),
                                label: Text('Tambah Field Persyaratan'),
                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(10),
                                  primary: Style.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                )),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          ...fasilitasControllers.map((fasilitasController) =>
                              buildTextField2(fasilitasController)),
                          SizedBox(height: 16),
                          Container(
                            width: double.infinity,
                            child: ElevatedButton.icon(
                                onPressed: () {
                                  setState(() {
                                    allFasilitas =
                                        '${allFasilitas ?? ''}${fasilitasControllers[fasilitasControllers.length - 1].fasilitas.text};';
                                    fasilitasControllers.add(
                                        FasilitasControllers(
                                            TextEditingController()));
                                  });
                                  print("ALL FASILITAS : $allFasilitas");
                                },
                                icon: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                ),
                                label: Text('Tambah Field Fasilitas'),
                                style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(10),
                                  primary: Style.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                )),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text('Pin Lokasi Pekerjaan Sampingan'),
                          SizedBox(
                            height: 8,
                          ),
                          Text(pinAddress == null ||
                                  pickLat == null ||
                                  pickLng == null
                              ? 'Anda Belum Menentukan Pin Lokasi Pekerjaan Sampingan'
                              : '$pinAddress\n(${pickLat.toString()}, ${pickLng.toString()})'),
                          SizedBox(height: 16),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: double.infinity,
                                    child: Material(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      elevation: 8,
                                      color: Style.primaryColor,
                                      clipBehavior: Clip.antiAlias,
                                      child: MaterialButton(
                                        color: Style.primaryColor,
                                        child: Text(
                                          'Pilih Pin Lokasi Pekerjaan Sampingan',
                                          style: Style.appStyle16White,
                                        ),
                                        onPressed: () async {
                                          final result = await Navigator.push(
                                              context, MaterialPageRoute(
                                                  builder: (context) {
                                            return JobLocationPicker();
                                          }));
                                          pickLat = double.parse(result[0]);
                                          pickLng = double.parse(result[1]);
                                          pinAddress = result[2];
                                          print(
                                              "PICKKRESULTT : ${pickLat.toString()} ${pickLng.toString()} ${pinAddress.toString()}");
                                          setState(() {
                                            pickLat = double.parse(result[0]);
                                            pickLng = double.parse(result[1]);
                                            pinAddress = result[2];
                                            print(
                                                "PICKKRESULTT : ${pickLat.toString()} ${pickLng.toString()} ${pinAddress.toString()}");
                                          });
                                        },
                                      ),
                                    ),
                                  )),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 16, horizontal: 0),
                                // child: _image1 == null ? Text('Anda dapat upload referensi desain di bawah ini') : Image.file(_image1)
                                child: images1.isEmpty
                                    ? Text(
                                        'Anda dapat upload / mengganti gambar pekerjaan sampingan di bawah ini')
                                    : buildGridView() /*()Image.file(_image)*/,
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  width: double.infinity,
                                  child: MaterialButton(
                                    height: 48,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    elevation: 8,
                                    color: Style.primaryColor,
                                    child: Text(
                                      images1.length == 0
                                          ? 'Upload Gambar'
                                          : 'Ganti Gambar',
                                      style: Style.appStyle16White,
                                    ),
                                    onPressed: loadAssets,
                                  ),
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.symmetric(vertical: 2.0)),
                              Align(
                                alignment: Alignment.center,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 0),
                                  child: Container(
                                    width: double.infinity,
                                    child: ElevatedButton.icon(
                                      onPressed: () async {
                                        if (_formKey.currentState.validate()) {
                                          if (pickLat == null) {
                                            return showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: new Text(
                                                      'Anda belum menentukan pin lokasi pekerjaan sampingan'),
                                                  actions: <Widget>[
                                                    ElevatedButton(
                                                      child: new Text("OK"),
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                        primary:
                                                            Style.primaryColor,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.0),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              },
                                            );
                                          } else {
                                            if (allPersyaratan != null ||
                                                allFasilitas != null) {
                                              setState(() {
                                                allPersyaratan =
                                                    '${allPersyaratan ?? ''}${persyaratanControllers[persyaratanControllers.length - 1].persyaratan.text};';
                                                allFasilitas =
                                                    '${allFasilitas ?? ''}${fasilitasControllers[fasilitasControllers.length - 1].fasilitas.text};';
                                              });
                                            }
                                            _formKey.currentState.save();
                                            print('is validate');
                                            final UserProvider userProvider =
                                                Provider.of<UserProvider>(
                                                    context,
                                                    listen: false);
                                            var formData = {
                                              "id": userProvider.user.id,
                                              "job_name": _judulController.text,
                                              "job_category": category,
                                              'job_date': date,
                                              "jadwal": jadwal,
                                              "hours_start": startTime,
                                              "hours_end": endTime,
                                              "job_description":
                                                  _deskripsiController.text,
                                              "fee": _gajiController.numberValue
                                                  .toString(),
                                              "persyaratan": allPersyaratan,
                                              "fasilitas": allFasilitas,
                                              "street": alamat,
                                              "city": kota,
                                              "pinlat": pickLat,
                                              "pinlng": pickLng
                                            };
                                            print("FORMDATA");
                                            print(formData);
                                            await postJobVM.showProgress(
                                                context,
                                                formData,
                                                images1,
                                                filename1);
                                          }
                                        } else
                                          print('is not validate');
                                      },
                                      icon: Icon(FontAwesomeIcons.check),
                                      label: Text('Selesai',
                                          style: Style.appStyle16White),
                                      style: ElevatedButton.styleFrom(
                                        primary: Colors.lightGreen,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8)),
                                        padding: EdgeInsets.all(10),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 32.0),
                                child: Container(
                                  width: double.infinity,
                                  child: MaterialButton(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    color: Style.secondaryColor,
                                    child: Text(
                                      'Batal Mengisi Form',
                                      style: Style.appStyle16White,
                                    ),
                                    onPressed: () =>
                                        Navigator.pushReplacementNamed(
                                            context, '/'),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ))))),
      ),
    );
  }

  Widget getAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Stack(
        children: <Widget>[
          AppBar(
            brightness: Brightness.dark,
            centerTitle: true,
            automaticallyImplyLeading: true,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            elevation: 0.0,
            backgroundColor: Style.primaryColor,
            title: Text(
              'Form Posting Job (2/2)',
            ),
          ),
        ],
      ),
    );
  }
}
