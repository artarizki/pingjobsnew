import 'dart:async';

import 'package:csc_picker/csc_picker.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/post_job_form2_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PostJobForm extends StatefulWidget {
  const PostJobForm({Key key}) : super(key: key);

  @override
  _PostJobFormState createState() => _PostJobFormState();
}

class _PostJobFormState extends State<PostJobForm>
    with TickerProviderStateMixin {
  final _alamatController = TextEditingController();
  final _kotaController = TextEditingController();
  var _dateController = TextEditingController();
  var _jadwalController = TextEditingController();
  var _startTimeController = TextEditingController();
  var _endTimeController = TextEditingController();
  String _valueChangedDate = '';
  String _valueChangedStartTime = '';
  String _valueChangedEndTime = '';
  String _valueDateSaved = '';
  String countryValue = 'Indonesia';
  String stateValue = 'Jakarta';
  String cityValue = 'Jakarta';

  var _jenisJadwal = [
    "Hari-hari/Hari, hari, ...",
    "Harian",
    "Bulanan",
    "Mingguan",
    "Bulanan"
  ];

  var _currencies = [
    "Administrasi",
    "Acara",
    "Bayi, Balita, & Anak-Anak",
    "Belanja",
    "Berita",
    "Edukasi & Pelatihan",
    "Entri Data",
    "Desain Grafis",
    "Desain Web",
    "Fashion & Kecantikan",
    "Finansial",
    "Hewan",
    "Hiburan & Media",
    "Internet",
    "Jasa Pindahan",
    "Jasa Pemasangan",
    "Jasa Tukang",
    "Keamanan",
    "Kesehatan",
    "Komputer & Teknologi Informasi",
    "Kuliner",
    "Lingkungan",
    "Olahraga",
    "Pelayanan Masyarakat",
    "Rumah Tangga",
    "Sains",
    "Sales",
    "Servis/Reparasi",
    "Seni",
    "Sosial",
    "Survey/Penelitian",
    "Transportasi",
    "Lainnya",
  ];

  var _city = [
    "Aceh Barat",
    "Aceh Barat Daya",
    "Aceh Besar",
    "Aceh Jaya",
    "Aceh Selatan",
    "Aceh Singkil",
    "Aceh Tamiang",
    "Aceh Tengah",
    "Aceh Tenggara",
    "Aceh Timur",
    "Aceh Utara",
    "Agam",
    "Alor",
    "Ambon",
    "Asahan",
    "Asmat",
    "Batam",
    "Badung",
    "Balangan",
    "Balikpapan",
    "Banda Aceh",
    "Bandar Lampung",
    "Bandung",
    "Bandung Barat",
    "Banggai",
    "Banggai Kepulauan",
    "Bangka",
    "Bangka Barat",
    "Bangka Selatan",
    "Bangka Tengah",
    "Bangkalan",
    "Bangli",
    "Banjar",
    "Banjar Baru",
    "Banjarmasin",
    "Banjarnegara",
    "Bantaeng",
    "Bantul",
    "Banyu Asin",
    "Banyumas",
    "Banyuwangi",
    "Barito Kuala",
    "Barito Selatan",
    "Barito Timur",
    "Barito Utara",
    "Barru",
    "Baru",
    "Batang",
    "Batang Hari",
    "Batu",
    "Batu Bara",
    "Baubau",
    "Bekasi",
    "Belitung",
    "Belitung Timur",
    "Belu",
    "Bener Meriah",
    "Bengkalis",
    "Bengkayang",
    "Bengkulu",
    "Bengkulu Selatan",
    "Bengkulu Tengah",
    "Bengkulu Utara",
    "Berau",
    "Biak Numfor",
    "Bima",
    "Binjai",
    "Bintan",
    "Bireuen",
    "Bitung",
    "Blitar",
    "Blora",
    "Boalemo",
    "Bogor",
    "Bojonegoro",
    "Bolaang Mongondow",
    "Bolaang Mongondow Selatan",
    "Bolaang Mongondow Timur",
    "Bolaang Mongondow Utara",
    "Bombana",
    "Bondowoso",
    "Bone",
    "Bone Bolango",
    "Bontang",
    "Boven Digoel",
    "Boyolali",
    "Brebes",
    "Bukittinggi",
    "Buleleng",
    "Bulukumba",
    "Bulungan",
    "Bungo",
    "Buol",
    "Buru",
    "Buru Selatan",
    "Buton",
    "Buton Utara",
    "Ciamis",
    "Cianjur",
    "Cilacap",
    "Cilegon",
    "Cimahi",
    "Cirebon",
    "Dumai",
    "Dairi",
    "Deiyai",
    "Deli Serdang",
    "Demak",
    "Denpasar",
    "Depok",
    "Dharmasraya",
    "Dogiyai",
    "Dompu",
    "Donggala",
    "Empat Lawang",
    "Ende",
    "Enrekang",
    "Fakfak",
    "Flores Timur",
    "Garut",
    "Gayo Lues",
    "Gianyar",
    "Gorontalo",
    "Gorontalo Utara",
    "Gowa",
    "Gresik",
    "Grobogan",
    "Gunung Kidul",
    "Gunung Mas",
    "Gunungsitoli",
    "Halmahera Barat",
    "Halmahera Selatan",
    "Halmahera Tengah",
    "Halmahera Timur",
    "Halmahera Utara",
    "Hulu Sungai Selatan",
    "Hulu Sungai Tengah",
    "Hulu Sungai Utara",
    "Humbang Hasundutan",
    "Indragiri Hilir",
    "Indragiri Hulu",
    "Indramayu",
    "Intan Jaya",
    "Jakarta Barat",
    "Jakarta Pusat",
    "Jakarta Selatan",
    "Jakarta Timur",
    "Jakarta Utara",
    "Jambi",
    "Jayapura",
    "Jayawijaya",
    "Jember",
    "Jembrana",
    "Jeneponto",
    "Jepara",
    "Jombang",
    "Kaimana",
    "Kampar",
    "Kapuas",
    "Kapuas Hulu",
    "Karang Asem",
    "Karanganyar",
    "Karawang",
    "Karimun",
    "Karo",
    "Katingan",
    "Kaur",
    "Kayong Utara",
    "Kebumen",
    "Kediri",
    "Keerom",
    "Kendal",
    "Kendari",
    "Kepahiang",
    "Kepulauan Anambas",
    "Kepulauan Aru",
    "Kepulauan Mentawai",
    "Kepulauan Meranti",
    "Kepulauan Sangihe",
    "Kepulauan Selayar",
    "Kepulauan Seribu",
    "Kepulauan Sula",
    "Kepulauan Talaud",
    "Kepulauan Yapen",
    "Kerinci",
    "Ketapang",
    "Klaten",
    "Klungkung",
    "Kolaka",
    "Kolaka Utara",
    "Konawe",
    "Konawe Selatan",
    "Konawe Utara",
    "Kotamobagu",
    "Kotawaringin Barat",
    "Kotawaringin Timur",
    "Kuantan Singingi",
    "Kubu Raya",
    "Kudus",
    "Kulon Progo",
    "Kuningan",
    "Kupang",
    "Kutai Barat",
    "Kutai Kartanegara",
    "Kutai Timur",
    "Labuhan Batu",
    "Labuhan Batu Selatan",
    "Labuhan Batu Utara",
    "Lahat",
    "Lamandau",
    "Lamongan",
    "Lampung Barat",
    "Lampung Selatan",
    "Lampung Tengah",
    "Lampung Timur",
    "Lampung Utara",
    "Landak",
    "Langkat",
    "Langsa",
    "Lanny Jaya",
    "Lebak",
    "Lebong",
    "Lembata",
    "Lhokseumawe",
    "Lima Puluh Kota",
    "Lingga",
    "Lombok Barat",
    "Lombok Tengah",
    "Lombok Timur",
    "Lombok Utara",
    "Lubuklinggau",
    "Lumajang",
    "Luwu",
    "Luwu Timur",
    "Luwu Utara",
    "Madiun",
    "Magelang",
    "Magetan",
    "Majalengka",
    "Majene",
    "Makassar",
    "Malang",
    "Malinau",
    "Maluku Barat Daya",
    "Maluku Tengah",
    "Maluku Tenggara",
    "Maluku Tenggara Barat",
    "Mamasa",
    "Mamberamo Raya",
    "Mamberamo Tengah",
    "Mamuju",
    "Mamuju Utara",
    "Manado",
    "Mandailing Natal",
    "Manggarai",
    "Manggarai Barat",
    "Manggarai Timur",
    "Manokwari",
    "Mappi",
    "Maros",
    "Mataram",
    "Maybrat",
    "Medan",
    "Melawi",
    "Merangin",
    "Merauke",
    "Mesuji",
    "Metro",
    "Mimika",
    "Minahasa",
    "Minahasa Selatan",
    "Minahasa Tenggara",
    "Minahasa Utara",
    "Mojokerto",
    "Morowali",
    "Muara Enim",
    "Muaro Jambi",
    "Mukomuko",
    "Muna",
    "Murung Raya",
    "Musi Banyuasin",
    "Musi Rawas",
    "Nabire",
    "Nagan Raya",
    "Nagekeo",
    "Natuna",
    "Nduga",
    "Ngada",
    "Nganjuk",
    "Ngawi",
    "Nias",
    "Nias Barat",
    "Nias Selatan",
    "Nias Utara",
    "Nunukan",
    "Ogan Ilir",
    "Ogan Komering Ilir",
    "Ogan Komering Ulu",
    "Ogan Komering Ulu Selatan",
    "Ogan Komering Ulu Timur",
    "Pacitan",
    "Padang",
    "Padang Lawas",
    "Padang Lawas Utara",
    "Padang Panjang",
    "Padang Pariaman",
    "Padangsidimpuan",
    "Pagar Alam",
    "Pakpak Bharat",
    "Palangka Raya",
    "Palembang",
    "Palopo",
    "Palu",
    "Pamekasan",
    "Pandeglang",
    "Pangandaran",
    "Pangkajene Dan Kepulauan",
    "Pangkal Pinang",
    "Paniai",
    "Parepare",
    "Pariaman",
    "Parigi Moutong",
    "Pasaman",
    "Pasaman Barat",
    "Paser",
    "Pasuruan",
    "Pati",
    "Payakumbuh",
    "Pegunungan Bintang",
    "Pekalongan",
    "Pekanbaru",
    "Pelalawan",
    "Pemalang",
    "Pematang Siantar",
    "Penajam Paser Utara",
    "Pesawaran",
    "Pesisir Barat",
    "Pesisir Selatan",
    "Pidie",
    "Pidie Jaya",
    "Pinrang",
    "Pohuwato",
    "Polewali Mandar",
    "Ponorogo",
    "Pontianak",
    "Poso",
    "Prabumulih",
    "Pringsewu",
    "Probolinggo",
    "Pulang Pisau",
    "Pulau Morotai",
    "Puncak",
    "Puncak Jaya",
    "Purbalingga",
    "Purwakarta",
    "Purworejo",
    "Raja Ampat",
    "Rejang Lebong",
    "Rembang",
    "Rokan Hilir",
    "Rokan Hulu",
    "Rote Ndao",
    "Siak",
    "Sabang",
    "Sabu Raijua",
    "Salatiga",
    "Samarinda",
    "Sambas",
    "Samosir",
    "Sampang",
    "Sanggau",
    "Sarmi",
    "Sarolangun",
    "Sawah Lunto",
    "Sekadau",
    "Seluma",
    "Semarang",
    "Seram Bagian Barat",
    "Seram Bagian Timur",
    "Serang",
    "Serdang Bedagai",
    "Seruyan",
    "Siau Tagulandang Biaro",
    "Sibolga",
    "Sidenreng Rappang",
    "Sidoarjo",
    "Sigi",
    "Sijunjung",
    "Sikka",
    "Simalungun",
    "Simeulue",
    "Singkawang",
    "Sinjai",
    "Sintang",
    "Situbondo",
    "Sleman",
    "Solok",
    "Solok Selatan",
    "Soppeng",
    "Sorong",
    "Sorong Selatan",
    "Sragen",
    "Subang",
    "Subulussalam",
    "Sukabumi",
    "Sukamara",
    "Sukoharjo",
    "Sumba Barat",
    "Sumba Barat Daya",
    "Sumba Tengah",
    "Sumba Timur",
    "Sumbawa",
    "Sumbawa Barat",
    "Sumedang",
    "Sumenep",
    "Sungai Penuh",
    "Supiori",
    "Surabaya",
    "Surakarta",
    "Tabalong",
    "Tabanan",
    "Takalar",
    "Tambrauw",
    "Tana Tidung",
    "Tana Toraja",
    "Tanah Bumbu",
    "Tanah Datar",
    "Tanah Laut",
    "Tangerang",
    "Tangerang Selatan",
    "Tanggamus",
    "Tanjung Balai",
    "Tanjung Jabung Barat",
    "Tanjung Jabung Timur",
    "Tanjung Pinang",
    "Tapanuli Selatan",
    "Tapanuli Tengah",
    "Tapanuli Utara",
    "Tapin",
    "Tarakan",
    "Tasikmalaya",
    "Tebing Tinggi",
    "Tebo",
    "Tegal",
    "Teluk Bintuni",
    "Teluk Wondama",
    "Temanggung",
    "Ternate",
    "Tidore Kepulauan",
    "Timor Tengah Selatan",
    "Timor Tengah Utara",
    "Toba Samosir",
    "Tojo Una-una",
    "Toli-toli",
    "Tolikara",
    "Tomohon",
    "Toraja Utara",
    "Trenggalek",
    "Tual",
    "Tuban",
    "Tulang Bawang Barat",
    "Tulangbawang",
    "Tulungagung",
    "Wajo",
    "Wakatobi",
    "Waropen",
    "Way Kanan",
    "Wonogiri",
    "Wonosobo",
    "Yahukimo",
    "Yalimo",
    "Yogyakarta"
  ];

  var tmpimage1;
  String _currentSelectedValue;
  String _jenisJadwalValue;
  final picker = ImagePicker();
  List<Asset> images1 = <Asset>[];
  var pathimage;

  _PostJobFormState();

  //bawah ini pake mulit image picker
  Widget buildGridView() {
    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      crossAxisCount: 3,
      children: List.generate(images1.length, (index) {
        Asset asset = images1[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images1,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images1 = resultList;
    });
  }

  final _formKey = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();

  FocusNode nodeAlamat;
  FocusNode nodeKota;
  FocusNode nodeDate;
  FocusNode nodeJadwal;
  String token;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    super.initState();
    getPref();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light));
    nodeAlamat = FocusNode();
    nodeKota = FocusNode();
    nodeDate = FocusNode();
    nodeJadwal = FocusNode();
    // inisilalisasi datetime sekarang ke string
    _dateController = TextEditingController(text: DateTime.now().toString());
  }

  @override
  void dispose() {
    nodeAlamat.dispose();
    nodeKota.dispose();
    nodeDate.dispose();
    nodeJadwal.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Style.bgColor,
        child: Scaffold(
          key: _scafoldKey,
          appBar: getAppBar(context),
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: token != null
                ? Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              child: Text(
                                'Isikan Data Postingan Pekerjaan Anda',
                                style: Style.appStyle16,
                              ),
                            ),
                            DropdownSearch<String>(
                              dropdownSearchDecoration: InputDecoration(
                                hintText:
                                    'Kuliner, Rumah Tangga, Transportasi, ...',
                                errorStyle: TextStyle(color: Colors.redAccent),
                                labelText:
                                    'Pilih Jenis Kategori Pekerjaan Sampingan',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              searchBoxDecoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              mode: Mode.BOTTOM_SHEET,
                              showSelectedItem: true,
                              showClearButton: true,
                              showSearchBox: true,
                              items: _currencies,
                              label: "Pilih Jenis Kategori Pekerjaan Sampingan",
                              hint: "Kuliner, Rumah Tangga, Transportasi, ...",
                              // popupItemDisabled: (String s) => s.startsWith('A'),
                              onChanged: (String value) {
                                _currentSelectedValue = value;
                                setState(() {
                                  _currentSelectedValue = value;
                                });
                              },
                              onSaved: (String value) {
                                _currentSelectedValue = value;
                                setState(() {
                                  _currentSelectedValue = value;
                                });
                              },
                              validator: (String item) {
                                if (item == null || item == "" || item == " ") {
                                  return 'Harap pilih Kategori Pekerjaan Sampingan Anda';
                                }
                                return null;
                              },
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 0),
                            ),
                            DropdownSearch<String>(
                              dropdownSearchDecoration: InputDecoration(
                                hintText: 'Aceh/Jakarta/Makassar/Jayapura, ...',
                                errorStyle: TextStyle(color: Colors.redAccent),
                                labelText: 'Pilih Kabupaten/Kota',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              searchBoxDecoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              mode: Mode.BOTTOM_SHEET,
                              showSelectedItem: true,
                              showClearButton: true,
                              showSearchBox: true,
                              items: _city,
                              label: "Pilih Kabupaten/Kota",
                              hint: "Aceh/Jakarta/Makassar/Jayapura, ...",
                              // popupItemDisabled: (String s) => s.startsWith('A'),
                              onChanged: (String value) {
                                cityValue = value;
                                setState(() {
                                  cityValue = value;
                                });
                              },
                              onSaved: (String value) {
                                cityValue = value;
                                setState(() {
                                  cityValue = value;
                                });
                              },
                              validator: (String item) {
                                if (item == null || item == "" || item == " ") {
                                  return 'Harap isi Kota Anda';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 8),
                            TextFormField(
                              controller: _alamatController,
                              decoration: InputDecoration(
                                  labelText: 'Alamat Pekerjaan Sampingan',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 8.0),
                                  hintText: 'Jl. Soekarno Hatta No.21 ...',
                                  hintStyle: TextStyle(
                                    color: Colors.black26,
                                  )),
                              maxLength: 50,
                              maxLines: 3,
                              textCapitalization: TextCapitalization.sentences,
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.multiline,
                              validator: (value) {
                                if (value?.isEmpty ?? true) {
                                  return 'Harap isi Alamat Anda';
                                }
                                return null;
                              },
                              focusNode: nodeAlamat,
                            ),
                            SizedBox(height: 8),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 0),
                              child: Text('Pilih Tanggal Pekerjaan Sampingan'),
                            ),
                            DateTimePicker(
                              type: DateTimePickerType.date,
                              dateMask: 'yyyy/MM/dd',
                              controller: _dateController,
                              fieldHintText:
                                  'Masukkan Tanggal Pekerjaan Sampingan',
                              dateHintText:
                                  'Masukkan Tanggal Pekerjaan Sampingan',
                              fieldLabelText: 'Tanggal Pekerjaan Sampingan',
                              //initialValue: _initialValue,
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              icon: Icon(Icons.event),
                              dateLabelText: 'Date',
                              //locale: Locale('en', 'US'),
                              onChanged: (val) =>
                                  setState(() => _valueChangedDate = val),
                              validator: (val) {
                                if (val?.isEmpty ?? true) {
                                  return 'Harap isi Tanggal Pekerjaan Sampingan';
                                }
                                return null;
                              },
                              onSaved: (val) =>
                                  setState(() => _valueDateSaved = val),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 0),
                            ),
                            DropdownSearch<String>(
                              dropdownSearchDecoration: InputDecoration(
                                hintText:
                                "Hari - Hari, Mingguan, Bulanan, ...",
                                errorStyle: TextStyle(color: Colors.redAccent),
                                labelText:
                                "Pilih Jenis Jadwal Pekerjaan Sampingan",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              searchBoxDecoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              ),
                              mode: Mode.BOTTOM_SHEET,
                              showSelectedItem: true,
                              showClearButton: true,
                              showSearchBox: true,
                              items: _jenisJadwal,
                              label: "Pilih Jenis Jadwal Pekerjaan Sampingan",
                              hint: "Hari - Hari, Mingguan, Bulanan, ...",
                              // popupItemDisabled: (String s) => s.startsWith('A'),
                              onChanged: (String value) {
                                _jenisJadwalValue = value;
                                setState(() {
                                  _jenisJadwalValue = value;
                                });
                              },
                              onSaved: (String value) {
                                _jenisJadwalValue = value;
                                setState(() {
                                  _jenisJadwalValue = value;
                                });
                              },
                              validator: (String item) {
                                if (item == null || item == "" || item == " ") {
                                  return 'Harap pilih jenis Jadwal Pekerjaan Sampingan Anda';
                                }
                                return null;
                              },
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                            ),
                            _jenisJadwalValue == 'Hari-hari/Hari, hari, ...' ? TextFormField(
                              controller: _jadwalController,
                              decoration: InputDecoration(
                                  labelText: 'Pilih AJadwal (Senin-Kamis/Senin Kamis Sabtu) ...',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 8.0),
                                  hintText:
                                      'Senin-Kamis, Senin Selasa Kamis',
                                  hintStyle: TextStyle(
                                    color: Colors.black26,
                                  )),
                              maxLength: 50,
                              maxLines: 1,
                              textCapitalization: TextCapitalization.words,
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.text,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Harap isi Jadwal Pekerjaan Sampingan Anda';
                                }
                                return null;
                              },
                              focusNode: nodeJadwal,
                            ) : SizedBox(),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 0),
                              child: Text(
                                  'Pilih Jam Mulai & Berakhir Pekerjaan Sampingan'),
                            ),
                            DateTimePicker(
                              type: DateTimePickerType.time,
                              controller: _startTimeController,
                              textInputAction: TextInputAction.done,
                              // initialValue: '00:00',
                              icon: Icon(Icons.access_time_rounded),
                              dateLabelText: 'Masukkan Jam Mulai',
                              fieldLabelText: 'Jam Mulai',
                              timeHintText: 'Masukkan Jam Mulai',
                              use24HourFormat: true,
                              // locale: Locale('id', 'ID'),
                              onChanged: (val) =>
                                  setState(() => _valueChangedStartTime = val),
                              validator: (val) {
                                if (val?.isEmpty ?? true) {
                                  return 'Harap isi Jam Mulai Pekerjaan Sampingan';
                                }
                                return null;
                              },
                              onSaved: (val) =>
                                  setState(() => _valueChangedStartTime = val),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 0),
                            ),
                            DateTimePicker(
                              type: DateTimePickerType.time,
                              controller: _endTimeController,
                              textInputAction: TextInputAction.done,
                              // initialValue: '00:00',
                              icon: Icon(Icons.access_time_rounded),
                              dateLabelText: 'Masukkan Jam Berakhir',
                              fieldLabelText: 'Jam Berakhir',
                              timeHintText: 'Masukkan Jam Berakhir',
                              use24HourFormat: true,
                              // locale: Locale('id', 'ID'),
                              onChanged: (val) =>
                                  setState(() => _valueChangedEndTime = val),
                              validator: (val) {
                                if (val?.isEmpty ?? true) {
                                  return 'Harap isi Jam Berakhir Pekerjaan Sampingan';
                                }
                                return null;
                              },
                              onSaved: (val) =>
                                  setState(() => _valueChangedEndTime = val),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(top: 32.0),
                                child: Container(
                                  width: double.infinity,
                                  child: MaterialButton(
                                    height: 48,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    color: Style.primaryColor,
                                    child: Text(
                                      'Selanjutnya',
                                      style: Style.appStyle16White,
                                    ),
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
                                        print('is validate');
                                        Navigator.push(context,
                                            MaterialPageRoute(builder: (_) {
                                          return PostJobForm2(
                                            category: _currentSelectedValue,
                                            alamat: _alamatController.text,
                                            kota: cityValue,
                                            date: _valueChangedDate,
                                            jadwal: _jenisJadwalValue == 'Hari-hari/Hari, hari, ...' ? _jadwalController.text : _jenisJadwalValue,
                                            startTime: _valueChangedStartTime,
                                            endTime: _valueChangedEndTime,
                                          );
                                        }));
                                      } else
                                        print('is not validate');
                                    },
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  )
                : Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 250,
                          height: 250,
                          child: Image.asset(
                              'assets/images/postjobloginerror.png'),
                        ),
                        Container(
                          width: 200,
                          child: ElevatedButton.icon(
                            onPressed: () => Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Login(),
                              ),
                            ),
                            icon: Icon(
                              Icons.login_outlined,
                              color: Colors.white,
                            ),
                            label: Text('Login'),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(10),
                              primary: Style.primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  Widget getAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50.0),
      child: Stack(
        children: <Widget>[
          AppBar(
            brightness: Brightness.dark,
            centerTitle: true,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Style.primaryColor,
            title: Text(
              'Form Posting Job (1/2)',
            ),
          ),
        ],
      ),
    );
  }
}
