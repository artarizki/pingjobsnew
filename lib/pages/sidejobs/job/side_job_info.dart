import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:optimized_cached_image/optimized_cached_image.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';

class SideJobInfo extends StatefulWidget {
  final ModelSideJob sidejobData;
  final LocationData currentLocation;

  const SideJobInfo({Key key, this.sidejobData, this.currentLocation})
      : super(key: key);

  @override
  _SideJobInfoState createState() =>
      _SideJobInfoState(sidejobData, currentLocation);
}

class _SideJobInfoState extends State<SideJobInfo> {
  ModelSideJob sidejobData;
  double distance;
  int finaldistance;
  int kmdistance;
  LocationData currentLocation;

  _SideJobInfoState(this.sidejobData, this.currentLocation);

  Future getDistance() async {
    distance = Geolocator.distanceBetween(
      currentLocation.latitude,
      currentLocation.longitude,
      double.parse(sidejobData.xJPin),
      double.parse(sidejobData.yJPin),
    );
    finaldistance = distance.ceil();
    if(finaldistance >= 1000)
      kmdistance = (finaldistance/1000).ceil();
    setState(() {
      finaldistance = distance.ceil();
      if(finaldistance >= 1000)
        kmdistance = (finaldistance/1000).ceil();
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    getDistance();
  }

  @override
  Widget build(BuildContext context) {
    String media = 'assets/images/untitled.jpg';
    String mediaUrl =
        'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';
    return Column(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 2,
          child: OptimizedCacheImage(
            imageUrl: mediaUrl + sidejobData.jobPhotovideo,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                Container(
                  height: 48,
                  child: LinearProgressIndicator(
                    value: downloadProgress.progress,
                  ),
                ),
            errorWidget: (context, url, error) => Image.asset(media),
            fit: BoxFit.cover,
          ),
        ),
        Container(
          color: Style.bgColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 16, right: 16, top: 8, bottom: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                widget.sidejobData.jobName,
                                textAlign: TextAlign.left,
                                style: Style.appStyleJobTitle,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 4),
                            Container(
                              child: Text(
                                  'Rp. ${toCurrencyString(widget.sidejobData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
                                  textAlign: TextAlign.end,
                                  style: Style.appStyle16600,
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.sidejobData.street.length > 30 ? '${widget.sidejobData.street.substring(0,30)}' : '${widget.sidejobData.street}',
                              style: Style.appStyle14g09,
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Icon(
                              FontAwesomeIcons.mapMarkerAlt,
                              size: 14,
                              color: Style.secondaryColor,
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            finaldistance == null
                                ? SizedBox(
                                    height: 8,
                                    width: 8,
                                    child: SizedBox(),
                                  )
                                : finaldistance >= 1000
                                    ? Expanded(
                                        child: Text(
                                          '$kmdistance km',
                                          overflow: TextOverflow.ellipsis,
                                          style: Style.appStyle14g09,
                                        ),
                                      )
                                    : Expanded(
                                        child: Text(
                                          '$finaldistance m',
                                          overflow: TextOverflow.ellipsis,
                                          style: Style.appStyle14g09,
                                        ),
                                      ),
                            const SizedBox(
                              width: 4,
                            ),
                            Spacer(),
                            widget.sidejobData.idJobseeker == null
                                ? Text(
                                    'TERSEDIA',
                                    style: Style.appStyleDefaultBoldGreen,
                                  )
                                : Text(
                                    'TERAMBIL',
                                    style: Style.appStyleDefaultBoldRed,
                                  ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                '${widget.sidejobData.jobDescription}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: Style.appStyle14,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
