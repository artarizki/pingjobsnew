import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/pages/sidejobs/job/side_job_info.dart';
import 'package:pingjobs/pages/sidejobs/job/side_job_sendbutton.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/pages/sidejobs/detailjob/detailjob2_screen.dart';
import 'package:pingjobs/component/style.dart';

class SideJobListView extends StatefulWidget {
  const SideJobListView({
    Key key,
    this.sidejobData,
    this.animationController,
    this.animation,
    this.callback,
    this.currentLocation,
  }) : super(key: key);

  final VoidCallback callback;

  // deklarasi data dummy pekerjaan sampingan beserta animasi
  final ModelSideJob sidejobData;
  final AnimationController animationController;
  final Animation<dynamic> animation;
  final LocationData currentLocation;

  @override
  _SideJobListViewState createState() => _SideJobListViewState(
      callback, sidejobData, animationController, animation, currentLocation);
}

class _SideJobListViewState extends State<SideJobListView> {
  VoidCallback callback;

  // deklarasi data dummy pekerjaan sampingan beserta animasi
  int index;
  ModelSideJob sidejobData;
  AnimationController animationController;
  Animation<dynamic> animation;
  LocationData currentLocation;
  double distance;
  int finaldistance;
  int idJobProvider;
  String responseMessage;

  // GetIdJobProviderProvider getIdJobProviderVM;

  @override
  void initState() {
    super.initState();
    index = null;
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    WidgetsBinding.instance.addPostFrameCallback((_) {
      countDistance();
    });

  }

  getDistance() {
    distance = Geolocator.distanceBetween(
      currentLocation?.latitude ?? 0,
      currentLocation?.longitude ?? 0,
      double.parse(sidejobData.xJPin),
      double.parse(sidejobData.yJPin),
    );
    finaldistance = distance.ceil();
    setState(() {
      finaldistance = distance.ceil();
    });
  }

  Future<int> checkId() async {
    var provider;
    provider = await Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    provider.asMap().forEach((indexData, item) {
      if (sidejobData.idJobprovider == item.user.id.toString()) {
        index = indexData;
        setState(() {
          index = indexData;
        });
        print("INDEXSAME = $index");
      }
    });
    print("INDEKS SETELAH DICOCOKKAN : $index");
    return index;
  }

  countDistance() async {
    return Timer.periodic(Duration(seconds: 1), getDistance());
  }

  _SideJobListViewState(this.callback, this.sidejobData,
      this.animationController, this.animation, this.currentLocation);

  @override
  Widget build(BuildContext context) {
    countDistance();
    String media = 'assets/images/untitled.jpg';
    String mediaUrl =
        'https://bertigagroup.com/pj.com/public/storage/images/sidejobs/';

    getC(BuildContext context) {
      var provider = Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
      return provider;
    }

    var provider = Provider.of<ConversationProvider>(context, listen: true);

    Future selectList() async {
      callback();
      print("KE CHAT");
      await checkId();
      var providerGet;
      print("INDEX : " + index.toString());
      print("ID JOB : " + sidejobData.idJob + sidejobData.jobName);
      String message = "Halo, Saya ingin bertanya tentang pekerjaan ini";
      if (index == null) {
        print("MASUK INDEKS NULL");
        await provider.newMessage(message, sidejobData.idJobprovider);
        providerGet = await getC(context);
        providerGet.asMap().forEach((indexData, item) async {
          print(
            "INDEKS USER ID CONVERSATION2 : " + item.user.id.toString(),
          );
          if (sidejobData.idJobprovider == item.user.id.toString()) {
            index = indexData;
            setState(() {
              index = indexData;
            });
            print("INDEXSAME = $index");
          }
        });
        final result = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailJob(
              sidejobData: sidejobData,
              index: index,
            ),
          ),
        );
        index = result[0];
        setState(() {
          index = result[0];
        });
        print(
          "PANJANG IDJOBPROV: " + providerGet.length.toString(),
        );
        print(
          "INDEXSETELAH ISI : " + index.toString(),
        );
      } else {
        print("GAK MASUK INDEKS NULL");
        print(
          "INDEKS CONVERSATION KE :" + index.toString(),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailJob(
              sidejobData: sidejobData,
              index: index,
            ),
          ),
        );
      };
    }

    return ChangeNotifierProvider<ConversationProvider>.value(
      value: ConversationProvider(),
      child: AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 50 * (1.0 - animation.value), 0.0),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, top: 8, bottom: 16),
                child: InkWell(
                  splashColor: Colors.transparent,
                  onTap: selectList,
                  child: sidejobData == null
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(8.0),
                            ),
                            boxShadow: <BoxShadow>[Style.shadowElement],
                          ),
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(8.0),
                            ),
                            child: Stack(
                              children: <Widget>[
                                SideJobInfo(sidejobData: sidejobData, currentLocation: currentLocation,),
                                SideJobSendButton(sidejobData: sidejobData,),
                              ],
                            ),
                          ),
                        ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
