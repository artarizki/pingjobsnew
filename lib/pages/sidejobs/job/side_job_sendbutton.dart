import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SideJobSendButton extends StatefulWidget {
  final ModelSideJob sidejobData;
  final int index;

  const SideJobSendButton({Key key, this.sidejobData, this.index}) : super(key: key);

  @override
  _SideJobSendButtonState createState() => _SideJobSendButtonState(sidejobData, index);
}

class _SideJobSendButtonState extends State<SideJobSendButton> {
  ModelSideJob sidejobData;
  LocationData currentLocation;
  int index;
  String token;
  _SideJobSendButtonState(this.sidejobData, this.index);

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString('access_token');
    setState(() {
      token = preferences.getString('access_token');
      print ("ADA TOKEN : ${preferences.getString('access_token')}");
    });
    return (preferences.getString('access_token'));
  }

  @override
  void initState() {
    super.initState();
    getPref();
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
  }

  Future<int> checkId() async {
    var provider;
    provider = await Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    provider.asMap().forEach((indexData, item) {
      if (sidejobData.idJobprovider == item.user.id.toString()) {
        index = indexData;
        setState(() {
          index = indexData;
        });
        print("INDEXSAME = $index");
      }
    });
    print("INDEKS SETELAH DICOCOKKAN : $index");
    return index;
  }

  loginFirst() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Login untuk mengirim pesan ?'),
            actions: <Widget>[
              ElevatedButton(
                child: Text(
                  "Login",
                  style: Style.appStylewhiteDefaultBold,
                ),
                onPressed: () => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Style.primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              ElevatedButton(
                child: Text(
                  "Tidak",
                  style: Style.appStyleDefaultBoldRed,
                ),
                onPressed: () => Navigator.pop(context),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side:
                    BorderSide(color: Style.secondaryColor),
                  ),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ConversationProvider>(context, listen: true);

    getC(BuildContext context) {
      var provider = Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
      return provider;
    }

    Future sendMessage() async {
      print("KE CHAT");
      await checkId();
      var providerGet;
      print(
        "INDEX : " + index.toString(),
      );
      print("ID JOB : " + sidejobData.idJob + sidejobData.jobName);
      String message = "Halo, Saya ingin bertanya tentang pekerjaan ini";
      if (index == null) {
        print("MASUK INDEKS NULL");
        await provider.newMessage(message, sidejobData.idJobprovider);
        providerGet = await getC(context);
        providerGet.asMap().forEach((indexData, item) async {
          print(
            "INDEKS USER ID CONVERSATION2 : " + item.user.id.toString(),
          );
          if (sidejobData.idJobprovider == item.user.id.toString()) {
            index = indexData;
            setState(() {
              index = indexData;
            });
            print("INDEXSAME = $index");
          }
        });
        final result = await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ChatScreen(
              conversation: providerGet[index],
            );
          }),
        );
        index = result[0];
        setState(() {
          index = result[0];
        });
        print(
          "PANJANG IDJOBPROV: " + providerGet.length.toString(),
        );
        print(
          "INDEXSETELAH ISI : " + index.toString(),
        );
      } else {
        print("GAK MASUK INDEKS NULL");
        print(
          "INDEKS CONVERSATION KE :" + index.toString(),
        );
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => ChatScreen(
              conversation: provider.concersations[index],
            ),
          ),
        );
      };
    }

    return Positioned(
      top: 8,
      right: 8,
      child: Container(
        decoration: BoxDecoration(
          color: Style.primaryColor,
          borderRadius: BorderRadius.all(
            Radius.circular(100),
          ),
          boxShadow: [Style.shadowElement],
        ),
        // color: Style.primaryColor,
        child: Provider.of<ConversationProvider>(
            context)
            .busy
            ? CircularProgressIndicator()
            : InkWell(
          borderRadius:
          const BorderRadius.all(
            Radius.circular(16.0),
          ),
          child: Padding(
            padding:
            const EdgeInsets.all(8.0),
            child: Icon(
              Icons.send_rounded,
              color: Colors.white,
            ),
          ),
          onTap: token != null ? sendMessage : loginFirst,
        ),
      ),
    );
  }
}
