import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/totaljobs_model.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';

class JobProviderLocationinfo extends StatefulWidget {
  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobs;
  final int distance2;
  final int index;
  final LocationData startLocation;

  const JobProviderLocationinfo({
    Key key,
    this.aktivitasData,
    this.totalJobs,
    this.distance2,
    this.index,
    this.startLocation,
  }) : super(key: key);

  @override
  _JobProviderLocationinfoState createState() => _JobProviderLocationinfoState(
      aktivitasData, totalJobs, distance2, index, startLocation);
}

class _JobProviderLocationinfoState extends State<JobProviderLocationinfo> {
  ModelAktivitas aktivitasData;
  ModelTotalJobs totalJobs;
  int distance2;
  int index;
  int kmdistance;
  var fotoProfil;

  Location location;
  List<Address> addresses;
  LocationData startLocation;
  var realAddress;

  _JobProviderLocationinfoState(
    this.aktivitasData,
    this.totalJobs,
    this.distance2,
    this.index,
    this.startLocation,
  );

  Future setJsLoc() async {
    await getAddress(startLocation.latitude, startLocation.longitude);
  }

  Future runSetLoc() async {
    await setJsLoc();
  }

  Future<String> getAddress(lat, long) async {
    try {
      final coordinates = new Coordinates(lat, long);
      addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      realAddress = addresses.first.addressLine;
      setState(() {
        realAddress = addresses.first.addressLine;
      });
      print("Alamat : $realAddress");
    } catch (e) {
      print("Error occured: $e");
      return null;
    }
  }

  @override
  void initState() {
    super.initState();
    if (distance2 >= 1000) kmdistance = (distance2 / 1000).ceil();
    setState(() {
      if (distance2 >= 1000) kmdistance = (distance2 / 1000).ceil();
    });
    location = new Location();
    runSetLoc();
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  @override
  Widget build(BuildContext context) {
    getAddress(startLocation?.latitude ?? 0, startLocation?.longitude ?? 0);
    print("ALAMAT : $realAddress");
    String mediaUrlUsers =
        'https://bertigagroup.com/pj.com/public/storage/images/users/';

    getC(BuildContext context) {
      var provider = Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
      return provider;
    }

    var provider = Provider.of<ConversationProvider>(context, listen: true);

    Future sendMessage() async {
      print("KE CHAT");
      await checkId();
      var providerGet;
      print(
        "INDEX : " + index.toString(),
      );
      print("ID JOB : " + aktivitasData.idJob + aktivitasData.jobName);
      String message = "Halo, Saya ingin bertanya tentang pekerjaan ini";
      if (index == null) {
        print("MASUK INDEKS NULL");
        await provider.newMessage(message, aktivitasData.idJobseeker);
        providerGet = await getC(context);
        providerGet.asMap().forEach((indexData, item) async {
          print(
            "INDEKS USER ID CONVERSATION2 : " + item.user.id.toString(),
          );
          if (aktivitasData.idJobseeker == item.user.id.toString()) {
            index = indexData;
            setState(() {
              index = indexData;
            });
            print("INDEXSAME = $index");
          }
        });
        final result = await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ChatScreen(
              conversation: providerGet[index],
            );
          }),
        );
        index = result[0];
        setState(() {
          index = result[0];
        });
        print(
          "PANJANG IDJOBPROV: " + providerGet.length.toString(),
        );
        print(
          "INDEXSETELAH ISI : " + index.toString(),
        );
      } else {
        print("GAK MASUK INDEKS NULL");
        print(
          "INDEKS CONVERSATION KE :" + index.toString(),
        );
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => ChatScreen(
              conversation: provider.concersations[index],
            ),
          ),
        );
      };
    }

    return DraggableScrollableSheet(
      initialChildSize: 0.3,
      minChildSize: 0.3,
      maxChildSize: 0.75,
      builder: (BuildContext context, scrollController) {
        return Container(
          color: Style.bgColor,
          margin: EdgeInsets.symmetric(horizontal: 0.0),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Center(
                    child: Container(
                      color: Colors.black54,
                      height: 3,
                      width: MediaQuery.of(context).size.width * 0.2,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        maxRadius: 40,
                        backgroundImage: fotoProfil == null
                            ? NetworkImage(
                                'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                            : NetworkImage(mediaUrlUsers + fotoProfil),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(aktivitasData.jobName,
                                  style: Style.appStyleJobTitle22900),
                              aktivitasData.idJobseeker == null
                                  ? Text(
                                      'Belum ada pelamar pekerjaan ini',
                                      style: Style.appStyle18,
                                    )
                                  : Text(totalJobs.name,
                                      style: Style.appStyle18),
                              Text(
                                aktivitasData?.idJobseeker == null ||
                                        totalJobs?.jobApplied == null
                                    ? '0 total pekerjaan terselesaikan'
                                    : '${totalJobs.jobApplied.toString()} total pekerjaan terselesaikan',
                                style: Style.appStyle16,
                              ),
                              aktivitasData.idJobseeker == null
                                  ? SizedBox()
                                  : distance2.compareTo(10) == 1
                                      ? Text('Pekerja Sedang Dalam Perjalanan',
                                          style: Style.appStyle20600blue)
                                      : Text('Pekerja Sampai di tujuan',
                                          style: Style.appStyle20600green)
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image(
                            image: AssetImage('assets/icons/money.png'),
                            width: 45,
                            height: 45,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Text('Gaji', style: Style.appStyle20Bold),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                            'Rp. ${toCurrencyString(aktivitasData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
                            style: Style.appStyle20Bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      distance2 == null
                          ? Text('Jarak 0 m dari Anda', style: Style.appStyle16)
                          : distance2 >= 1000
                              ? Text(
                                  'Jarak ${kmdistance.toString()} km dari Anda',
                                  style: Style.appStyle16)
                              : Text(
                                  'Jarak ${distance2.toString()} m dari Anda',
                                  style: Style.appStyle16),
                    ],
                  ),
                ),
                // Padding(
                //   padding: EdgeInsets.all(16),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.start,
                //     mainAxisSize: MainAxisSize.max,
                //     children: [
                //       ElevatedButton.icon(
                //         onPressed: () {
                //
                //         },
                //         icon: Icon(FontAwesomeIcons.check),
                //         label: Text('Konfirmasi Penyediaan Selesai'),
                //         style: ElevatedButton.styleFrom(
                //           primary: Colors.lightGreenAccent,
                //         ),
                //       )
                //     ],
                //   ),
                // ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Kategori Pekerjaan Sampingan',
                            style: Style.appStyle16,
                          ),
                          Text(
                            aktivitasData.jobCategory ?? 'Lainnya',
                            style: Style.appStyle16,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.location_pin, color: Colors.orange),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Lokasi Pekerja', style: Style.appStyle18),
                              Text(
                                  realAddress == null
                                      ? 'Lokasi pekerja tidak ditemukan'
                                      : realAddress,
                                  style: Style.appStyle22800),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.location_pin, color: Colors.green),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Lokasi Pekerjaan', style: Style.appStyle18),
                              Text(aktivitasData.street,
                                  style: Style.appStyle22800),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: aktivitasData.idJobseeker == null
                            ? ElevatedButton.icon(
                                onPressed: () async {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Belum ada pelamar pekerjaan ini'),
                                    ),
                                  );
                                },
                                icon: Icon(Icons.send_rounded),
                                label: Text('Kirim Pesan'),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.grey,
                                ),
                              )
                            : ElevatedButton.icon(
                                onPressed: sendMessage,
                                icon: Icon(Icons.send_rounded),
                                label: Text('Kirim Pesan'),
                                style: ElevatedButton.styleFrom(
                                  primary: Style.primaryColor,
                                ),
                              ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: ElevatedButton.icon(
                          onPressed: () async {
                            aktivitasData.idJobseeker == null
                                ? ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Belum ada pelamar pekerjaan ini'),
                                    ),
                                  )
                                : await FlutterPhoneDirectCaller.callNumber(
                                    totalJobs.mobilenumber);
                          },
                          icon: Icon(Icons.call),
                          label: Text('Panggil'),
                          style: ElevatedButton.styleFrom(
                            primary: aktivitasData.idJobseeker == null
                                ? Colors.grey
                                : Style.primaryColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // Padding(
                //   padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                //   child: SizedBox(
                //     child: Container(
                //       width: double.infinity,
                //       child: ElevatedButton.icon(
                //         onPressed: () {},
                //         icon: Icon(FontAwesomeIcons.check),
                //         label: Text('Konfirmasi Penyediaan Selesai'),
                //         style: ElevatedButton.styleFrom(
                //           primary: Colors.lightGreen,
                //         ),
                //       ),
                //     ),
                //   ),
                // )
              ],
            ),
          ),
        );
      },
    );
  }

  Future<int> checkId() async {
    var provider;
    provider = await Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    provider.asMap().forEach((indexData, item) {
      if (aktivitasData.idJobseeker == item.user.id.toString()) {
        index = indexData;
        setState(() {
          index = indexData;
        });
        print("INDEXSAME = $index");
      }
    });
    print("INDEKS SETELAH DICOCOKKAN : $index");
    return index;
  }
}
