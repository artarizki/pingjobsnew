import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/totaljobs_model.dart';
import 'package:pingjobs/pages/chat/chat_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:provider/provider.dart';

class JobSeekerLocationinfo extends StatefulWidget {
  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobs;
  final int distance2;

  const JobSeekerLocationinfo({
    Key key,
    this.aktivitasData,
    this.totalJobs,
    this.distance2,
  }) : super(key: key);

  @override
  _JobSeekerLocationinfoState createState() =>
      _JobSeekerLocationinfoState(aktivitasData, totalJobs, distance2);
}

class _JobSeekerLocationinfoState extends State<JobSeekerLocationinfo> {
  ModelAktivitas aktivitasData;
  ModelTotalJobs totalJobs;
  int distance2;
  int kmdistance;
  var fotoProfil;
  int index;

  Location location;
  List<Address> addresses;
  LocationData currentLocation;
  var realAddress;

  _JobSeekerLocationinfoState(
    this.aktivitasData,
    this.totalJobs,
    this.distance2,
  );

  Future getDistance() async {
    if(distance2 >= 1000)
      kmdistance = (distance2/1000).ceil();
    setState(() {
      if(distance2 >= 1000)
        kmdistance = (distance2/1000).ceil();
    });
  }
  
  Future setCurrentLoc() async {
    currentLocation = await location.getLocation();
    await getAddress(currentLocation.latitude, currentLocation.longitude);
  }

  Future runSetLoc() async {
    await setCurrentLoc();
  }

  Future<String> getAddress(lat, long) async {
    try {
      final coordinates = new Coordinates(lat, long);
      addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      realAddress = addresses.first.addressLine;
      setState(() {
        realAddress = addresses.first.addressLine;
      });
      print("Alamat : $realAddress");
    } catch (e) {
      print("Error occured: $e");
      return null;
    }
  }

  @override
  void initState() {
    super.initState();
    if (distance2 >= 1000) kmdistance = (distance2 / 1000).ceil();
    setState(() {
      if (distance2 >= 1000) kmdistance = (distance2 / 1000).ceil();
    });
    location = new Location();
    runSetLoc();
    location.onLocationChanged.listen((LocationData cLocation) {
      currentLocation = cLocation;
    });
    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
  }

  @override
  Widget build(BuildContext context) {
    getAddress(currentLocation?.latitude ?? 0, currentLocation?.longitude ?? 0);
    print("ALAMAT : $realAddress");
    String mediaUrlUsers =
        'https://bertigagroup.com/pj.com/public/storage/images/users/';

    getC(BuildContext context) {
      var provider = Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
      return provider;
    }

    var provider = Provider.of<ConversationProvider>(context, listen: true);

    Future sendMessage() async {
      print("KE CHAT");
      await checkId();
      var providerGet;
      print(
        "INDEX : " + index.toString(),
      );
      print("ID JOB : " + aktivitasData.idJob + aktivitasData.jobName);
      String message = "Halo, Saya ingin bertanya tentang pekerjaan ini";
      if (index == null) {
        print("MASUK INDEKS NULL");
        await provider.newMessage(message, aktivitasData.idJobprovider);
        providerGet = await getC(context);
        providerGet.asMap().forEach((indexData, item) async {
          print(
            "INDEKS USER ID CONVERSATION2 : " + item.user.id.toString(),
          );
          if (aktivitasData.idJobprovider == item.user.id.toString()) {
            index = indexData;
            setState(() {
              index = indexData;
            });
            print("INDEXSAME = $index");
          }
        });
        final result = await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ChatScreen(
              conversation: providerGet[index],
            );
          }),
        );
        index = result[0];
        setState(() {
          index = result[0];
        });
        print(
          "PANJANG IDJOBPROV: " + providerGet.length.toString(),
        );
        print(
          "INDEXSETELAH ISI : " + index.toString(),
        );
      } else {
        print("GAK MASUK INDEKS NULL");
        print(
          "INDEKS CONVERSATION KE :" + index.toString(),
        );
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => ChatScreen(
              conversation: provider.concersations[index],
            ),
          ),
        );
      };
    }

    return DraggableScrollableSheet(
      initialChildSize: 0.3,
      minChildSize: 0.3,
      maxChildSize: 0.75,
      builder: (BuildContext context, scrollController) {
        return Container(
          color: Style.bgColor,
          margin: EdgeInsets.symmetric(horizontal: 0.0),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Center(
                    child: Container(
                      color: Colors.black54,
                      height: 3,
                      width: MediaQuery.of(context).size.width * 0.2,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        maxRadius: 40,
                        backgroundImage: fotoProfil == null
                            ? NetworkImage(
                                'https://s3.amazonaws.com/37assets/svn/765-default-avatar.png')
                            : NetworkImage(mediaUrlUsers + fotoProfil),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(aktivitasData.jobName,
                                  style: Style.appStyleJobTitle22900),
                              Text(totalJobs.name, style: Style.appStyle18),
                              Text(
                                totalJobs?.jobPosted == null
                                    ? '0 total pekerjaan terposting'
                                    : '${totalJobs.jobPosted.toString()} total pekerjaan terposting',
                                style: Style.appStyle16,
                              ),
                              distance2.compareTo(10) == 1
                                  ? Text('Anda Sedang Dalam Perjalanan',
                                      style: Style.appStyle20600blue)
                                  : Text('Anda Sampai di tujuan',
                                      style: Style.appStyle20600green)
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image(
                            image: AssetImage('assets/icons/money.png'),
                            width: 45,
                            height: 45,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Text('Gaji', style: Style.appStyle20Bold),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                            'Rp. ${toCurrencyString(aktivitasData.fee, thousandSeparator: ThousandSeparator.Period, mantissaLength: 0)}',
                            style: Style.appStyle20Bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      distance2 == null
                          ? Text('Jarak 0 m dari Anda', style: Style.appStyle16)
                          : distance2 >= 1000
                              ? Text(
                                  'Jarak ${kmdistance.toString()} km dari Anda',
                                  style: Style.appStyle16)
                              : Text(
                                  'Jarak ${distance2.toString()} m dari Anda',
                                  style: Style.appStyle16)
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Kategori Pekerjaan Sampingan',
                              style: Style.appStyle16),
                          Text(aktivitasData.jobCategory ?? 'Lainnya',
                              style: Style.appStyle16Bold),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.location_pin, color: Colors.orange),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Lokasi Anda', style: Style.appStyle18),
                              realAddress == null
                                  ? Text('Memuat lokasi Anda...',
                                      style: Style.appStyle22800)
                                  : Text(realAddress,
                                      style: Style.appStyle22800),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.location_pin, color: Colors.green),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Lokasi Pekerjaan', style: Style.appStyle18),
                              Text(aktivitasData.street,
                                  style: Style.appStyle22800),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: ElevatedButton.icon(
                          onPressed: sendMessage,
                          icon: Icon(Icons.send_rounded),
                          label: Text('Kirim Pesan'),
                          style: ElevatedButton.styleFrom(
                            primary: Style.primaryColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: ElevatedButton.icon(
                          onPressed: () async {
                            await FlutterPhoneDirectCaller.callNumber(
                                totalJobs.mobilenumber);
                          },
                          icon: Icon(Icons.call),
                          label: Text('Panggil'),
                          style: ElevatedButton.styleFrom(
                            primary: Style.primaryColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<int> checkId() async {
    var provider;
    provider = await Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    provider.asMap().forEach((indexData, item) {
      if (aktivitasData.idJobprovider == item.user.id.toString()) {
        index = indexData;
        setState(() {
          index = indexData;
        });
        print("INDEXSAME = $index");
      }
    });
    print("INDEKS SETELAH DICOCOKKAN : $index");
    return index;
  }
}
