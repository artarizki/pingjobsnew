import 'dart:async';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pingjobs/component/line_string.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/totaljobs_model.dart';
import 'package:pingjobs/component/network_helper.dart';
import 'package:geocoder/geocoder.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/pages/sidejobs/jobseeker/jobseeker_location_info_screen.dart';
import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 0;
const LatLng SOURCE_LOCATION = LatLng(-7.11439382, 112.4200467);
const double sourceLat = -7.11439382;
const double sourceLng = 112.4200467;
const LatLng DEST_LOCATION = LatLng(-7.1150422, 112.4158926);

class LokasiPekerja extends StatefulWidget {
  const LokasiPekerja(
      {Key key, this.aktivitasData, this.totalJobs, this.fotoProfil, this.id})
      : super(key: key);

  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobs;
  final fotoProfil;
  final String id;

  @override
  _LokasiPekerjaState createState() =>
      _LokasiPekerjaState(aktivitasData, totalJobs, fotoProfil, id);
}

class _LokasiPekerjaState extends State<LokasiPekerja> {
  final ModelAktivitas aktivitasData;
  final ModelTotalJobs totalJobs;
  var fotoProfil;
  int index;
  String id;

  _LokasiPekerjaState(
      this.aktivitasData, this.totalJobs, this.fotoProfil, this.id);

  // getAddress
  List<Address> addresses;

// deklarasi pin custom marker
  BitmapDescriptor sourceIcon;
  BitmapDescriptor trackingIcon;
  BitmapDescriptor destinationIcon;

// lokasi awal pengguna dan lokasi saat ini saat berpindah tempat
  LocationData currentLocation;
  LocationData newestLocation;
  LocationData startLocation;

// referensi ke lokasi tujuan
  LocationData destinationLocation;

// wrapper API Location
  Location location;
  Completer<GoogleMapController> _controller = Completer();

  // deklarasi marker, polyline untuk rute, dan koordinat
  Set<Marker> _markers = Set<Marker>();
  Set<Polyline> _polylines = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  Polyline polyline;

  // posisi kamera
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(-7.1143938, 112.4200467),
    zoom: 15,
  );

  // data rute dari request http
  var data;
  double distance;
  int distance2;
  int kmdistance;
  double xJPin;
  double yJPin;
  double southWestLat;
  double southWestLng;
  double northEastLat;
  double northEastLng;

  Future getDistance() async {
    distance = Geolocator.distanceBetween(
      currentLocation.latitude,
      currentLocation.longitude,
      double.parse(aktivitasData.xJPin),
      double.parse(aktivitasData.yJPin),
    );
    distance2 = distance.ceil();
    if(distance2 >= 1000)
      kmdistance = (distance2/1000).ceil();
    setState(() {
      distance2 = distance.ceil();
      if(distance2 >= 1000)
        kmdistance = (distance2/1000).ceil();
    });
  }
  
  @override
  void initState() {
    super.initState();

    xJPin = double.parse(aktivitasData.xJPin);
    yJPin = double.parse(aktivitasData.yJPin);

    Provider.of<ConversationProvider>(context, listen: false)
        .getConversations();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<ConversationProvider>(context, listen: false)
          .getConversations();
    });
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
    distance2 = 0;

    // inisialisasi lokasi
    location = new Location();

    // membuat pin
    setSourceAndDestinationIcons();

    // request rute dari API
    getJsonData();
    getDistance();
    // listen perubahan lokasi dari event onLocationChanged
    location.onLocationChanged.listen((LocationData cLocation) {
      // currentLocation terdapat Lat dan Longtude dari posisi user scr realtime
      currentLocation = cLocation;
      Future.delayed(Duration(seconds: 3), () {
        if (currentLocation.latitude <= destinationLocation.latitude &&
            currentLocation.longitude <= destinationLocation.longitude) {
          southWestLat = currentLocation.latitude;
          northEastLat = destinationLocation.latitude;
          southWestLng = currentLocation.longitude;
          northEastLng = destinationLocation.longitude;
        } else if(currentLocation.longitude <= destinationLocation.longitude){
          southWestLat = destinationLocation.latitude;
          northEastLat = currentLocation.latitude;
          southWestLng = currentLocation.longitude;
          northEastLng = destinationLocation.longitude;
        } else if(currentLocation.latitude <= destinationLocation.latitude){
          southWestLat = currentLocation.latitude;
          northEastLat = destinationLocation.latitude;
          southWestLng = destinationLocation.longitude;
          northEastLng = currentLocation.longitude;
        } else {
          southWestLat = destinationLocation.latitude;
          northEastLat = currentLocation.latitude;
          southWestLng = destinationLocation.longitude;
          northEastLng = currentLocation.longitude;
        }
        updatePinOnMap();
        updateLocation({
          "pinlat": currentLocation.latitude,
          "pinlng": currentLocation.longitude,
          "id": aktivitasData.idJob,
        });
        getDistance();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    getJsonData();

    // location.onLocationChanged.listen((LocationData cLocation) {
    //   currentLocation = cLocation;
    //   Future.delayed(Duration(seconds: 3), () {
    //     updatePinOnMap();
    //   });
    //   updateLocation({
    //     "pinlat": currentLocation.latitude,
    //     "pinlng": currentLocation.longitude,
    //     "id": userProvider.user.id,
    //   });
    // });

    return Container(
      color: Style.bgColor,
      child: Scaffold(
        appBar: _buildAppBar(context),
        backgroundColor: Style.bgColor,
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                myLocationEnabled: true,
                compassEnabled: true,
                tiltGesturesEnabled: false,
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: LatLng(
                    newestLocation?.latitude ?? 0,
                    newestLocation?.longitude ?? 0,
                  ),
                  zoom: 15,
                ),
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  showPinsonMap();
                },
                markers: _markers,
                polylines: _polylines,
              ),
            ),
            JobSeekerLocationinfo(
              aktivitasData: aktivitasData,
              totalJobs: totalJobs,
              distance2: distance2,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Lokasi Pekerjaan'),
      backgroundColor: Style.primaryColor,
      brightness: Brightness.dark,
      centerTitle: true,
      automaticallyImplyLeading: true,
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Future<String> updateLocation(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/update_userloc.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    var message = json.decode(response.body);
    print("Message : ${message['message']}");
    return message['message'].toString();
  }

  // request rute dari API
  void getJsonData() async {
    newestLocation = await location.getLocation();
    // Buat instance Class NetworkHelper yang menggunakan paket http
    // untuk meminta data ke server dan menerima respons sebagai format JSON
    // destCoordinate = LatLng(double.parse(aktivitasData.xJPin),double.parse(aktivitasData.yJPin));
    await setInitialLocation();
    NetworkHelper network;
    network = NetworkHelper(
      sourceLng: startLocation.longitude,
      sourceLat: startLocation.latitude,
      destLng: destinationLocation.longitude,
      destLat: destinationLocation.latitude,
    );
    print("CurrentLng : " +
        currentLocation.longitude.toString() +
        "CurrentLat : " +
        currentLocation.latitude.toString());
    print("StartLng : " +
        startLocation.longitude.toString() +
        "StartLat : " +
        startLocation.latitude.toString());
    print("DestLng : " +
        destinationLocation.longitude.toString() +
        "DestLat : " +
        destinationLocation.latitude.toString());

    try {
      // getData () mengembalikan data decoded json
      data = await network.getData();

      // dapat mencapai data JSON yang diinginkan secara manual
      LineString ls =
          LineString(data['features'][0]['geometry']['coordinates']);

      for (int i = 0; i < ls.lineString.length; i++) {
        polylineCoordinates
            .add(LatLng(ls.lineString[i][1], ls.lineString[i][0]));
      }
      //removePolyLines();
      if (polylineCoordinates.length == ls.lineString.length) setPolyLines();
    } catch (e) {
      print(e);
    }
  }

  // set initial location dengan mengambil posisi user terkini
  Future setInitialLocation() async {
    currentLocation = await location.getLocation();

    print("LATLONG : ${currentLocation.latitude} ${currentLocation.longitude}");

    print("CurrentLng : " +
        currentLocation.longitude.toString() +
        "CurrentLat : " +
        currentLocation.latitude.toString());

    startLocation = LocationData.fromMap({
      "longitude": currentLocation.longitude,
      "latitude": currentLocation.latitude,
    });

    print("StartLng : " +
        startLocation.longitude.toString() +
        "StartLat : " +
        startLocation.latitude.toString());

    if (xJPin != null) {
      print("MASUK ELSE");
      destinationLocation = LocationData.fromMap({
        "latitude": double.parse(aktivitasData.xJPin),
        "longitude": double.parse(aktivitasData.yJPin)
      });
      print(
          "DESTAKTIV : ${destinationLocation.latitude} ${destinationLocation.longitude}");
      print("DestLng : " +
          destinationLocation.longitude.toString() +
          "DestLat : " +
          destinationLocation.latitude.toString());
    }

    if(xJPin != null) {
      if (startLocation.latitude <= destinationLocation.latitude &&
          startLocation.longitude <= destinationLocation.longitude) {
        southWestLat = startLocation.latitude;
        northEastLat = destinationLocation.latitude;
        southWestLng = startLocation.longitude;
        northEastLng = destinationLocation.longitude;
      } else if(startLocation.longitude <= destinationLocation.longitude){
        southWestLat = destinationLocation.latitude;
        northEastLat = startLocation.latitude;
        southWestLng = startLocation.longitude;
        northEastLng = destinationLocation.longitude;
      } else if(startLocation.latitude <= destinationLocation.latitude){
        southWestLat = startLocation.latitude;
        northEastLat = destinationLocation.latitude;
        southWestLng = destinationLocation.longitude;
        northEastLng = startLocation.longitude;
      } else {
        southWestLat = destinationLocation.latitude;
        northEastLat = startLocation.latitude;
        southWestLng = destinationLocation.longitude;
        northEastLng = startLocation.longitude;
      }
    }
    // else {
    //   destinationLocation = LocationData.fromMap({
    //     "latitude": DEST_LOCATION.latitude,
    //     "longitude": DEST_LOCATION.longitude
    //   });
    // }

  }

  // membuat pin
  void setSourceAndDestinationIcons() {
    sourceIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange);
    trackingIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure);
    destinationIcon =
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen);
  }

  // memusatkan tampilan ke lokasi terkini
  void centerOnMap() async {
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          zoom: 15.0,
          tilt: CAMERA_TILT,
          bearing: CAMERA_BEARING,
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
        ),
      ),
    );
  }

  // memperbarui pin
  void updatePinOnMap() async {
    // membuat posisi baru di map dan kamera mengikutinya
    // CameraPosition cPosition = CameraPosition(
    //   zoom: CAMERA_ZOOM,
    //   tilt: CAMERA_TILT,
    //   bearing: CAMERA_BEARING,
    //   target: LatLng(currentLocation.latitude, currentLocation.longitude),
    // );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(southWestLat ?? 0, southWestLng ?? 0),
          northeast: LatLng(northEastLat ?? 0, northEastLng ?? 0),
        ),
        75));
    // controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));

    // agar flutter diberi tahu untuk mengubah posisi kamera
    setState(() {
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      // hapus penanda dan tambahkan lagi untuk pembaruan
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      _markers.add(
        Marker(
          markerId: MarkerId('trackingPin'),
          position: pinPosition,
          icon: trackingIcon,
        ),
      );
    });
  }

  // membuat rute dari 2 titik
  setPolyLines() {
    polyline = Polyline(
      polylineId: PolylineId("polyline"),
      color: Style.primaryColor,
      points: polylineCoordinates,
      width: 4,
    );
    _polylines.remove(polyline);
    _polylines.add(polyline);
    setState(() {});
  }

  void showPinsonMap() {
    // set lokasi rute
    setPolyLines();
    setState(() {
      // set lokasi rute
      setPolyLines();
      _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
      _markers.removeWhere((m) => m.markerId.value == 'destPin');
      _markers.removeWhere((m) => m.markerId.value == 'trackingPin');
      // add pin lokasi tujuan
      _markers.add(
        Marker(
          markerId: MarkerId('destPin'),
          position: LatLng(xJPin, yJPin),
          icon: destinationIcon,
          infoWindow: InfoWindow(
            title: "Lokasi Pekerjaan Sampingan",
            snippet: aktivitasData.jobName,
          ),
        ),
      );

      // add pin lokasi terkini/awal
      _markers.add(
        Marker(
          markerId: MarkerId('sourcePin'),
          position: LatLng(startLocation.latitude, startLocation.longitude),
          icon: sourceIcon,
          infoWindow: InfoWindow(
            title: "Posisi Awal Anda",
            snippet: "Posisi awal Kamu Disini !",
          ),
        ),
      );

      _markers.add(
        Marker(
          markerId: MarkerId('trackingPin'),
          position: LatLng(currentLocation.latitude, currentLocation.longitude),
          icon: trackingIcon,
          infoWindow: InfoWindow(
            title: "Posisi Anda Terkini",
            snippet: "Kamu Sekarang Disini !",
          ),
        ),
      );
    });
  }
}
