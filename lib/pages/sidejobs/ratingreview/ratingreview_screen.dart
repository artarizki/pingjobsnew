import 'package:flutter/material.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class RatingReview extends StatefulWidget {
  const RatingReview({Key key, this.aktivitasData, this.backStatus})
      : super(key: key);

  final ModelAktivitas aktivitasData;
  final bool backStatus;

  @override
  _RatingReviewState createState() =>
      _RatingReviewState(aktivitasData, backStatus);
}

class _RatingReviewState extends State<RatingReview> {
  final ModelAktivitas aktivitasData;
  double rating = 1.0;
  String review;
  final _reviewController = TextEditingController();
  bool _ketepatanwaktu = false;
  bool _sikaperilaku = false;
  String ketepatanwaktu;
  String sikaperilaku;
  FocusNode nodeReview;
  bool backStatus;

  _RatingReviewState(this.aktivitasData, this.backStatus);

  @override
  void initState() {
    super.initState();
    nodeReview = FocusNode();
  }

  @override
  void dispose() {
    nodeReview.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final RatingReviewVM rrVM = Provider.of<RatingReviewVM>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Rating Review'),
        backgroundColor: Style.primaryColor,
        brightness: Brightness.dark,
        centerTitle: true,
        automaticallyImplyLeading: backStatus,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => backStatus == false
              ? ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text('Harap isi rating dan review terlebih dahulu'),
                ))
              : Navigator.pop(context),
        ),
      ),
      body: WillPopScope(
        onWillPop: () async => backStatus,
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.86,
            // height: MediaQuery.of(context).size.height,
            color: Style.bgColor,
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 8, bottom: 16),
                    child: Text(
                      'Berikan bintang dan ulasanmu terhadap pekerjaan ini',
                      style: Style.appStyle16Light,
                    ),
                  ),
                  Card(
                      elevation: 10,
                      color: Colors.white,
                      child: Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: SmoothStarRating(
                              // allowHalfRating: true,
                              onRated: (value) {
                                rating = value;
                                setState(() {
                                  rating = value;
                                });
                              },
                              filledIconData: Icons.star,
                              halfFilledIconData: Icons.star_half,
                              defaultIconData: Icons.star_border,
                              starCount: 5,
                              rating: rating,
                              size: 40,
                              color: Colors.yellow,
                              borderColor: Colors.yellow,
                              spacing: 1,
                            ),
                          ))),
                  SizedBox(
                    height: 16,
                  ),
                  Card(
                    elevation: 10,
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(16),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: TextFormField(
                                  controller: _reviewController,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Harap isi Review Anda';
                                    }
                                    return null;
                                  },
                                  focusNode: nodeReview,
                                  keyboardType: TextInputType.multiline,
                                  maxLength: 100,
                                  maxLines: 10,
                                  textCapitalization:
                                      TextCapitalization.sentences,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 8.0),
                                      hintText: 'Bagikan ulasan mu disini',
                                      hintStyle: TextStyle(
                                        color: Colors.black26,
                                      )),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                  ),
                  aktivitasData.role == 'penyedia' ? Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Style.primaryColor),
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                    ),
                    child: CheckboxListTile(
                      title: Text('Ketepatan Waktu'),
                      activeColor: Style.primaryColor,
                      checkColor: Colors.white,
                      selected: _ketepatanwaktu,
                      value: _ketepatanwaktu,
                      onChanged: (bool value) {
                        _ketepatanwaktu = value;
                        ketepatanwaktu = _ketepatanwaktu == true ? "1" : "0";
                        setState(() {
                          _ketepatanwaktu = value;
                          ketepatanwaktu = _ketepatanwaktu == true ? "1" : "0";
                        });
                      },
                    ),
                  ) : SizedBox(),
                  aktivitasData.role == 'penyedia' ? Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Style.primaryColor),
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                    ),
                    child: CheckboxListTile(
                      title: Text('Sikap dan perilaku'),
                      activeColor: Style.primaryColor,
                      checkColor: Colors.white,
                      selected: _sikaperilaku,
                      value: _sikaperilaku,
                      onChanged: (bool value) {
                        _sikaperilaku = value;
                        sikaperilaku = _sikaperilaku == true ? "1" : "0";
                        setState(() {
                          _sikaperilaku = value;
                          sikaperilaku = _sikaperilaku == true ? "1" : "0";
                        });
                      },
                    ),
                  ) : SizedBox(),
                  Container(
                    color: Style.bgColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 24),
                          child: SizedBox(
                            child: Container(
                              height: 45,
                              width: double.infinity,
                              child: ElevatedButton.icon(
                                onPressed: () async {
                                  final UserProvider userProvider =
                                      Provider.of<UserProvider>(context,
                                          listen: false);
                                  var role = aktivitasData.role == 'pekerja'
                                      ? aktivitasData.idJobprovider
                                      : aktivitasData.idJobseeker;
                                  var ketepatan = aktivitasData.role == 'penyedia' ? ketepatanwaktu : null;
                                  var sikap = aktivitasData.role == 'penyedia' ? sikaperilaku : null;
                                  var rrData = {
                                    "id_user": userProvider.user.id,
                                    "id_user2": role,
                                    "id_job": aktivitasData.idJob,
                                    // "id_jobprovider": "2",
                                    "keterangan": _reviewController.text,
                                    "bintang": rating.toString(),
                                    "role": aktivitasData.role,
                                    "ketepatan_waktu": ketepatan,
                                    "sikap_perilaku": sikap,
                                  };
                                  print("rrDATA");
                                  print(rrData);
                                  await rrVM.showProgress(rrData, context);
                                },
                                icon: Icon(Icons.check),
                                label: Text('Selesai', style: Style.appStyle20),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.lightGreen,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
