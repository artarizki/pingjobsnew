import 'package:flutter/material.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/component/style.dart';

class PostJobSuccess extends StatefulWidget {
  const PostJobSuccess({Key key}) : super(key: key);

  Size get preferredSize => Size.fromHeight(50.0);

  @override
  _PostJobSuccessState createState() => _PostJobSuccessState();
}

class _PostJobSuccessState extends State<PostJobSuccess>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 120.0,
          ),
          Center(
            child: Image.asset(
              'assets/images/success/checkmark.png',
              width: 100,
              height: 100,
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Text("Posting Pekerjaan Berhasil !",
                  style: Style.appStyle22800),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              child: Text(
                "Terima Kasih telah posting pekerjaan di PingJobs. Semoga Selamat dan sukses sampai tujuan)",
                style: TextStyle(color: Colors.black87, fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(50, 5, 50, 5),
            child: SizedBox(
              width: double.infinity,
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return NavigationPage();
                  }));
                },
                textColor: Colors.white,
                color: Style.primaryColor,
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  'Kembali ke Beranda',
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
