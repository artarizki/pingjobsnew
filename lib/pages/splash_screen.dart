import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:pingjobs/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:pingjobs/component/style.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime(var userEsist) async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage(userEsist));
  }

  navigationPage(var userEsist) async {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => NavigationPage(),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      var userEsist =
          await Provider.of<AuthProvider>(context, listen: false).getUser();
      startTime(userEsist);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Style.primaryColor,
      body: new Center(
        child: Container(
          width: 150,
          height: 150,
          child: new Image.asset('assets/logos/logo.png'),
        ),
      ),
    );
  }
}
