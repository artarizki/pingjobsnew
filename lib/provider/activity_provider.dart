import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:http/http.dart' as http;

class AktivitasVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelAktivitas> _aktivitas;

  List<ModelAktivitas> get listAktivitas => _aktivitas;
  ModelAktivitas _detail;

  ModelAktivitas get detailAktivitas => _detail;

  // same as
  // List<ModelAktivitas>getListAktivitas() {
  //   return _aktivitas;
  // }
  set listAktivitas(List<ModelAktivitas> value) {
    _aktivitas = value;
    notifyListeners();
  }

  set detailAktivitas(ModelAktivitas value) {
    _detail = value;
    notifyListeners();
  }

  // same as
  // setListAktivitas(List<ModelAktivitas> aktivitas) {
  //     _aktivitas = [];
  //     _aktivitas = aktivitas;
  //     notifyListeners();
  // }

  // Prototype Get idAktivitas
  var _id = 0;

  int get idAktivitas => _id;

  set idAktivitas(int value) {
    _id = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelAktivitas>> fetchData(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_activity_history.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_activity_history.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelAktivitasFromJson(response.body);
    // print("MyData : " + myData.toString());
    listAktivitas = myData;
    return listAktivitas;
  }

  static Future<ModelAktivitas> getData(http.Client client) async {
    var encodedBody = json.encode({"id_user": 1});
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_activity_history.php');
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      ModelAktivitas modelAktivitas = ModelAktivitas.fromJson(json.decode(response.body));
      return modelAktivitas;
    }
  }

  //Connection to api webservice database
  Future<String> setNote(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_note.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_activity_history.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // List myData = modelAktivitasFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listAktivitas = myData;
    return message['message'].toString();
  }
}
