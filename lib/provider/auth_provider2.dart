import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/model/auth_model.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/pages/auth/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthVM extends ChangeNotifier {
  //Prototype get list data from json
  String match, idJobSeeker, idJobProvider, name, email;

  List<ModelAuth> _auth;

  List<ModelAuth> get listAuth => _auth;
  ModelAuth _detail;

  ModelAuth get detailAuth => _detail;

  set listAuth(List<ModelAuth> value) {
    _auth = value;
    notifyListeners();
  }

  set detailAuth(ModelAuth value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idAuth
  var _id = 0;

  int get idAuth => _id;

  set idAuth(int value) {
    _id = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelAuth>> fetchData() async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_profile.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_profile.php');
    final response = await http.get(url);
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelAuthFromJson(response.body);
    // print("MyData : " + myData.toString());
    listAuth = myData;
    return listAuth;
  }

  userRegistration(BuildContext context, var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    final response = await http.post(
        Uri.parse(
            "https://bertigagroup.com/pj.com/public/backend/register.php"),
        headers: headers,
        body: json.encode(data));

//
    var message = jsonDecode(response.body);

    if (message['message'] == 'Berhasil Mendaftarkan User') {
      print(message['message']);
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => Login()));
    } else {
      print(message['message']);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(message['message']),
            actions: <Widget>[
              ElevatedButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  userLogin(BuildContext context, var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/login.php");
    // var url = Uri.parse("http://pingjobs.000webhostapp.com/login.php");

    // Starting Web API Call.
    final response =
        await http.post(url, headers: headers, body: json.encode(data));

    var result = jsonDecode(response.body);
    match = result['message'];
    idJobSeeker = result['id_jobseeker'];
    idJobProvider = result['id_jobprovider'];
    name = result['name'];
    email = result['email'];

    // If the Response Message is Matched.
    if (result['message'] == 'Login Matched') {
      // Navigate to Profile Screen & Sending Email to Next Screen.
      savePref();
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => NavigationPage()));
    } else {
      // If Email or Password did not Matched.
      // Hiding the CircularProgressIndicator.
      // Showing Alert Dialog with Response JSON Message.
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(result['message']),
            actions: <Widget>[
              ElevatedButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  userForgotPassword(BuildContext context, var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/forgot_password.php");

    // Starting Web API Call.
    final response =
        await http.post(url, headers: headers, body: json.encode(data));

    var result = jsonDecode(response.body);
    match = result['message'];

    // If the Response Message is Matched.
    if (result['message'] == 'Berhasil Ganti Password') {
      // Navigate to Profile Screen & Sending Email to Next Screen.
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => Login()));
    } else {
      // If Email or Password did not Matched.
      // Hiding the CircularProgressIndicator.
      // Showing Alert Dialog with Response JSON Message.
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(result['message']),
            actions: <Widget>[
              ElevatedButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  static Future uForgotPassword(http.Client client) async {
    var data = {
      "email": "arta@gmail.com",
      "password": "arta123",
      "confirmpassword": "arta123"
    };

    var encodedBody = json.encode(data);

    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/forgot_password.php");

    // Starting Web API Call.
    final response = await client.post(url, headers: headers, body: encodedBody);
    var result = jsonDecode(response.body);

    // If the Response Message is Matched.
    if (result['message'] == 'Berhasil Ganti Password') {
      if(response.statusCode != 200) {
        throw Exception("Error");
      } else {
        return result['message'].toString();
      }
    } else {
      if(response.statusCode != 200) {
        throw Exception("Error");
      } else {
        return result['message'].toString();
      }
    }
  }


  savePref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('match', match);
    preferences.setString('id_jobseeker', idJobSeeker);
    preferences.setString('id_jobprovider', idJobProvider);
    preferences.setString('name', name);
    preferences.setString('email', email);
    print("TES MATCH");
    print(preferences.getString('match'));
    print(preferences.getString('id_jobseeker'));
    print(preferences.getString('id_jobprovider'));
    print(preferences.getString('name'));
    print(preferences.getString('email'));
    // ignore: deprecated_member_use
//      preferences.commit();
  }

  Future<String> updateData(var data) async {
    // print("DATA");
    // print(data);
    // listPostJob = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/update_profile.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/update_profile.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // print("Message: ${message}");
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
    return message.toString();
  }

  Future<String> deleteData(var data) async {
    // print("DATA");
    // print(data);
    // listPostJob = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/delete_profile.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/delete_profile.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // print("Message: ${message}");
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
    return message.toString();
  }
}
