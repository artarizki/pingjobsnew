import 'dart:convert';

import 'package:pingjobs/model/conversation_model.dart';
import 'package:pingjobs/model/message_model.dart';
import 'package:pingjobs/provider/base_provider.dart';
import 'package:pingjobs/service/conversation_service.dart';

class ConversationProvider extends BaseProvider {
  ConversationService _conversationService = ConversationService();
  List<ConversationModel> _concersations = [];

  List<ConversationModel> get concersations => _concersations;

  Future<List<ConversationModel>> getConversations() async {
    if (_concersations.isNotEmpty) return _concersations;
    setBusy(true);
    var response = await _conversationService.getConversations();

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print("DATA CONVERSATION");
      print(data['data']);
      data['data'].forEach((conversation) =>
          _concersations.add(ConversationModel.fromJson(conversation)));
      print(response.body);
      notifyListeners();
      setBusy(false);
    }
    return _concersations;
  }

  Future<List<ConversationModel>> getConversations2() async {
    if (_concersations.isNotEmpty) return _concersations;
    setBusy(true);
    var response = await _conversationService.getConversations();

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print("DATA CONVERSATION 2");
      print(data['data']);
      data['data'].forEach((conversation) =>
          _concersations.add(ConversationModel.fromJson(conversation)));
      print(response.body);
      notifyListeners();
      setBusy(false);
    }
    return _concersations;
  }

  Future<void> storeMessage(MessageModal message) async {
    setBusy(true);
    var response = await _conversationService.storeMessage(message);
    // print(response.body);
    if (response.statusCode == 201) {
      var data = jsonDecode(response.body);
      setBusy(false);
      addMessageToConversation(
          message.conversationId, MessageModal.fromJson(data['data']));
    }
    setBusy(false);
  }

  Future<void> newMessage(String message, String idJobProvider) async {
    setBusy(true);
    var response =
        await _conversationService.newMessage(message, idJobProvider);
    print("MASUKNEWMESSAGE");
    print(response.statusCode);
    if (response.statusCode == 201) {
      // var data = jsonDecode(response.body);
      getConversations();
      notifyListeners();
      setBusy(false);
      // addMessageToConversation(
      //     message.conversationId, MessageModal.fromJson(data['data']));
    }
    setBusy(false);
  }

  addMessageToConversation(int conversationId, MessageModal message) {
    print("MASUKCONV");
    var conversation = _concersations
        .firstWhere((conversation) => conversation.id == conversationId);
    conversation.messages.add(message);
    print("PANJANG CONVERSATION : ${_concersations.length}");
    toTheTop(conversation);
    notifyListeners();
  }

  toTheTop(ConversationModel conversation) {
    print("TO THE TOP");
    var index = _concersations.indexOf(conversation);

    for (var i = index; i > 0; i--) {
      var x = _concersations[i];
      _concersations[i] = _concersations[i - 1];
      _concersations[i - 1] = x;
    }
  }
}
