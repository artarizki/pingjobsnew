import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/history_model.dart';
import 'package:http/http.dart' as http;

class HistoryVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelHistory> _history;
  ModelHistory _historydata = ModelHistory();

  List<ModelHistory> get listHistory => _history;

  ModelHistory get historydata => _historydata;

  set listHistory(List<ModelHistory> value) {
    _history = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelHistory>> fetchData(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_history.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_history.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelHistoryFromJson(response.body);
    // print("MyData : " + myData.toString());
    listHistory = myData;
    return listHistory;
  }

  static Future<ModelHistory> getData(http.Client client) async {
    var encodedBody = json.encode({"id_user": 1});
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_history.php');
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      ModelHistory modelHistory = ModelHistory.fromJson(json.decode(response.body));
      return modelHistory;
    }
  }
}
