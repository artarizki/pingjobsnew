import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:http/http.dart';
import 'package:multi_image_picker2/multi_image_picker2.dart';
import 'package:pingjobs/component/style.dart';
import 'package:pingjobs/model/postjob_model.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/pages/navigation_screen.dart';
import 'package:pingjobs/pages/sidejobs/success/post_success_screen.dart';
import 'package:http_parser/http_parser.dart';

class PostJobVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelPostJob> _postJob;

  List<ModelPostJob> get listPostJob => _postJob;
  ModelPostJob _detail;

  ModelPostJob get detailPostJob => _detail;
  String message;
  var responseUpload;

  set listPostJob(List<ModelPostJob> value) {
    _postJob = value;
    notifyListeners();
  }

  set detailPostJob(ModelPostJob value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idPostJob
  var _id = 0;

  int get idPostJob => _id;

  set idPostJob(int value) {
    _id = value;
    notifyListeners();
  }

  uploadImages(List<Asset> images1) async {
    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/add_images.php");
    // create multipart request
    MultipartRequest request = http.MultipartRequest("POST", url);

    for (Asset assets in images1) {
      print("FILENAME : " + assets.name);
      ByteData byteData = await assets.getByteData();
      //ini bisa compress quality
      List<int> imageData = await FlutterImageCompress.compressWithList(
        byteData.buffer.asUint8List(),
        quality: 10,
      );
      // List<int> compressedImageData = await FlutterImageCompress.compressWithList(
      //   imageData,
      //   quality: 1,
      // );
      MultipartFile multipartFile = new MultipartFile.fromBytes(
        "photo[]",
        imageData,
        filename: assets.name,
        contentType: MediaType("image", "jpg"),
      );
      // add file to multipart
      request.files.add(multipartFile);
      // send
    }
    print("request.files.length");
    print(request.files.length);
    responseUpload = await request.send();
    print(responseUpload.statusCode);
    if (responseUpload.statusCode == 200) {
      message = "Uploaded";
    } else {
      message = "Upload Failed";
    }
    return message;
  }

  static addImages(http.Client client) async {
    var responseUpload;
    String message;
    List<int> imageBytes = (await rootBundle.load('assets/images/cucimobiltest.jpg')).buffer.asUint8List();
    // SERVER LOGIN API URL
    var url = Uri.parse(
        "https://bertigagroup.com/pj.com/public/backend/add_images.php");

    // create multipart request
    MultipartRequest request = http.MultipartRequest("POST", url);
    MultipartFile multipartFile = new MultipartFile.fromBytes(
      "photo[]",
      imageBytes,
      filename: "testupload.jpg",
      contentType: MediaType("image", "jpg"),
    );

    // add file to multipart
    request.files.add(multipartFile);

    // send
    responseUpload = await request.send();
    if (responseUpload.statusCode == 200) {
      message = "Uploaded";
    } else {
      message = "Upload Failed";
    }
    return message;
  }

  Future<void> showProgress(BuildContext context, Map data, List<Asset> images1,
      List<String> filename1) async {
    var result = await showDialog(
      context: context,
      builder: (context) => FutureProgressDialog(
        insertData(
          context,
          data,
          images1,
          filename1,
        ),
        message: Text('Mengupload Pekerjaan...'),
      ),
    );
    if(result == 'Insert Job Success'){
      Navigator.push(context, MaterialPageRoute(builder: (_) {
        return PostJobSuccess();
      }));
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(result ?? 'Upload Pekerjaan Gagal'),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Style.primaryColor),
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
//    return Navigator.push(
//        context,
//        MaterialPageRoute(builder: (context) => Home2(status: 'loggedin',))
//    );
  }

  //Connection to api webservice database
  insertData(BuildContext context, Map data, List<Asset> images1,
      List<String> filename1) async {
    print("DATA");
    print(data);

    String uploadResponse = await uploadImages(images1);
    print("UPLOADRESPONSE : $uploadResponse");
    print("LISTIMAGES : ${filename1.join(",")}");
    if (uploadResponse == "Uploaded") {
      print("LISTIMAGES : ${filename1.join(",")}");
      data.putIfAbsent("job_photovideo", () => filename1.join(",").toString());
      print("DATA NEW");
      print(data);

      // listPostJob = data;
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
      Uri url = Uri.parse(
          'https://bertigagroup.com/pj.com/public/backend/add_postjob.php');
      final response =
          await http.post(url, headers: headers, body: json.encode(data));
      print("RESPONSE :");
      print(response.statusCode);
      var message = json.decode(response.body);
      print("Message: ${message['message']}");
      return message['message'].toString();
    } else {
      return uploadResponse;
    }
  }

  static Future<String> addData(http.Client client) async {
    var data = {
      "id": 1,
      "job_name": "Test Judul Pekerjaan",
      "job_category": 'Lainnya',
      'job_date': '2021-01-01 00:00:00',
      "job_description": "Test Deksripsi Pekerjaan",
      "fee": "0",
      "hours": "0",
      "job_photovideo": "testupload.jpg",
      "street": "Test Alamat",
      "city": "Test Kota",
      "pinlat": 0,
      "pinlng": 0
    };
    String uploadResponse = await addImages(client);
    if (uploadResponse == "Uploaded") {
      var encodedBody = json.encode(data);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
      Uri url = Uri.parse(
          'https://bertigagroup.com/pj.com/public/backend/add_postjob.php');
      final response = await client.post(url, headers: headers, body: encodedBody);
      var message = json.decode(response.body);
      if(response.statusCode != 200) {
        throw Exception("Error");
      } else {
        return message['message'].toString();
      }
    } else if(uploadResponse == "Upload Failed") {
      var encodedBody = json.encode(data);
      Map<String, String> headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
      Uri url = Uri.parse(
          'https://bertigagroup.com/pj.com/public/backend/add_postjob.php');
      final response = await client.post(url, headers: headers, body: encodedBody);
      var message = json.decode(response.body);
      if(response.statusCode != 200) {
        throw Exception("Error");
      } else {
        return message['message'].toString();
      }
    } else {
      throw Exception("Error");
    }
  }

  Future<String> applyData(var data) async {
    print("DATA");
    print(data);
    // listPostJob = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/apply_postjob.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/apply_postjob.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    print("RESPONSE :");
    print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    print("Message: $message");
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
    return message.toString();
  }

  static Future<String> chooseData(http.Client client) async {
    // listPostJob = data;
    var data = {
      "id_jobseeker": "1",
      "id_job_applied": "6",
      "job_date": "2021-06-22 00:00:00"
    };
    var encodedBody = json.encode(data);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/apply_postjob.php');
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      var message = json.decode(response.body);
      return message["message"].toString();
    }
  }

  Future<String> cancelData(var data, BuildContext context) async {
    print("DATA");
    print(data);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/cancel_job.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/cancel_job.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    print("RESPONSE CANCEL JOB :");
    print(response.statusCode);
    var message = json.decode(response.body);
    print("Message: $message");
    if (message['message'] == 'Cancel Success') {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Berhasil Membatalkan Pekerjaan'),
        ),
      );
      return Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NavigationPage(),
        ),
      );
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(message['message']),
            actions: <Widget>[
              TextButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    return message['message'];
  }
}
