import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/profile_model.dart';
import 'package:http/http.dart' as http;

class ProfilVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelProfile> _profil;
  ModelProfile _profildata = ModelProfile();

  List<ModelProfile> get listProfil => _profil;

  ModelProfile get profildata => _profildata;
  ModelProfile _detail;

  ModelProfile get detailProfil => _detail;

  set listProfil(List<ModelProfile> value) {
    _profil = value;
    notifyListeners();
  }

  set detailProfil(ModelProfile value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idProfil
  var _id = 0;

  int get idProfil => _id;

  set idProfil(int value) {
    _id = value;
    notifyListeners();
  }
}
