import 'package:pingjobs/provider/conversation_provider.dart';
import 'package:pingjobs/provider/locator.dart';
import 'package:pingjobs/provider/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'auth_provider.dart';

List<SingleChildWidget> provider = [
  ChangeNotifierProvider(
    create: (context) => locator<ConversationProvider>(),
  ),
  ChangeNotifierProvider(
    create: (context) => locator<AuthProvider>(),
  ),
  ChangeNotifierProvider(
    create: (context) => locator<UserProvider>(),
  ),
];
