import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/ratingreview_profile_model.dart';
import 'package:http/http.dart' as http;

class RatingReviewProfileVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelRatingReviewProfile> _ratingreviewprofile;
  ModelRatingReviewProfile _ratingreviewprofiledata =
      ModelRatingReviewProfile();

  List<ModelRatingReviewProfile> get listRatingReviewProfile =>
      _ratingreviewprofile;

  ModelRatingReviewProfile get ratingreviewprofiledata =>
      _ratingreviewprofiledata;

  set listRatingReviewProfile(List<ModelRatingReviewProfile> value) {
    _ratingreviewprofile = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelRatingReviewProfile>> fetchData(var data) async {
    // smkitanwirulafkar.sch.id
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_historyanotherprofile.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    print("RESPONSE RRPROFILE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelRatingReviewProfileFromJson(response.body);
    // print("MyData : " + myData.toString());
    listRatingReviewProfile = myData;
    return listRatingReviewProfile;
  }
}
