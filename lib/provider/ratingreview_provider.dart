import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:pingjobs/model/ratingreview_model.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/pages/navigation_screen.dart';

class RatingReviewVM extends ChangeNotifier {
  //Prototype get list data from json
  List<RatingReviewModel> _ratingreview;

  List<RatingReviewModel> get listRatingReview => _ratingreview;
  RatingReviewModel _detail;

  RatingReviewModel get detailRatingReview => _detail;
  bool _busy = false;

  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }

  set listRatingReview(List<RatingReviewModel> value) {
    _ratingreview = value;
    notifyListeners();
  }

  set detailRatingReview(RatingReviewModel value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idRatingReview
  var _id = 0;

  int get idRatingReview => _id;

  set idRatingReview(int value) {
    _id = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<RatingReviewModel>> fetchData(var data) async {
    // print(data);
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_ratingreview.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_jobs.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = ratingReviewModelFromJson(response.body);

    // print("MyData : " + myData.toString());
    listRatingReview = myData;
    return myData;
  }

  Future<void> showProgress(var data, BuildContext context) async {
    var result = await showDialog(
      context: context,
      builder: (context) => FutureProgressDialog(
        insertData(
          data,
          context,
        ),
        message: Text('Tunggu Sebentar...'),
      ),
    );
    if (result == 'RatingReview Success') {
      return Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => NavigationPage()));
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(result ?? 'Proses Rating Review Gagal'),
            actions: <Widget>[
              TextButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  //Connection to api webservice database
  insertData(var data, BuildContext context) async {
    print("DATA");
    print(data);
    // listRatingReview = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_ratingreview.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/add_postjob.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    print("RESPONSE :");
    print(response.statusCode);
    print(json.decode(response.body));
    var message = json.decode(response.body);
    print("Message: $message");

    return message['message'].toString();
  }

  static Future<String> addData(http.Client client) async {
    var rrData = {
      "id_user": "8",
      "id_user2": "1",
      "id_job": "7",
      "keterangan": "Test Review",
      "bintang": "5",
      "role": "penyedia",
      "ketepatan_waktu": "1",
      "sikap_perilaku": "1",
    };
    var encodedBody = json.encode(rrData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_ratingreview.php');
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      var message = json.decode(response.body);
      return message["message"].toString();
    }
  }
}
