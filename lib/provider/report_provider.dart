import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/report_model.dart';
import 'package:http/http.dart' as http;

class ReportVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelReport> _report;

  List<ModelReport> get listReport => _report;
  ModelReport _detail;

  ModelReport get detailReport => _detail;

  set listReport(List<ModelReport> value) {
    _report = value;
    notifyListeners();
  }

  set detailReport(ModelReport value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idReport
  var _id = 0;

  int get idReport => _id;

  set idReport(int value) {
    _id = value;
    notifyListeners();
  }

  Future<String> addData(var data) async {
    // print("DATA");
    // print(data);
    // listPostJob = data;
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_report.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/update_report.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // print("Message: ${message}");
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
    return message.toString();
  }

  static Future<String> insertData(http.Client client) async {
    var reportData = {
      "id_reporter": "2",
      "email_targetuser": "arta@gmail.com",
      "keterangan": "Test Report"
    };
    var encodedBody = json.encode(reportData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_report.php');
    //client bisa , http get dataerror tidak bisa
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      var message = json.decode(response.body);
      return message["message"].toString();
    }
  }
}
