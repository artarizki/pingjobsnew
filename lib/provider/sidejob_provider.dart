import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/pages/sidejobs/ratingreview/ratingreview_screen.dart';

class SideJobVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelSideJob> _sidejobs;
  List<ModelSideJob> _sidejobsfound;
  List<ModelSideJob> _sidejobsclear = [];

  List<ModelSideJob> get listSideJob => _sidejobs;

  List<ModelSideJob> get listSideJobfound => _sidejobsfound;
  ModelSideJob _detail;

  ModelSideJob get detailSideJob => _detail;

  // same as
  // List<ModelSideJob>getListSideJob() {
  //   return _sidejobs;
  // }
  set listSideJob(List<ModelSideJob> value) {
    _sidejobs = value;
    notifyListeners();
  }

  set listSideJobfound(List<ModelSideJob> value) {
    _sidejobsfound = value;
    notifyListeners();
  }

  set detailSideJob(ModelSideJob value) {
    _detail = value;
    notifyListeners();
  }

  // same as
  // setListSideJob(List<ModelSideJob> sidejobs) {
  //     _sidejobs = [];
  //     _sidejobs = sidejobs;
  //     notifyListeners();
  // }

  // Prototype Get idSideJob
  var _id = 0;

  int get idSideJob => _id;

  set idSideJob(int value) {
    _id = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelSideJob>> fetchData() async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_jobs.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_jobs.php');
    final response = await http.get(url);
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    if(response.statusCode != 200) {
      throw Exception("Error");
    }
    List myData = modelSideJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    listSideJob = myData;
    return myData;
  }

  static Future<ModelSideJob> getData(http.Client client) async {
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_jobs.php');
    final response = await client.get(url);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      ModelSideJob myData = ModelSideJob.fromJson(json.decode(response.body));
      return myData;
    }
  }

  //Connection to api webservice database
  Future<String> finishData(
      var data, BuildContext context, ModelAktivitas aktivitasData) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/end_job.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_jobs.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    var message = json.decode(response.body);
    // print("Message: ${message}");
    if (message['message'] == 'End Success') {
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  RatingReview(aktivitasData: aktivitasData, backStatus: false)));
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(message['message']),
            actions: <Widget>[
              TextButton(
                child: new Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    // List myData = modelPostJobFromJson(response.body);
    // print("MyData : " + myData.toString());
    // listPostJob = myData;
  }

  static Future<String> endData(http.Client client) async {
    var data = {"id_job": 1};
    var encodedBody = json.encode(data);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/end_job.php');
    final response = await client.post(url, headers: headers, body: encodedBody);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      var message = json.decode(response.body);
      return message["message"].toString();
    }
  }


  // Future<List<ModelSideJob>> fetchResultData() async{
  //   List myData = modelSideJobFromJson(response.body);
  //
  //   print("MyData : " + myData.toString());
  //   listSideJob = myData;
  //   return myData;
  // }

  Future<List<ModelSideJob>> searchData(List<ModelSideJob> searchData) async {
    listSideJobfound = searchData;
    return searchData;
  }

  Future<List<ModelSideJob>> clearData() async {
    _sidejobsfound.clear();
    listSideJobfound = _sidejobsclear;
    return _sidejobsclear;
  }
}
