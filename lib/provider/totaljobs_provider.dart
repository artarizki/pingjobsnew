import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:pingjobs/model/totaljobs_model.dart';
import 'package:http/http.dart' as http;

class TotalJobsVM extends ChangeNotifier {
  //Prototype get list data from json
  List<ModelTotalJobs> _totalJobs;
  ModelTotalJobs _totalJobsData = ModelTotalJobs();

  List<ModelTotalJobs> get listTotalJobs => _totalJobs;

  ModelTotalJobs get totalJobsData => _totalJobsData;

  set listTotalJobs(List<ModelTotalJobs> value) {
    _totalJobs = value;
    notifyListeners();
  }

  List<ModelTotalJobs> _totalJobs2;

  List<ModelTotalJobs> get listTotalJobs2 => _totalJobs2;
  ModelTotalJobs _totalJobsData2 = ModelTotalJobs();

  ModelTotalJobs get totalJobsData2 => _totalJobsData2;

  set listTotalJobs2(List<ModelTotalJobs> value) {
    _totalJobs2 = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<ModelTotalJobs>> getTotalJobs(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_totaljobs.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_totalJobse.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelTotalJobsFromJson(response.body);
    // print("MyData : " + myData.toString());
    listTotalJobs = myData;
    return listTotalJobs;
  }

  Future<List<ModelTotalJobs>> getTotalJobsAnotherUSer(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_totaljobs.php');
    // Uri url = Uri.parse('http://pingjobs.000webhostapp.com/get_totalJobse.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = modelTotalJobsFromJson(response.body);
    // print("MyData : " + myData.toString());
    listTotalJobs2 = myData;
    return listTotalJobs2;
  }
}
