import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:pingjobs/model/ratingreview_model.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/totalrr_model.dart';
import 'package:pingjobs/pages/navigation_screen.dart';

class TotalRRVM extends ChangeNotifier {
  //Prototype get list data from json
  List<TotalRrModel> _ratingreview;

  List<TotalRrModel> get listTotalRR => _ratingreview;
  TotalRrModel _detail;

  TotalRrModel get detailTotalRR => _detail;
  bool _busy = false;

  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }

  set listTotalRR(List<TotalRrModel> value) {
    _ratingreview = value;
    notifyListeners();
  }

  set detailTotalRR(TotalRrModel value) {
    _detail = value;
    notifyListeners();
  }

  // Prototype Get idTotalRR
  var _id = 0;

  int get idTotalRR => _id;

  set idTotalRR(int value) {
    _id = value;
    notifyListeners();
  }

  //Connection to api webservice database
  Future<List<TotalRrModel>> fetchData(var data) async {
    // print(data);
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_count_rr.php');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    // print("RESPONSE :");
    // print(response.statusCode);
    // print(json.decode(response.body));
    List myData = totalRrModelFromJson(response.body);

    // print("MyData : " + myData.toString());
    listTotalRR = myData;
    return myData;
  }
}
