import 'dart:convert';
import 'dart:io';
import 'package:pingjobs/model/user_model.dart';
import 'package:pingjobs/provider/base_provider.dart';
import 'package:pingjobs/service/user_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

class UserProvider extends BaseProvider {
  UserModel _user = UserModel();
  UserService _userService = UserService();

  UserModel get user => _user;
  String fileName;

  setUser(UserModel user) {
    _user = user;
    notifyListeners();
  }

  Future<String> imageNametoDbase(var data) async {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_imagename.php');
    final response =
        await http.post(url, headers: headers, body: json.encode(data));
    print("RESPONSE :");
    print(response.statusCode);
    print(json.decode(response.body));
    var message = json.decode(response.body);
    return message['message'].toString();
  }

  Future<bool> updateUser() async {
    setBusy(true);
    var response = await _userService.updateUser(_user);
    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      setUser(UserModel.fromJson(data['data']));

      setBusy(false);
      return true;
    }
    if (response.statusCode == 422) {
      var message = data['errors']['email'][0];
      print(message);
      setMessage(message);
      return false;
    }
    setBusy(false);
    return false;
  }

  Future<void> pickImage() async {
    var image = await ImagePicker().getImage(source: ImageSource.gallery);
    setBusy(true);
    // return;
    File file = File(image.path);
    fileName = file.path.split('/').last;

    var response = await _userService.uploadImage(file);
    print(response.body);

    String result =
        await imageNametoDbase({"file_name": fileName, "id": _user.id});
    print("RESULT : " + result);

    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      print(data);
      setUser(UserModel.fromJson(data['data']));
      print(_user.toJson());
      setBusy(false);
      return true;
    }
  }

  Future<void> setFcmToken(String token) async {
    _userService.setFcmToken(token);
  }
}
