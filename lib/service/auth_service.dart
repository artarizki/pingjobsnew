import 'package:http/http.dart' as http;
import 'package:pingjobs/model/user_model.dart';
import 'package:pingjobs/service/base_api.dart';

class AuthService extends BaseApi {
  Future<http.Response> getUser() async {
    return await api.httpGet('user');
  }

  Future<http.Response> login(String email, password) async {
    return await api.httpPost('login', {'email': email, 'password': password});
  }

  Future<http.Response> register(UserModel user) async {
    return await api.httpPost('register', {
      'email': user.email,
      'password': user.password,
      'name': user.name,
      'mobilenumber': user.mobilenumber
    });
  }

  Future<http.Response> logout() async {
    return await api.httpPost('logout', {});
  }
}
