import 'package:pingjobs/model/message_model.dart';
import 'package:pingjobs/service/base_api.dart';
import 'package:http/http.dart' as http;

class ConversationService extends BaseApi {
  Future<http.Response> getConversations() async {
    return await api.httpGet('conversations');
  }

  Future<http.Response> storeMessage(MessageModal message) async {
    return await api.httpPost('messages', {
      'body': message.body,
      'conversation_id': message.conversationId.toString()
    });
  }

  Future<http.Response> newMessage(String message, String idJobProvider) async {
    return await api.httpPost(
        'conversations', {'user_id': idJobProvider, 'message': message});
  }
}
