import 'package:pingjobs/model/user_model.dart';
import 'package:pingjobs/service/base_api.dart';
import 'package:http/http.dart' as http;

class UserService extends BaseApi {
  Future<http.Response> updateUser(UserModel user) async {
    return await api.httpPost('update', {
      'email': user.email,
      'name': user.name,
      'specialization': user.specialization,
      'mobilenumber': user.mobilenumber,
      'bio': user.bio
    });
  }

  Future<http.Response> uploadImage(var file) async {
    return await api.httpPostWithFile('upload', file: file);
  }

  Future<http.Response> setFcmToken(String token) async {
    return await api.httpPost('fcm', {'fcm_token': token.toString()});
  }
}
