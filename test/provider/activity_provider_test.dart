import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pingjobs/model/activity_model.dart';
import 'package:pingjobs/provider/activity_provider.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'activity_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('getActivityData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
              'https://bertigagroup.com/pj.com/public/backend/get_activity_history.php'), headers: headers, body: json.encode({"id_user": 1})))
        .thenAnswer((_) async => http.Response('''
            {
                "id_activity_history": "2",
                "id_job": "1",
                "id_jobseeker": "1",
                "id_jobprovider": "2",
                "role": "pekerja",
                "status": "selesai",
                "note": "jangann",
                "X(a.coordinate_jobseeker)": null,
                "Y(a.coordinate_jobseeker)": null,
                "start_date": "2021-03-21 14:13:02",
                "end_date": "2021-03-21 14:13:02",
                "create_at": "2021-03-21 14:13:02",
                "update_at": "2021-03-21 14:13:02",
                "job_name": "Jalan-jalan kucing",
                "job_category": null,
                "job_description": "Kucing saya butuh jalan jalan 3 kali seminggu. Dibutuhkan orang untuk bisa menemaninya karena saya tidak dapat menemaninya selama sebulan",
                "fee": "15000",
                "hours": "3",
                "job_photovideo": "kucing.jpg",
                "street": "Krian, Sidoarjo",
                "X(j.pin)": "-7.1150422",
                "Y(j.pin)": "112.4158926"
            }''', 200));
    // List<ModelAktivitas> modelAktivitas = await AktivitasVM.getData(client);
    expect(await AktivitasVM.getData(client), isA<ModelAktivitas>());
  });

  test('getActivityDataError', () {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_activity_history.php'), headers: headers, body: json.encode({"id_user": 1})))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelActivity> modelActivity = await ActivityVM.getData(client);
    expect(AktivitasVM.getData(client), throwsException);
  });
}