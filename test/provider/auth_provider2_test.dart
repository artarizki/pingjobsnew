import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/report_model.dart';
import 'package:pingjobs/model/user_model.dart';
import 'package:pingjobs/provider/auth_provider2.dart';
import 'package:pingjobs/provider/report_provider.dart';
import 'package:pingjobs/service/auth_service.dart';

import 'auth_provider2_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('forgotpassword', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {
      "email": "arta@gmail.com",
      "password": "arta123",
      "confirmpassword": "arta123",
    };

    var encodedBody = json.encode(data);

    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/forgot_password.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('{"message":"Berhasil Ganti Password"}', 200));
    expect(await AuthVM.uForgotPassword(client), equals("Berhasil Ganti Password"));
  });

  test('forgotpasswordError', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {
      "email": "arta@gmail.com",
      "password": "arta123",
      "confirmpassword": "arta123",
    };

    var encodedBody = json.encode(data);

    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/forgot_password.php') , headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    expect(AuthVM.uForgotPassword(client), throwsException);
  });
}