import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/report_model.dart';
import 'package:pingjobs/model/user_model.dart';
import 'package:pingjobs/provider/auth_provider2.dart';
import 'package:pingjobs/provider/report_provider.dart';
import 'package:pingjobs/service/auth_service.dart';

import 'auth_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  Future<Map> getToken(http.Client client) async {
    var formData = {
      'grant_type' : 'password',
      'client_id' : '2',
      'client_secret' : 'eMc9vSxtrUsQGajD6bVr6uedwjIgUIu4P8KRooCd',
      'username' : 'arta@gmail.com',
      'password' : 'arta123',
      'scope' : '*',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/oauth/token');
    //client bisa , http get dataerror tidak bisa
    final response = await http.post(url, body: formData);
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      Map message = json.decode(response.body);
      message.remove("access_token");
      message.remove("refresh_token");
      return message;
    }
  }

  Future<Map> login2(http.Client client) async {
    var formData = {
      "email" : "arta@gmail.com",
      "password" : "arta123"
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/api/login');
    final response = await http.post(url, body: json.encode(formData));
    if(response.statusCode != 200) {
      throw Exception("Error");
    } else {
      Map message = json.decode(response.body);
      message.remove("access_token");
      message.remove("refresh_token");
      return message;
    }
  }

  Future<Map> register2(http.Client client) async {
    // hati2
    var formData = {
      "email" : "test21@gmail.com",
      "password" : "test21123",
      "name" : "test21",
      "mobilenumber" : "0"
    };
    var encodedBody = json.encode(formData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/api/register');
    final response = await http.post(url, headers: headers, body: encodedBody);
    print(response.body);
    if(response.statusCode != 201) {
      throw Exception("Error");
    } else {
      Map message = json.decode(response.body);
      message["data"].remove("id");
      message["data"].remove("created_at");
      return message;
    }
  }

  Future<Map> register3(http.Client client) async {
    // hati2
    var formData = {
      "email" : "test21@gmail.com",
      "password" : "test21123",
      "name" : "test21",
      "mobilenumber" : "0"
    };
    var encodedBody = json.encode(formData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Uri url = Uri.parse(
        'https://bertigagroup.com/pj.com/public/api/register');
    final response = await http.post(url, headers: headers, body: encodedBody);
    print(response.body);
    if(response.statusCode != 201) {
      throw Exception("Error");
    } else {
      Map message = json.decode(response.body);
      message["data"].remove("id");
      message["data"].remove("created_at");
      return message;
    }
  }


  test('login', () async {
    AuthService _authService = AuthService();
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {
      "email": "arta@gmail.com",
      "password": "arta123"
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Map response = await getToken(client);
    print("response : $response");

    var response2 = await _authService.login('arta@gmail.com', 'arta123');
    Map data2 = jsonDecode(response2.body);
    data2.remove("access_token");
    data2.remove("refresh_token");

    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/api/login'), headers: headers, body: data))
        .thenAnswer((_) async => http.Response('$data2', 200));
    expect(data2, equals(response));
  });

  test('loginError', () async {
    AuthService _authService = AuthService();
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {
      "email": "arta@gmail.com",
      "password": "arta123"
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    Map response = await getToken(client);
    print("response : $response");

    var response2 = await _authService.login('arta@gmail.com', 'arta123');
    Map data2 = jsonDecode(response2.body);
    data2.remove("access_token");
    data2.remove("refresh_token");

    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/api/login') , headers: headers, body: data))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    expect(login2(client), throwsException);
  });

  test('register', () async {
    UserModel _user = UserModel();
    AuthService _authService = AuthService();
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var formData = {
      "email": "test21@gmail.com",
      "password": "test21123",
      "name": "test21",
      "mobilenumber": "0"
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };

    Map response = await register2(client);
    print("response : $response");

    _user.email = "test22@gmail.com";
    _user.password = "test22123";
    _user.name = "test22";
    _user.mobilenumber = "0";

    var response2 = await _authService.register(_user);
    Map data2 = json.decode(response2.body);
    data2["data"].remove("id");
    data2["data"].remove("created_at");
    data2["data"].update('name', (value) => "test21");
    data2["data"].update('email', (value) => "test21@gmail.com");

    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/api/register'), body: formData))
        .thenAnswer((_) async => http.Response('$data2', 200));
    expect(data2, equals(response));
  });

  test('registerError', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var formData = {
      "email": "test21@gmail.com",
      "password": "test21123",
      "name": "test21",
      "mobilenumber": "0"
    };
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/api/register') , headers: headers, body: formData))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    expect(register3(client), throwsException);
  });
}