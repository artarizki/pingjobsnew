import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/history_model.dart';
import 'package:pingjobs/provider/history_provider.dart';

import 'history_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('getHistoryData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_history.php'), headers: headers, body: json.encode({"id_user": 1})))
        .thenAnswer((_) async => http.Response('''
            {
                "start_date": "3 March",
                "id_job": "2",
                "role": "pekerja",
                "job_name": "Bantuan Kursus 1 Hari",
                "job_description": "Saya lagi keluar kota. dia tidak bias belajar tanpa pendampingan seseorang. Dibutuhkan tenaga bantuan kursus 1 hari untuk pelajaran Bahasa inggris",
                "fee": "24000",
                "hours": "4",
                "job_photovideo": "kursus.jpg",
                "id_user": "2",
                "id_user2": "1",
                "keterangan": "AA",
                "bintang": "3",
                "ketepatan_waktu": "1",
                "sikap_perilaku": "1",
                "id_jobseeker": "1",
                "id_jobprovider": "3"
            }''', 200));
    // List<ModelHistory> modelHistory = await HistoryVM.getData(client);
    expect(await HistoryVM.getData(client), isA<ModelHistory>());
  });

  test('getHistoryDataError', () {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_history.php'), headers: headers, body: json.encode({"id_user": 1})))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelHistory> modelHistory = await HistoryVM.getData(client);
    expect(HistoryVM.getData(client), throwsException);
  });
}