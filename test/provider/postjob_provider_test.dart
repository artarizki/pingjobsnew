import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/postjob_model.dart';
import 'package:pingjobs/provider/postjob_provider.dart';

import 'postjob_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  test('addData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    var data = {
      "id": 1,
      "job_name": "Test Judul Pekerjaan",
      "job_category": 'Lainnya',
      'job_date': '2021-01-01 00:00:00',
      "job_description": "Test Deksripsi Pekerjaan",
      "fee": "0",
      "hours": "0",
      "job_photovideo": "testupload.jpg",
      "street": "Test Alamat",
      "city": "Test Kota",
      "pinlat": 0,
      "pinlng": 0
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_postjob.php'), headers: headers, body: json.encode(data)))
        .thenAnswer((_) async => http.Response('{"message":"Insert Job Success"} ', 200));
    // List<ModelAktivitas> modelAktivitas = await AktivitasVM.getData(client);
    expect(await PostJobVM.addData(client), equals("Insert Job Success"));
  });

  test('addDataError', () {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    var data = {
      "id": 1,
      "job_name": "Test Judul Pekerjaan",
      "job_category": 'Lainnya',
      'job_date': '2021-01-01 00:00:00',
      "job_description": "Test Deksripsi Pekerjaan",
      "fee": "0",
      "hours": "0",
      "job_photovideo": "testupload.jpg",
      "street": "Test Alamat",
      "city": "Test Kota",
      "pinlat": 0,
      "pinlng": 0
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_postjob.php'), headers: headers, body: json.encode(data)))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelActivity> modelActivity = await ActivityVM.getData(client);
    expect(PostJobVM.addData(client), throwsException);
  });

  test('chooseData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    var data = {
      "id_jobseeker": "1",
      "id_job_applied": "6",
      "job_date": "2021-06-22 00:00:00"
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/apply_postjob.php'), headers: headers, body: json.encode(data)))
        .thenAnswer((_) async => http.Response('{"message":"Apply Success"} ', 200));
    // List<ModelAktivitas> modelAktivitas = await AktivitasVM.getData(client);
    expect(await PostJobVM.chooseData(client), equals("Apply Success"));
  });

  test('chooseDataError', () {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    var data = {
      "id_jobseeker": "1",
      "id_job_applied": "6",
      "job_date": "2021-06-22 00:00:00"
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/apply_postjob.php'), headers: headers, body: json.encode(data)))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelActivity> modelActivity = await ActivityVM.getData(client);
    expect(PostJobVM.chooseData(client), throwsException);
  });
}