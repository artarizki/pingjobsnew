import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/ratingreview_model.dart';
import 'package:pingjobs/provider/ratingreview_provider.dart';
import 'ratingreview_provider_test.mocks.dart';
// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('addRRData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var rrData = {
      "id_user": "8",
      "id_user2": "1",
      "id_job": "7",
      "keterangan": "Test Review",
      "bintang": "5",
      "role": "penyedia",
      "ketepatan_waktu": "1",
      "sikap_perilaku": "1"
    };
    var encodedBody = json.encode(rrData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_ratingreview.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('{"message": "RatingReview Success"}', 200));
    expect(await RatingReviewVM.addData(client), equals("RatingReview Success"));
  });

  test('addRRDataError', () {
    var rrData = {
      "id_user": "8",
      "id_user2": "1",
      "id_job": "7",
      "keterangan": "Test Review",
      "bintang": "5",
      "role": "penyedia",
      "ketepatan_waktu": "1",
      "sikap_perilaku": "1"
    };
    var encodedBody = json.encode(rrData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_ratingreview.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    expect(RatingReviewVM.addData(client), throwsException);
  });
}