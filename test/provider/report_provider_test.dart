import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:pingjobs/model/report_model.dart';
import 'package:pingjobs/provider/report_provider.dart';

import 'report_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('addReportData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var reportData = {
      "id_reporter": "2",
      "email_targetuser": "arta@gmail.com",
      "keterangan": "Test Report"
    };
    var encodedBody = json.encode(reportData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_report.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('{"message": "Insert Report Success"}', 200));
    expect(await ReportVM.insertData(client), equals("Insert Report Success"));
  });

  test('addReportDataError', () {
    var reportData = {
      "id_reporter": "2",
      "email_targetuser": "arta@gmail.com",
      "keterangan": "Test Report"
    };
    var encodedBody = json.encode(reportData);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/add_report.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelActivity> modelActivity = await ActivityVM.getData(client);
    expect(ReportVM.insertData(client), throwsException);
  });
}