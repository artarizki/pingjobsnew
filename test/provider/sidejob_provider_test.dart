import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pingjobs/provider/sidejob_provider.dart';
import 'package:pingjobs/model/sidejob_model.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'sidejob_provider_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('getSideJobData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.get(Uri.parse(
            'https://bertigagroup.com/pj.com/public/backend/get_jobs.php')))
        .thenAnswer((_) async => http.Response('''
            {
              "id_job": "6",
              "id_jobseeker": null,
              "id_jobprovider": "1",
              "name": "arta",
              "foto_profil": "image_picker5844114728275886390.jpg",
              "job_name": "Asisten Masak",
              "job_category": "Lainnya",
              "job_date": "2021-07-07 00:00:00",
              "job_description": "Saya butuh orang untuk asisten masak",
              "fee": "100000",
              "distance": null,
              "hours": "2",
              "job_photovideo": "IMG-20210629-WA0008.jpeg,IMG-20210704-WA0002.jpeg",
              "street": "Kranggan",
              "city": "Lamongan",
              "X(j.pin)": "-7.1164190629084",
              "Y(j.pin)": "112.41906601936"
            }''', 200));
    // List<ModelSideJob> modelSideJob = await SideJobVM.getData(client);
    expect(await SideJobVM.getData(client), isA<ModelSideJob>());
  });

  test('getSideJobDataError', () {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.get(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/get_jobs.php')))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    // List<ModelSideJob> modelSideJob = await SideJobVM.getData(client);
    expect(SideJobVM.getData(client), throwsException);
  });

  test('endData', () async {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {"id_job": 1};
    var encodedBody = json.encode(data);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/end_job.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('{"message": "End Success"}', 200));
    expect(await SideJobVM.endData(client), equals("End Success"));
  });

  test('endDataError', () {
    final client = MockClient();
    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    var data = {"id_job": 1};
    var encodedBody = json.encode(data);
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    when(client.post(Uri.parse(
        'https://bertigagroup.com/pj.com/public/backend/end_job.php'), headers: headers, body: encodedBody))
        .thenAnswer((_) async => http.Response('Not Found', 404));
    expect(SideJobVM.endData(client), throwsException);
  });
}
